import React from "react";

const ErrorCard = (props) => {
  return (
    <div className="flex w-full items-center rounded-lg border border-border-100 p-4">
      <div className="mr-4 h-4 w-[2px] flex-shrink-0 rounded-lg bg-pastelRed" />
      <div>
        <p className="font-regular text-base text-black">{props.title}</p>
      </div>
    </div>
  );
};

export default ErrorCard;
