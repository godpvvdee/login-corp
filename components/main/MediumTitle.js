import React from "react";

const MediumTitle = (props) => {
  return (
    <div className="mb-5 flex">
      <div className="mt-1 mr-5 h-5 w-[2px] rounded-lg bg-primary" />
      <div>
        <p className="font-bold text-px18 text-black">{props.title}</p>
      </div>
    </div>
  );
};

export default MediumTitle;
