const SmallTitle = (props) => {
  return (
    <div className="mb-5 flex items-center">
      <div className="mr-4 h-4 w-[2px] rounded-lg bg-primary" />
      <p className="font-bold text-base text-black">{props.title}</p>
    </div>
  );
};

export default SmallTitle;
