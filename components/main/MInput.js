import React, { useState } from "react";
import NumberFormat from "react-number-format";

const MInput = (props) => {
  const [isHide, setIsHide] = useState(true);
  const handleKeyDown = (data) => {
    if (data.key === "Enter") {
      props.onEnter();
    }
  };
  return (
    <div className="md-input-main mb-2">
      <div className="md-input-box">
        {props.type === "pin" ? (
          <input
            autoComplete="new-password"
            id={props.id}
            value={props.value}
            name={props.id}
            type={isHide ? "password" : "text"}
            onKeyPress={(event) => {
              if (!/[0-9]/.test(event.key)) {
                event.preventDefault();
              }
            }}
            className="md-input"
            maxLength={6}
            placeholder=" "
            onKeyDown={handleKeyDown}
            onChange={(e) => {
              props.onChange(e.target.value);
            }}
          />
        ) : props.numberFormat ? (
          <NumberFormat
            placeholder=" "
            isAllowed={(values) => {
              const { formattedValue, floatValue } = values;
              return formattedValue === "" || floatValue <= 100000000000;
            }}
            value={props.value}
            className="md-input"
            thousandSeparator="'"
            decimalScale={2}
            decimalSeparator="."
            onValueChange={(e) => props.onChange(e.value)}
          />
        ) : (
          <input
            autoComplete="off"
            id={props.id}
            value={props.value}
            name={props.id}
            type={props.type}
            className="md-input"
            maxLength={props.maxLength}
            placeholder=" "
            onKeyDown={handleKeyDown}
            onChange={(e) => {
              props.onChange(e.target.value);
            }}
          />
        )}
        <label className="md-label">{props.placeholder}</label>
        {props.type === "pin" && (
          <button
            onClick={() => {
              setIsHide(!isHide);
            }}
          >
            <img
              src={isHide ? "/icons/ic_hide.png" : "/icons/ic_unhide.png"}
              className="absolute top-[14px] right-4 h-5 w-5"
            />
          </button>
        )}
        <div className="md-input-underline" />
      </div>
    </div>
  );
};

export default MInput;
