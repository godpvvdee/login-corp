import React from "react";
import Link from "next/link";
const SidebarItem = (props) => {
  return (
    <Link href={props.to}>
      <a>
        <div
          onClick={() => {
            props.onClick();
          }}
          className={` group mb-1 flex items-center rounded-xl py-3 px-3 transition duration-300 ease-out hover:bg-primary ${
            props.id === "active" ? "bg-primary " : "bg-white"
          } `}
        >
          <svg
            className={`mr-4 h-6 w-6 group-hover:fill-white ${
              props.id === "active" ? "fill-white " : "fill-gray"
            } ${props.isSmall ? "pl-[2.5px]" : "pl-0"}`}
            xmlns="http://www.w3.org/2000/svg"
          >
            {props.icon}
          </svg>

          <p
            className={`mt-[-1px] font-bold text-xs group-hover:text-white ${
              props.id === "active" ? "text-white" : "text-gray"
            }`}
          >
            {props.title}
          </p>
        </div>
      </a>
    </Link>
  );
};

export default SidebarItem;
