import React from "react";

const ExtraLargeTitle = (props) => {
  return (
    <div className="mb-5 flex">
      <div className="mt-1 mr-4 h-13 w-[2px] rounded-lg bg-primary" />
      <div className="">
        <p className="font-bold text-px24 text-black">{props.title}</p>
        <p className="-mt-7 font-regular text-base text-black">
          {props.description}
        </p>
      </div>
    </div>
  );
};

export default ExtraLargeTitle;
