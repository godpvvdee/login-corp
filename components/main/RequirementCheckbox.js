import React from "react";

const RequirementCheckbox = (props) => {
  return (
    <div className="mb-4 flex">
      <img
        className="mr-4 h-4 w-4"
        src={
          props.isTrue
            ? "/icons/ic_check_circle.png"
            : "/icons/ic_checkbox_false.png"
        }
        alt=""
      />
      <p className="mt-[-1px] font-regular text-xs text-black">{props.title}</p>
    </div>
  );
};

export default RequirementCheckbox;
