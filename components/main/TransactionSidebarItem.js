import React from "react";
import Link from "next/link";

const TransactionSidebarItem = (props) => {
  return (
    <Link href={props.to}>
      <div
        onClick={() => props.onClick()}
        className={`group flex h-12 items-center border-l-2 pl-7 hover:border-primary  hover:bg-primary/20 ${
          props.id === "active"
            ? "border-primary bg-primary/20"
            : "border-white bg-white"
        }`}
      >
        <svg
          className={`mr-4 h-6 w-6 group-hover:fill-primary ${
            props.id === "active" ? "fill-primary " : "fill-gray"
          } ${props.isSmall ? "pl-[2px]" : "pl-0"}`}
          xmlns="http://www.w3.org/2000/svg"
        >
          {props.icon}
        </svg>
        <p
          className={`mt-[-1px] font-bold text-xs group-hover:text-primary ${
            props.id === "active" ? "text-primary " : "text-gray"
          }`}
        >
          {props.title}
        </p>
      </div>
    </Link>
  );
};

export default TransactionSidebarItem;
