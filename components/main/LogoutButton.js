import IconLogout from "../../icons/IconLogout";
import IconSettings from "../../icons/IconSettings";
import { useRouter } from "next/router";

import axios from "axios";
const LogoutButton = (props) => {
  const router = useRouter();

  const logoutHandler = async () => {
    await axios.post("/api/logout", {});
    router.reload(window.location.pathname);
  };
  return (
    <div
      onClick={logoutHandler}
      className="group flex cursor-pointer items-center rounded-lg bg-white py-3 px-3 hover:bg-error"
    >
      <svg
        className="mr-4 h-6 w-6 fill-error  group-hover:fill-white "
        xmlns="http://www.w3.org/2000/svg"
      >
        <IconLogout />
      </svg>

      <p className="mt-[-2px] font-bold text-xs text-error group-hover:text-white">
        Гарах
      </p>
    </div>
  );
};

export default LogoutButton;
