import Axios from "axios";
import React, { useState } from "react";
import Link from "next/link";
import IconBank from "../../icons/IconBank";
import IconCard from "../../icons/IconCard";
import IconChart from "../../icons/IconChart";
import IconGrid from "../../icons/IconGrid";
import IconTemplate from "../../icons/IconTemplate";
import IconTranCheck from "../../icons/IconTranCheck";
import IconTransaction from "../../icons/IconTransaction";
import IconTransactionBank from "../../icons/IconTransactionBank";
import IconUser from "../../icons/IconUser";
import IconWaitingList from "../../icons/IconWaitingList";
import LogoutButton from "./LogoutButton";
import SidebarItem from "./SidebarItem";
import TransactionSidebarItem from "./TransactionSidebarItem";
import { Transition } from "@tailwindui/react";
import IconWaitingGroup from "../../icons/IconWaitingGroup";
import IconHamburger from "../../icons/IconHamburger";

const SideBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const logoutHandler = async () => {
    await Axios.post("/api/logout", {});
    props.onLogout(false);
    window.location.replace("/");
  };
  return (
    <>
      <div className="hidden md:block">
        <div className="fixed top-0 left-0 bottom-0 z-50 flex min-h-screen w-75 flex-col justify-between overflow-x-hidden bg-white px-7 md:bg-defaultBackground ">
          <div>
            <div className="flex justify-between">
              <Link href="/">
                <a>
                  <img
                    src="/icons/ic_mbank.png"
                    className="mt-7 h-10 w-[100px]"
                    alt=""
                  />
                </a>
              </Link>
              <button
                onClick={() => props.setToggle(false)}
                className="mt-8 h-8 w-8 md:hidden"
              >
                <img src="/icons/esc.png" className="h-8 w-8" alt="" />
              </button>
            </div>
            <div className="my-7 min-h-[1px] bg-tableBorder" />
            <SidebarItem
              onClick={() => {
                props.onClick();
                setIsOpen(false);
              }}
              id={
                window.location.pathname === "/accounts" ||
                window.location.pathname === "/"
                  ? "active"
                  : "inactive"
              }
              icon={<IconBank />}
              title="Нүүр"
              to="/accounts"
            />
            <button
              onClick={() => setIsOpen(!isOpen)}
              className={`${
                isOpen
                  ? "mb-0 rounded-t-xl bg-primary"
                  : "mb-1 rounded-xl bg-white"
              } group flex w-full  items-center justify-between py-3 px-3 transition duration-300 ease-out hover:bg-primary`}
            >
              <div className="flex items-center">
                <svg
                  className={`${
                    isOpen ? "fill-white" : "fill-gray"
                  } mr-4 h-6 w-6 group-hover:fill-white`}
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <IconTransaction />
                </svg>

                <p
                  className={`${
                    isOpen ? "text-white" : "text-gray"
                  } mt-[-1px] font-bold text-xs group-hover:text-white`}
                >
                  Шилжүүлэг
                </p>
              </div>
              <img
                src={
                  isOpen
                    ? "/icons/ic_menu_picker_true.png"
                    : "/icons/ic_menu_picker_false.png"
                }
                className="h-5 w-5"
                alt=""
              />
            </button>
            <Transition
              show={isOpen}
              enter="transition ease-out origin-top-left duration-200"
              enterFrom="opacity-0 scale-90"
              enterTo="opacity-100 scale-100"
              leave="transition origin-top-left ease-in duration-100"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-90"
            >
              {isOpen ? (
                <div className="flex flex-col">
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-intra"
                        ? "active"
                        : "inactive"
                    }
                    title="Бусдын данс руу"
                    icon={<IconTransactionBank />}
                    to="/transaction-intra"
                  />
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-between"
                        ? "active"
                        : "inactive"
                    }
                    title="Өөрийн данс хооронд"
                    icon={<IconUser />}
                    to="/transaction-between"
                  />
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-batch"
                        ? "active"
                        : "inactive"
                    }
                    title="Багц шилжүүлэг"
                    icon={<IconTransaction />}
                    to="/transaction-batch"
                  />
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-template"
                        ? "active"
                        : "inactive"
                    }
                    title="Шилжүүлгийн загвар"
                    icon={<IconTemplate />}
                    to="/transaction-template"
                  />
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-waiting"
                        ? "active"
                        : "inactive"
                    }
                    title="Батлах шилжүүлэг"
                    isSmall
                    icon={<IconWaitingList />}
                    to="/transaction-waiting"
                  />
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-waiting-group"
                        ? "active"
                        : "inactive"
                    }
                    title="Батлах багц шилжүүлэг"
                    isSmall
                    icon={<IconWaitingGroup />}
                    to="/transaction-waiting-group"
                  />
                  <TransactionSidebarItem
                    onClick={() => props.onClick()}
                    id={
                      window.location.pathname === "/transaction-check"
                        ? "active"
                        : "inactive"
                    }
                    title="Шилжүүлэг лавлах"
                    icon={<IconTranCheck />}
                    to="/transaction-check"
                  />
                </div>
              ) : (
                <div />
              )}
            </Transition>
            <SidebarItem
              onClick={() => {
                setIsOpen(false);
                props.onClick();
              }}
              id={window.location.pathname === "/cards" ? "active" : "inactive"}
              icon={<IconCard />}
              title="Карт"
              to="/cards"
            />
            <SidebarItem
              onClick={() => {
                setIsOpen(false);
                props.onClick();
              }}
              //   id={window.location.pathname === "/pfm" ? "active" : "inactive"}
              icon={<IconChart />}
              title="PFM"
              to="/pfm"
            />
            <SidebarItem
              onClick={() => {
                setIsOpen(false);
                props.onClick();
              }}
              //   id={window.location.pathname === "/mhub" ? "active" : "inactive"}
              icon={<IconGrid />}
              title="М Хаб"
              to="/mhub"
            />
          </div>
          <div className="w-full pb-7">
            {/* <SidebarItem
          onClick={() => {
            setIsOpen(false);
          }}
          id={window.location.pathname === "/help" ? "active" : "inactive"}
          icon={<IconHelp />}
          title="М Зөвлөх"
          to="/help"
        />
        <SidebarItem
          onClick={() => {
            setIsOpen(false);
          }}
          id={window.location.pathname === "/terms" ? "active" : "inactive"}
          icon={<IconLock />}
          title="Нууцлал"
          to="/security"
          isSmall
        /> */}
            <LogoutButton onLogout={logoutHandler} />
          </div>
        </div>
      </div>

      <div className="block md:hidden">
        {props.toggle ? (
          <div className="absolute inset-0 z-20 bg-black/60 md:relative md:h-0 ">
            <div className="fixed top-0 left-0 bottom-0 z-50 flex min-h-screen w-75 flex-col justify-between overflow-x-hidden bg-white px-7 md:block md:bg-defaultBackground ">
              <div>
                <div className="flex justify-between">
                  <Link href="/">
                    <a>
                      <img
                        src="/icons/ic_mbank.png"
                        className="mt-7 h-10 w-[100px]"
                        alt=""
                      />
                    </a>
                  </Link>
                  <button
                    onClick={() => props.setToggle(false)}
                    className="mt-8 h-8 w-8 md:hidden"
                  >
                    <img src="/icons/esc.png" className="h-8 w-8" alt="" />
                  </button>
                </div>
                <div className="my-7 min-h-[1px] bg-tableBorder" />
                <SidebarItem
                  onClick={() => {
                    props.onClick();
                    setIsOpen(false);
                  }}
                  //   id={
                  //     window.location.pathname === "/accounts" ||
                  //     window.location.pathname === "/"
                  //       ? "active"
                  //       : "inactive"
                  //   }
                  icon={<IconBank />}
                  title="Нүүр"
                  to="/accounts"
                />
                <button
                  onClick={() => setIsOpen(!isOpen)}
                  className={`${
                    isOpen
                      ? "mb-0 rounded-t-xl bg-primary"
                      : "mb-1 rounded-xl bg-white"
                  } group flex w-full  items-center justify-between py-3 px-3 transition duration-300 ease-out hover:bg-primary`}
                >
                  <div className="flex items-center">
                    <svg
                      className={`${
                        isOpen ? "fill-white" : "fill-gray"
                      } mr-4 h-6 w-6 group-hover:fill-white`}
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <IconTransaction />
                    </svg>

                    <p
                      className={`${
                        isOpen ? "text-white" : "text-gray"
                      } mt-[-1px] font-bold text-xs group-hover:text-white`}
                    >
                      Шилжүүлэг
                    </p>
                  </div>
                  <img
                    src={
                      isOpen
                        ? "/icons/ic_menu_picker_true.png"
                        : "/icons/ic_menu_picker_false.png"
                    }
                    className="h-5 w-5"
                    alt=""
                  />
                </button>
                <Transition
                  show={isOpen}
                  enter="transition ease-out origin-top-left duration-200"
                  enterFrom="opacity-0 scale-90"
                  enterTo="opacity-100 scale-100"
                  leave="transition origin-top-left ease-in duration-100"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-90"
                >
                  {isOpen ? (
                    <div className="flex flex-col">
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname === "/transaction-intra"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Бусдын данс руу"
                        icon={<IconTransactionBank />}
                        to="/transaction-intra"
                      />
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname === "/transaction-between"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Өөрийн данс хооронд"
                        icon={<IconUser />}
                        to="/transaction-between"
                      />
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname === "/transaction-batch"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Багц шилжүүлэг"
                        icon={<IconTransaction />}
                        to="/transaction-batch"
                      />
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname === "/transaction-template"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Шилжүүлгийн загвар"
                        icon={<IconTemplate />}
                        to="/transaction-template"
                      />
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname === "/transaction-waiting"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Батлах шилжүүлэг"
                        isSmall
                        icon={<IconWaitingList />}
                        to="/transaction-waiting"
                      />
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname ===
                        //   "/transaction-waiting-group"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Батлах багц шилжүүлэг"
                        isSmall
                        icon={<IconWaitingGroup />}
                        to="/transaction-waiting-group"
                      />
                      <TransactionSidebarItem
                        onClick={() => props.onClick()}
                        // id={
                        //   window.location.pathname === "/transaction-check"
                        //     ? "active"
                        //     : "inactive"
                        // }
                        title="Шилжүүлэг лавлах"
                        icon={<IconTranCheck />}
                        to="/transaction-check"
                      />
                    </div>
                  ) : (
                    <div />
                  )}
                </Transition>
                <SidebarItem
                  onClick={() => {
                    setIsOpen(false);
                    props.onClick();
                  }}
                  //   id={
                  //     window.location.pathname === "/cards"
                  //       ? "active"
                  //       : "inactive"
                  //   }
                  icon={<IconCard />}
                  title="Карт"
                  to="/cards"
                />

                <SidebarItem
                  onClick={() => {
                    setIsOpen(false);
                    props.onClick();
                  }}
                  //   id={
                  //     window.location.pathname === "/pfm" ? "active" : "inactive"
                  //   }
                  icon={<IconChart />}
                  title="PFM"
                  to="/pfm"
                />
                <SidebarItem
                  onClick={() => {
                    setIsOpen(false);
                    props.onClick();
                  }}
                  //   id={
                  //     window.location.pathname === "/mhub" ? "active" : "inactive"
                  //   }
                  icon={<IconGrid />}
                  title="М Хаб"
                  to="/mhub"
                />
              </div>
              <div className="w-full pb-7">
                {/* <SidebarItem
          onClick={() => {
            setIsOpen(false);
          }}
          id={window.location.pathname === "/help" ? "active" : "inactive"}
          icon={<IconHelp />}
          title="М Зөвлөх"
          to="/help"
        />
        <SidebarItem
          onClick={() => {
            setIsOpen(false);
          }}
          id={window.location.pathname === "/terms" ? "active" : "inactive"}
          icon={<IconLock />}
          title="Нууцлал"
          to="/security"
          isSmall
        /> */}
                <LogoutButton onLogout={logoutHandler} />
              </div>
            </div>
             
            <div
              className="h-screen w-full "
              onClick={() => props.setToggle(false)}
            ></div>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default SideBar;
