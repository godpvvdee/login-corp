import React from "react";
import SmallTitle from "./SmallTitle";

const WarningCard = (props) => {
  return (
    <div className="flex w-full flex-col rounded-lg border border-border-100 p-4">
      <div className="mb-4 flex items-center">
        <div className="mr-4 h-4 w-[2px] rounded-lg bg-secondary" />
        <p className="font-bold text-base text-black">{props.title}</p>
      </div>
      <p className="font-regular text-xs text-black">{props.description}</p>
    </div>
  );
};

export default WarningCard;
