import React, { useState } from "react";

const MInputPassword = (props) => {
  const [isHide, setIsHide] = useState(true);
  const handleKeyDown = (data) => {
    if (data.key === "Enter") {
      props.onEnter();
    }
  };
  return (
    <div className="md-input-main mb-2">
      <div className="md-input-box">
        <input
          autoComplete="new-password"
          id={props.id}
          value={props.value}
          name={props.id}
          type={isHide ? "password" : "text"}
          className="md-input"
          maxLength={props.maxLength}
          placeholder=" "
          onKeyDown={handleKeyDown}
          onChange={(e) => {
            props.onChange(e.target.value);
          }}
        />
        <label className="md-label">{props.placeholder}</label>

        <button
          onClick={() => {
            setIsHide(!isHide);
          }}
        >
          <img
            src={isHide ? "/icons/ic_hide.png" : "/icons/ic_unhide.png"}
            className="absolute top-[14px] right-4 h-5 w-5"
          />
        </button>
        <div className="md-input-underline" />
      </div>
    </div>
  );
};

export default MInputPassword;
