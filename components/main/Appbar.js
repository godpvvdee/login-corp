import { sha256 } from "js-sha256";
import React, { useEffect, useState } from "react";
// import { useHistory } from "react-router-dom";
import { useRouter } from "next/router";
import axios from "axios";
import { toast } from "react-toastify";
import NotificationDeleteModal from "../modals/NotificationDeleteModal";
import NotificationModal from "../modals/NotificationModal";
import PincodeCheckModal from "../modals/PincodeCheckModal";
import ProfileChangePassword from "../profile/ProfileChangePassword";
import ProfileCorporateInfo from "../profile/ProfileCorporateInfo";
import ProfileDeviceList from "../profile/ProfileDeviceList";
import ProfilePersonalInfo from "../profile/ProfilePersonalInfo";
import ErrorToast from "./ErrorToast";
import OutsideClickHandler from "react-outside-click-handler";
import SideBar from "./Sidebar";
import InfoModal from "../modals/InfoModal";
import ProfileSecurity from "../profile/ProfileSecurity";
import { useAuth } from "../../contexts/auth";
import Login from "../Auth/Login";
import ChooseCorporate from "pages/choose";
const Appbar = (props) => {
  const [toggle, setToggle] = useState(false);
  const { isAuthenticated, user } = useAuth();
  const router = useRouter();

  //   let history = useHistory();
  const [pinmodalLoading, setPinmodalLoading] = useState(false);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [isCorporatePickerOpen, setIsCorporatePickerOpen] = useState(false);
  const [isNotificationsOpen, setIsNotificationsOpen] = useState(false);
  const [isNotificationModalOpen, setIsNotificationModalOpen] = useState(false);
  const [isNotificationDeleteModalOpen, setIsNotificationDeleteModalOpen] =
    useState(false);
  const [selectedNotification, setSelectedNotification] = useState({});
  const [notificationsList, setNotificationsList] = useState([]);
  const [notificationsCount, setNotificationsCount] = useState(
    props.notificationsCount
  );
  const [pinModal, setPinModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [selectedCorp, setSelectedCorp] = useState({});
  const [timeoutModal, setTimeoutModal] = useState(false);
  const [counter, setCounter] = useState(900);

  const [userNames, setUserNames] = useState();

  useEffect(() => {
    if (counter > 0) {
      const intervalId = setInterval(() => {
        setCounter(counter - 1);
      }, 1000);
      return () => clearInterval(intervalId);
    } else {
      logoutHandler();
    }
  }, [counter]);
  const logoutHandler = async () => {
    await axios.post("/api/logout", {});

    setTimeoutModal(true);
  };
  // console.log("props.chosenCorporate", props.chosenCorporate);
  // const chooseCorporateHandler = async (data) => {
  //   await axios
  //     .post("/chooseActiveCorporate", {
  //       corporate: data.corporate_cif,
  //     })
  //     .then((res) => {
  //       if (res.data.isActive) {
  //         checkResetPin(data);
  //       } else {
  //         toast.error(`🥺 ${res.data.message}`, {
  //           position: "bottom-right",
  //           autoClose: 3000,
  //           hideProgressBar: false,
  //           closeOnClick: true,
  //           pauseOnHover: true,
  //           draggable: true,
  //           progress: undefined,
  //         });
  //       }
  //     });
  //   setIsCorporatePickerOpen(false);
  // };

  const pinHandler = (data) => {
    setPincode(data);
  };

  const checkAuth = async () => {
    await axios.post("/api/hello").then((res) => {
      console.log("RES ______. ", res);
      if (res.data.success) {
        setUserNames(res.data.data);
        console.log("users = ", userNames);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
  };
  const getUserInfo = async () => {
    await axios.get("/api/userInfo").then((res) => {
      console.log("RES ______ ", res.data);
      if (res.data.success) {
        setUserNames(res.data.data);
        console.log("users = ", userNames);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
  };

  if (user?.isAuth && isAuthenticated) {
    return (
      <>
        {router.pathname === "/choose" ? (
          window.location.assign("/accounts")
        ) : (
          <>
            {" "}
            <div className="top-0 flex h-24 min-w-full items-center justify-between px-7 pl-80">
              <div />

              <div className="absolute left-2 flex w-full items-center md:relative md:right-0 md:w-auto ">
                <div className="  ">
                  <button
                    onClick={() => setToggle(!toggle)}
                    className="m-2 h-8 w-8 md:hidden"
                  >
                    <img
                      src="/icons/hamburger.png"
                      className="h-8 w-8 "
                      alt=""
                    />
                  </button>
                </div>
                <div className="absolute right-10 flex md:relative md:right-0 ">
                  <button
                    onClick={() => {
                      setCounter(900);
                    }}
                    className="mr-4 hidden h-12 items-center justify-center rounded-lg  bg-white px-4 md:flex"
                  >
                    <img
                      src="/icons/ic_alarm.png"
                      className="h-5 w-4.5"
                      alt=""
                    />
                    <p className="ml-2 font-semibold text-base text-black">
                      {Math.floor(counter / 60) +
                        ":" +
                        (counter % 60).toString().padStart(2, "0")}
                    </p>
                  </button>
                  {/* <AppbarTimer /> */}
                  {/* <div className="h-12 w-12 bg-white rounded-lg flex items-center justify-center mr-4">
          <img src="/icons/ic_eye.png" className="w-6 h-6" alt="" />
        </div> */}
                  <OutsideClickHandler
                    onOutsideClick={() => {
                      setIsNotificationsOpen(false);
                    }}
                  >
                    <button
                      onClick={() => {
                        if (!isNotificationsOpen) {
                          getNotifications();
                        }
                        setIsNotificationsOpen(!isNotificationsOpen);
                        setIsDropdownOpen(false);
                      }}
                      className="relative mr-4 flex h-12 w-12 cursor-pointer items-center justify-center rounded-lg bg-white"
                    >
                      {notificationsCount > 0 ? (
                        <div className="absolute top-2 right-2 flex h-4 w-4 justify-center rounded-full bg-primary">
                          <p className="font-semibold text-[10px] text-white">
                            {notificationsCount}
                          </p>
                        </div>
                      ) : null}
                      <img
                        src="/icons/ic_bell.png"
                        className="h-5 w-4"
                        alt=""
                      />
                    </button>

                    {isNotificationsOpen ? (
                      <div className="absolute top-14 right-0 z-10 flex w-82 flex-col justify-center rounded-lg bg-white p-4 shadow-xl md:top-20 md:right-7 md:w-103">
                        <div className="flex items-center justify-between">
                          <p className="font-bold text-px18 text-black">
                            Мэдэгдэл
                          </p>
                          <div className="flex flex-row gap-2">
                            <button
                              onClick={() => {
                                readAllNotifications();
                              }}
                              className="rounded-lg bg-border-100 p-[10px]"
                            >
                              <p className="font-semibold text-xxs text-black">
                                Бүгд уншсан
                              </p>
                            </button>
                            <button
                              onClick={() => {
                                deleteAllNotifications();
                              }}
                              className="rounded-lg bg-border-100 p-[10px]"
                            >
                              <p className="font-semibold text-xxs text-black">
                                Цэвэрлэх
                              </p>
                            </button>
                          </div>
                        </div>
                        <div className="mt-4 flex max-h-[80vh] flex-col gap-2 overflow-y-scroll">
                          {notificationsList.map((item, index) => (
                            <button
                              onClick={(e) => {
                                e.stopPropagation();
                                setSelectedNotification(item);
                                readNotification(item);
                                setIsNotificationModalOpen(true);
                              }}
                              key={index}
                              className={`flex rounded-lg border border-border-100 p-4 ${
                                item.has_read === "0"
                                  ? "bg-white "
                                  : "bg-border-100"
                              }`}
                            >
                              <div
                                className={`flex h-9 w-9 items-center justify-center rounded-full ${
                                  item.has_read === "0"
                                    ? "bg-primary"
                                    : "bg-white"
                                }`}
                              >
                                <img
                                  src={
                                    item.has_read === "0"
                                      ? "/icons/ic_bell_white.png"
                                      : "/icons/ic_bell.png"
                                  }
                                  className="h-[20px] w-[16px]"
                                  alt=""
                                />
                              </div>
                              <div className="ml-4 flex w-60 flex-col text-left">
                                <p className="font-semibold text-base text-black">
                                  {item.title}
                                </p>
                                <p className="font-regular text-xs text-gray">
                                  {item.message}
                                </p>
                              </div>
                              <button
                                onClick={(e) => {
                                  e.stopPropagation();
                                  setSelectedNotification(item);
                                  setIsNotificationDeleteModalOpen(item);
                                }}
                              >
                                <img
                                  className="ml-8 flex h-5 w-5 cursor-pointer self-center"
                                  src="/icons/ic_close_gray.png"
                                  alt=""
                                />
                              </button>
                            </button>
                          ))}
                        </div>
                      </div>
                    ) : null}
                  </OutsideClickHandler>
                  <OutsideClickHandler
                    onOutsideClick={() => {
                      setIsDropdownOpen(false);
                    }}
                  >
                    <button
                      onClick={() => {
                        setIsNotificationsOpen(false);
                        setIsDropdownOpen(!isDropdownOpen);
                        getUserInfo();
                      }}
                      className="flex h-12 w-16 items-center justify-between rounded-lg bg-white px-4 md:w-64"
                    >
                      <div className="flex items-center justify-center">
                        <div className="mr-2 flex h-8 w-8 items-center justify-center rounded-full bg-primary">
                          <p className="mt-[-1px] font-bold text-xs text-white">
                            {props.chosenCorporate.corporate_name != undefined
                              ? props.chosenCorporate.corporate_name.substring(
                                  0,
                                  1
                                )
                              : ""}

                            {/* {props.chosenCorporate.map((item, index) => (
                        <span key={index}>
                          {item.corporate_name.substring(0, 1)}
                        </span>
                      ))} */}
                          </p>
                        </div>
                        <p className="mt-[-1px] hidden w-36 truncate font-semibold text-base text-black md:block">
                          {props.chosenCorporate.corporate_name}

                          {/* {props.chosenCorporate.map((item, index) => (
                      <span key={index}>{item.corporate_name}</span>
                    ))} */}
                          {/* {props.chosenCorporate.map((item, index) => (
                      <span key={index}>{item.corporate_name}</span>
                    ))} */}
                        </p>
                      </div>
                      <img
                        className="bg hidden h-[18px] w-[18px] md:block"
                        src={
                          isDropdownOpen
                            ? "/icons/ic_picker_active.png"
                            : "/icons/ic_picker.png"
                        }
                        alt=""
                      />
                    </button>

                    {isDropdownOpen ? (
                      <div className="absolute top-20 right-0 z-[1] flex w-72 flex-col justify-center rounded-lg bg-white p-4 shadow-xl">
                        <button
                          onClick={() =>
                            setIsCorporatePickerOpen(!isCorporatePickerOpen)
                          }
                          className="jusitfy mb-4 flex h-16 items-center justify-between rounded-lg bg-border-100 px-4"
                        >
                          <div className="flex items-center text-left">
                            <div className="flex h-8 w-8 items-center justify-center rounded-full bg-primary">
                              <p className="mt-[-1px] font-bold text-xs text-white">
                                {props.chosenCorporate.corporate_name !=
                                undefined
                                  ? props.chosenCorporate.corporate_name.substring(
                                      0,
                                      1
                                    )
                                  : ""}
                                {/* {props.chosenCorporate.map((item, index) => (
                            <span key={index}>
                              {item.corporate_name.substring(0, 1)}
                            </span>
                          ))} */}
                              </p>
                            </div>
                            <div className="ml-2 flex flex-col">
                              <p className="w-36 truncate font-semibold text-base text-black">
                                {props.chosenCorporate.corporate_name}
                              </p>
                              <p className="font-regular text-xs text-gray">
                                {props.chosenCorporate.position}
                              </p>
                            </div>
                          </div>
                          <img
                            src={
                              isCorporatePickerOpen
                                ? "/icons/ic_menu_picker_green.png"
                                : "/icons/ic_menu_picker_gray.png"
                            }
                            className="h-5 w-5"
                          />
                        </button>
                        {isCorporatePickerOpen ? (
                          props.chosenCorporate.map((item, index) => (
                            <button
                              key={index}
                              onClick={() => {
                                setPinModal(true);
                                setSelectedCorp(item);
                              }}
                              className={`mb-4 flex h-12 w-full items-center rounded-lg px-4 ${
                                index % 2 === 0 ? "bg-white" : "bg-border-100"
                              }  group hover:bg-primary`}
                            >
                              <div className="mr-2 flex h-8 w-8  items-center justify-center rounded-full bg-primary group-hover:bg-white">
                                <p className="mt-[-1px] font-bold text-xs text-white group-hover:text-primary">
                                  {item.corporate_name.substring(0, 1)}
                                </p>
                              </div>
                              <p className="mt-[-1px] w-36 truncate text-left font-regular text-base text-black group-hover:text-white">
                                {item.corporate_name}
                              </p>
                            </button>
                          ))
                        ) : (
                          <div />
                        )}
                        <ProfilePersonalInfo
                          onClick={() => setCounter(900)}
                          onCancel={() => setIsDropdownOpen(false)}
                        />
                        <ProfileCorporateInfo onClick={() => setCounter(900)} />
                        <ProfileDeviceList onClick={() => setCounter(900)} />
                        <ProfileSecurity onClick={() => setCounter(900)} />
                        <ProfileChangePassword
                          onClick={() => setCounter(900)}
                          onCancel={() => setIsDropdownOpen(false)}
                        />
                        {/* <ProfileChangePinCode /> */}
                      </div>
                    ) : (
                      <div />
                    )}
                  </OutsideClickHandler>
                </div>
              </div>
            </div>
            <SideBar
              toggle={toggle}
              setToggle={setToggle}
              onLogout={() => props.onLogout(false)}
              onClick={() => {
                setCounter(900);
              }}
            />
            <NotificationModal
              isOpen={isNotificationModalOpen}
              notification={selectedNotification}
              onClick={() => {
                setIsNotificationModalOpen(false);
              }}
            />
            <NotificationDeleteModal
              isOpen={isNotificationDeleteModalOpen}
              onClick={() => {
                setIsNotificationDeleteModalOpen(false);
              }}
              onDelete={() => {
                deleteNotification();
                setIsNotificationDeleteModalOpen(false);
              }}
            />
            <PincodeCheckModal
              onClose={() => {
                setPinModal(false);
              }}
              isLoading={pinmodalLoading}
              isOpen={pinModal}
              onChange={pinHandler}
              onClick={() => {
                checkPincode();
              }}
            />
            <ErrorToast />
            <InfoModal
              isOpen={timeoutModal}
              title="Холболт саллаа"
              description="Та 15 минут үйлдэл хийгээгүй тул дахин нэвтрэнэ үү."
              onClick={() => {
                window.location.reload();
                // router.push("/");
                setTimeoutModal(false);
              }}
            />
            {props.children}
          </>
        )}
      </>
    );
  } else if (isAuthenticated && !user?.isAuth) {
    return <ChooseCorporate />;
  } else {
    return <Login />;
  }
};

export default Appbar;
