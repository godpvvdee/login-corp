import React, { useState } from "react";
import Image from "next/image";
const MInputIcon = (props) => {
  const [isHide, setIsHide] = useState(true);
  const handleKeyDown = (data) => {
    if (data.key === "Enter") {
      props.onEnter();
    }
  };
  return (
    <div className="md-input-main relative mb-2">
      <div className="md-input-box w-[300px] md:w-full">
        <div className="fixed mt-[14px] ml-4">
          <Image src={props.icon} width="16px" height="20px" alt="" />
        </div>

        <input
          autoComplete="off"
          id={props.id}
          name={props.id}
          type={
            props.type === "password"
              ? isHide
                ? "password"
                : "text"
              : props.type
          }
          maxLength={props.maxLength}
          className="md-input login pl-9"
          placeholder=" "
          onKeyDown={handleKeyDown}
          onChange={(e) => props.onChange(e.target.value)}
        />

        <label htmlFor={props.id} className="md-label pl-9">
          {props.placeholder}
        </label>
        {props.type === "password" && (
          <button
            onClick={() => {
              setIsHide(!isHide);
            }}
          >
            <div className="absolute top-[14px] right-4">
              <Image
                src={isHide ? "/icons/ic_hide.png" : "/icons/ic_unhide.png"}
                width="20px"
                height="20px"
                alt=""
              />
            </div>
          </button>
        )}
        <div className="md-input-underline" />
      </div>
    </div>
  );
};

export default MInputIcon;
