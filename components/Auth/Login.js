import { useAuth } from "../../contexts/auth";
import Head from "next/head";
import { UAParser } from "ua-parser-js";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import MInputIcon from "../main/MInputIcon";
import ExtraLargeTitle from "../main/ExtraLargeTitle";

const delay = 0.5;
const Login = () => {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const parser = UAParser();
  const deviceId = parser.ua;
  const [show, setShow] = useState(false);

  const { login } = useAuth();

  useEffect(() => {
    let timer1 = setTimeout(() => setShow(true), delay * 1000);
    return () => {
      clearTimeout(timer1);
    };
  }, []);

  return show ? (
    <>
      <Head>
        <title>M-NEW-EP.</title>
      </Head>
      <div className="overflow-x-hidden bg-white">
        <div className="absolute top-[55px] left-[55px] z-50 mx-auto flex max-w-7xl justify-start">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className={`mx-4 h-8 w-20 cursor-pointer fill-white transition-all`}
          ></svg>
        </div>
        <div className=" m-auto grid  h-screen grid-cols-3  overflow-y-hidden ">
          <div className={"col-span-3 lg:col-span-2"}>
            <Image
              src={"/images/login_background.png"}
              layout={"responsive"}
              width={1300}
              height={1080}
              objectFit={"cover"}
              alt={"Neobank cityscape illustration 1"}
            />
          </div>

          <div className="col-span-3 mx-auto mb-4 flex  items-center justify-center rounded bg-white px-8 pt-6 pb-8 lg:col-span-1">
            <div className="flex flex-col md:block">
              <ExtraLargeTitle title="Нэвтрэх" description="1.0.0" />
              <div className="mb-9" />
              <MInputIcon
                maxLength={30}
                placeholder="Хэрэглэгчийн нэр"
                id="m-user"
                type="text"
                icon="/icons/ic_user.png"
                value={username}
                onChange={(e) => setUserName(e)}
              />
              <MInputIcon
                maxLength={16}
                placeholder="Нууц код"
                id="m-pass"
                type="password"
                icon="/icons/ic_password.png"
                onEnter={(event) => {
                  login(username, password, deviceId);
                }}
                onChange={(e) => setPassword(e)}
              />
              <button
                onClick={() => {
                  console.log("pass ", password);
                  username && password && login(username, password, deviceId);
                }}
                className="mt-6 h-12 w-[300px] rounded-lg bg-primary hover:opacity-90 md:w-full"
              >
                <p className="mt-[-1px] font-bold text-base text-white">
                  Нэвтрэх
                </p>
              </button>
              <Link href="/forgot">
                <button className="mt-6 w-[300px] text-center md:w-full">
                  <p className="cursor-pointer text-center font-regular text-xs text-gray">
                    Нэвтрэх нэр, нууц үгээ мартсан уу?
                    <span className="font-bold text-primary"> Сэргээх</span>
                  </p>
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : null;
};
export default Login;
