import React, { useState } from "react";

const TransactionAccountPicker = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const [isOpen, setIsOpen] = useState(false);

  const accountPickedHandler = (data) => {
    setIsOpen(false);
    props.onChange(data);
  };
  return (
    <>
      <button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className="flex h-16 w-full items-center justify-between rounded-lg bg-border-100 px-4"
      >
        <div className="flex flex-col text-left">
          <p className="font-regular text-xs text-gray">{props.title}</p>
          <p className="font-regular text-base text-black">
            {props.selectedAccount.currentAccount?.acct_num +
              " " +
              props.selectedAccount.currentAccount?.acct_ccy}
          </p>
        </div>
        <div className="flex">
          <p className="font-semibold text-base text-black">
            {formatter
              .format(
                props.selectedAccount.currentAccount?.positive_balance_amt
              )
              .replace("$", "")}
          </p>
          <img
            src={
              isOpen
                ? "/icons/ic_menu_picker_green.png"
                : "/icons/ic_menu_picker_gray.png"
            }
            className="ml-4 h-5 w-5"
            alt=""
          />
        </div>
      </button>
      {isOpen ? (
        <div className="flex flex-col rounded-lg bg-white">
          {props.accountList.map((item, index) => (
            <button
              key={index}
              onClick={() => {
                accountPickedHandler(item);
              }}
              className="group flex h-16 w-full items-center justify-between bg-white px-4 hover:bg-primary"
            >
              <div className="flex flex-col text-left">
                <p className="font-regular text-xs text-gray group-hover:text-white">
                  {props.title}
                </p>
                <p className="font-regular text-base text-black group-hover:text-white">
                  {item.currentAccount.acct_num +
                    " " +
                    item.currentAccount.acct_ccy}
                </p>
              </div>
              <div className="flex">
                <p className="font-semibold text-base text-black group-hover:text-white">
                  {formatter
                    .format(item.currentAccount.positive_balance_amt)
                    .replace("$", "")}
                </p>
              </div>
            </button>
          ))}
        </div>
      ) : (
        <div />
      )}
    </>
  );
};

export default TransactionAccountPicker;
