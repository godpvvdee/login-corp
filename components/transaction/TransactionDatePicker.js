import React, { useState } from "react";

const TransactionDatePicker = (props) => {
  const [isPickerOpen, setIsPickerOpen] = useState(false);
  return (
    <button
      onClick={() => {
        setIsPickerOpen(!isPickerOpen);
      }}
      className="mb-2 flex h-13 w-56 items-center justify-between rounded-lg bg-border-100 px-4"
    >
      <div className="flex flex-col">
        <p className="font-regular text-base text-gray">{props.title}</p>
      </div>
      <img src="/icons/ic_calendar_gray.png" alt="" className="h-5 w-5" />
    </button>
  );
};

export default TransactionDatePicker;
