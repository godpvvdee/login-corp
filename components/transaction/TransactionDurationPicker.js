import React, { useState } from "react";

const TransactionDurationPicker = (props) => {
  const [selectedIndex, setSelectedIndex] = useState(props.selectedIndex);

  return (
    <div className="flex gap-5">
      <button
        onClick={() => {
          setSelectedIndex(0);
          props.onClick(0);
        }}
        className="flex"
      >
        {selectedIndex === 0 ? (
          <img
            src="/icons/ic_check_circle.png"
            className="mr-2 h-5 w-5"
            alt=""
          />
        ) : (
          <div className="mr-2 h-5 w-5 rounded-full bg-border-200" />
        )}
        <p className="font-semibold text-base text-black">7 хоног</p>
      </button>
      <button
        onClick={() => {
          setSelectedIndex(1);
          props.onClick(1);
        }}
        className="flex"
      >
        {selectedIndex === 1 ? (
          <img
            src="/icons/ic_check_circle.png"
            className="mr-2 h-5 w-5"
            alt=""
          />
        ) : (
          <div className="mr-2 h-5 w-5 rounded-full bg-border-200" />
        )}
        <p className="font-semibold text-base text-black">Нэг сар</p>
      </button>
      <button
        onClick={() => {
          setSelectedIndex(2);
          props.onClick(2);
        }}
        className="flex"
      >
        {selectedIndex === 2 ? (
          <img
            src="/icons/ic_check_circle.png"
            className="mr-2 h-5 w-5"
            alt=""
          />
        ) : (
          <div className="mr-2 h-5 w-5 rounded-full bg-border-200" />
        )}
        <p className="font-semibold text-base text-black">Хагас жил</p>
      </button>
    </div>
  );
};

export default TransactionDurationPicker;
