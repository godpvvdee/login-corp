import React from "react";
import Link from "next/link";
const TransactionBatchWaitingTable = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  return (
    <>
      <div className="hidden md:block">
        {props.isReview ? (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="flex w-full py-3 px-4 md:px-7">
              <div className="w-1/4">
                <p className="font-bold text-xs text-gray">Шивсэн</p>
              </div>
              <div className="w-1/4">
                <p className="font-bold text-xs text-gray">Хаанаас</p>
              </div>
              <div className="w-1/4">
                <p className="font-bold text-xs text-gray">Шилжүүлгийн тоо</p>
              </div>
              <div className="w-1/4 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <Link
                  href={{
                    pathname: "/batch-review",
                    state: {
                      batchInfo: item,
                    },
                  }}
                >
                  <div
                    key={index}
                    className="flex min-h-[72px] w-full items-center bg-white px-4 hover:bg-border-100/40 md:px-7"
                  >
                    <div className="w-1/4">
                      <p className="font-regular text-base text-black">
                        {item.wrote_last_name + " " + item.wrote_first_name}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                    <div className="w-1/4">
                      <p className="font-regular text-base text-black">
                        {item.from_account_num + " " + item.currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        Илгээх данс
                      </p>
                    </div>
                    <div className="w-1/4">
                      <p className="font-regular text-base text-black">
                        {item.count}
                      </p>
                    </div>
                    <div className="w-1/4 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter
                          .format(item.total_amount)
                          .replaceAll("$", "") +
                          " " +
                          item.currency}
                      </p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
        ) : (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="flex w-full py-3 px-4 md:px-7">
              <div className="w-1/5">
                <p className="font-bold text-xs text-gray">Шивсэн</p>
              </div>
              <div className="w-1/5">
                <p className="font-bold text-xs text-gray">Хянасан</p>
              </div>
              <div className="w-1/5">
                <p className="font-bold text-xs text-gray">Хаанаас</p>
              </div>
              <div className="w-1/5">
                <p className="font-bold text-xs text-gray">Шилжүүлгийн тоо</p>
              </div>
              <div className="w-1/5 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <Link
                  href={{
                    pathname: "/batch-confirm",
                    state: {
                      batchInfo: item,
                    },
                  }}
                >
                  <div
                    key={index}
                    className="flex h-[72px] w-full items-center bg-white px-4 hover:bg-border-100/40 md:px-7"
                  >
                    <div className="w-1/5">
                      <p className="font-regular text-base text-black">
                        {item.wrote_last_name + " " + item.wrote_first_name}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                    <div className="w-1/5">
                      <p className="font-regular text-base text-black">
                        {item.reviewed_last_name +
                          " " +
                          item.reviewed_first_name}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.reviewed_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                    <div className="w-1/5">
                      <p className="font-regular text-base text-black">
                        {item.from_account_num + " " + item.currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        Илгээх данс
                      </p>
                    </div>
                    <div className="w-1/5">
                      <p className="font-regular text-base text-black">
                        {item.count}
                      </p>
                    </div>
                    <div className="w-1/5 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter
                          .format(item.total_amount)
                          .replaceAll("$", "") +
                          " " +
                          item.currency}
                      </p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
        )}
      </div>
      <div className="block md:hidden">
        {props.isReview ? (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="flex w-full py-3 px-4 md:px-7">
              <div className="w-1/2">
                <p className="font-bold text-xs text-gray">Шивсэн</p>
              </div>

              <div className="w-1/2 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <Link
                  href={{
                    pathname: "/batch-review",
                    state: {
                      batchInfo: item,
                    },
                  }}
                >
                  <div
                    key={index}
                    className="flex min-h-[72px] w-full items-center bg-white px-4 hover:bg-border-100/40 md:px-7"
                  >
                    <div className="w-1/2">
                      <p className="font-regular text-base text-black">
                        {item.wrote_last_name + " " + item.wrote_first_name}
                      </p>
                    </div>

                    <div className="w-1/2 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter
                          .format(item.total_amount)
                          .replaceAll("$", "") +
                          " " +
                          item.currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
        ) : (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="flex w-full py-3 px-4 md:px-7">
              <div className="w-1/2">
                <p className="font-bold text-xs text-gray">Шивсэн</p>
              </div>

              <div className="w-1/2 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <Link
                  href={{
                    pathname: "/batch-confirm",
                    state: {
                      batchInfo: item,
                    },
                  }}
                >
                  <div
                    key={index}
                    className="flex h-[72px] w-full items-center bg-white px-4 hover:bg-border-100/40 md:px-7"
                  >
                    <div className="w-1/2">
                      <p className="font-regular text-base text-black">
                        {item.wrote_last_name + " " + item.wrote_first_name}
                      </p>
                    </div>

                    <div className="w-1/2 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter
                          .format(item.total_amount)
                          .replaceAll("$", "") +
                          " " +
                          item.currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default TransactionBatchWaitingTable;
