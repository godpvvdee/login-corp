import React from "react";
import Link from "next/link";

const TransactionCheckTable = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const getBankIcon = (data) => {
    var temp = props.banksList.filter((item) => item.id === data);
    if (temp.length === 0) {
      return (
        <img src={`/icons/banks/bank_39.png`} className="mr-2 h-3 w-3" alt="" />
      );
    } else {
      return (
        <img
          src={`/icons/banks/bank_${temp[0].id}.png`}
          className="mr-2 h-3 w-3"
          alt=""
        />
      );
    }
  };

  return (
    <>
      <div
        className="hidden flex-col md:flex "
        style={{ maxHeight: "calc(100vh - 280px)" }}
      >
        <div className="flex w-full px-7 py-3">
          <div className="w-20"></div>
          <div className="w-1/6">
            <p className="font-bold text-xs text-gray">Огноо</p>
          </div>
          <div className="w-1/6">
            <p className="font-bold text-xs text-gray">Төрөл</p>
          </div>
          <div className="w-1/6">
            <p className="font-bold text-xs text-gray">Хаанаас</p>
          </div>
          <div className="w-1/6">
            <p className="font-bold text-xs text-gray">Хаашаа</p>
          </div>
          <div className="w-1/6">
            <p className="font-bold text-xs text-gray">Утга</p>
          </div>
          <div className="w-1/6 text-right">
            <p className="font-bold text-xs text-gray">Нийт дүн</p>
          </div>
        </div>
        <div className="min-h-[1px] bg-tableBorder" />
        <div className="flex flex-col overflow-scroll">
          {props.data.map((item, index) => (
            <Link
              key={index}
              href={{
                pathname: "/transaction-check-details",
                state: {
                  transaction: item,
                },
              }}
            >
              <div
                key={index}
                className="flex h-[72px] w-full items-center bg-white px-4 hover:bg-border-100/40 md:px-7"
              >
                <img
                  src={
                    item.part_tran_type === "D"
                      ? "/icons/ic_outcome.png"
                      : "/icons/ic_income.png"
                  }
                  alt=""
                  className="mr-7 h-10 w-10"
                />
                <div className="w-1/6">
                  <p className="font-regular text-base text-black">
                    {item.part_tran_type === "D" ? "Зарлага" : "Орлого"}
                  </p>
                  <p className="font-regular text-xs text-gray">
                    {item.pstd_date.replaceAll("T", " ").replaceAll("Z", "")}
                  </p>
                </div>
                <div className="w-1/6">
                  <p className="font-regular text-base text-black">
                    {item.tran_type}
                  </p>
                </div>
                <div className="w-1/6">
                  <p className="font-regular text-base text-black">
                    {item.from_acc + " " + item.acct_crncy_code}
                  </p>
                  <p className="font-regular text-xs text-gray">
                    Харилцах данс
                  </p>
                </div>
                <div className="w-1/6">
                  <div className="flex items-center">
                    {getBankIcon(item.bankid)}
                    <p className="font-regular text-base text-black">
                      {item.received_acc === null
                        ? "Шимтгэл"
                        : item.received_acc +
                          " " +
                          item.received_acct_crncy_code}
                    </p>
                  </div>
                  <p className="font-regular text-xs text-gray">
                    {item.received_acc_name}
                  </p>
                </div>
                <div className="w-1/6 overflow-hidden whitespace-nowrap">
                  <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
                    {item.remarks}
                  </p>
                </div>
                <div className="w-1/6 text-right">
                  <p
                    className={`font-regular text-base ${
                      item.part_tran_type === "D"
                        ? "text-black"
                        : "text-primary"
                    }`}
                  >
                    {item.part_tran_type === "D"
                      ? "-" +
                        formatter.format(item.tran_amt).replaceAll("$", "") +
                        " " +
                        item.received_acct_crncy_code
                      : "+" +
                        formatter.format(item.tran_amt).replaceAll("$", "") +
                        " " +
                        item.received_acct_crncy_code}
                  </p>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
      <div className="md:hidden ">
        {props.data.map((item, index) => (
          <div className="flex justify-between px-[18px] py-[10px]">
            <div className="flex flex-col ">
              <p className="font-regular text-base text-black">
                {item.part_tran_type === "D" ? "Зарлага" : "Орлого"}
              </p>
              <p className="font-regular text-xs text-gray">
                {item.received_acc_name}
              </p>
            </div>

            <div className="flex flex-col text-right">
              <p
                className={`font-regular text-base ${
                  item.part_tran_type === "D" ? "text-black" : "text-primary"
                }`}
              >
                {item.part_tran_type === "D"
                  ? "-" +
                    formatter.format(item.tran_amt).replaceAll("$", "") +
                    " " +
                    item.received_acct_crncy_code
                  : "+" +
                    formatter.format(item.tran_amt).replaceAll("$", "") +
                    " " +
                    item.received_acct_crncy_code}
              </p>
              <p className="font-regular text-xs text-gray">
                {item.pstd_date.replaceAll("T", " ").replaceAll("Z", "")}
              </p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default TransactionCheckTable;
