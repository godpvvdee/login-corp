import Axios from "axios";
import React, { useState } from "react";

const TransactionBankPicker = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const bankPickerHandler = (data) => {
    setIsOpen(false);
    props.onChange(data);
  };

  const imageOnErrorHandler = (event) => {
    event.currentTarget.src = "/icons/banks/bank_95.png";
  };

  return (
    <>
      <button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className={`flex h-13 w-full items-center justify-between rounded-lg bg-border-100 px-4 ${
          isOpen ? "mb-0" : "mb-2"
        }`}
      >
        <div className="flex items-center">
          <img
            src={`/icons/banks/bank_${props.selectedBank.id}.png`}
            className="mr-4 h-8 w-8"
            alt=""
            onError={imageOnErrorHandler}
          />
          <p className="mt-[-1px] font-regular text-base text-black">
            {props.selectedBank.bankname}
          </p>
        </div>
        <img
          src={
            isOpen
              ? "/icons/ic_menu_picker_green.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          className="ml-4 h-5 w-5"
          alt=""
        />
      </button>
      {isOpen ? (
        <div className="rounded-lg bg-white">
          {props.banksList.map((item, index) => (
            <button
              key={index}
              onClick={() => {
                bankPickerHandler(item);
              }}
              className="group flex h-13 w-full items-center bg-white px-4 hover:bg-primary"
            >
              <img
                src={`/icons/banks/bank_${item.id}.png`}
                className="mr-4 h-8 w-8"
                onError={imageOnErrorHandler}
                alt=""
              />
              <p className="mt-[-1px] text-left font-regular text-base text-black group-hover:text-white">
                {item.bankname}
              </p>
            </button>
          ))}
        </div>
      ) : null}
    </>
  );
};

export default TransactionBankPicker;
