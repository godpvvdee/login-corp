import React from "react";

const TransactionTitle = (props) => {
  return (
    <div className="mb-5 flex">
      <div
        className={`mt-1 mr-6 h-5 w-[2px] rounded-lg ${
          props.type === "D" ? "bg-pink" : "bg-primary"
        }`}
      />
      <div>
        <p
          className={`font-bold text-px18 ${
            props.type === "D" ? "text-pink" : "text-primary"
          }`}
        >
          {props.type === "D" ? "Зарлага" : "Орлого"}
        </p>
      </div>
    </div>
  );
};

export default TransactionTitle;
