import React from "react";
import ReactTooltip from "react-tooltip";
import UserTypeGetter from "../UserTypeGetter";

const TransactionWaitingTable = (props) => {
  const getTypeName = (data) => {
    switch (data) {
      case "INTRABANK":
        return "Банк доторх";
      case "OWN_ACCOUNT":
        return "Өөрийн данс руу";
      case "LOAN_PAYMENT":
        return "Зээлийн төлөлт";
      case "INTERBANK":
        return "Банк хооронд";
      default:
        return data;
    }
  };

  const getBankIcon = (data) => {
    if (data !== null) {
      var temp = props.banksList.filter((item) => item.id === data);

      if (temp.length === 0) {
        return (
          <img
            src={`/icons/banks/bank_39.png`}
            className="mr-2 h-3 w-3"
            alt=""
          />
        );
      } else {
        return (
          <img
            src={`/icons/banks/bank_${temp[0].id}.png`}
            className="mr-2 h-3 w-3"
            alt=""
          />
        );
      }
    } else {
      return (
        <img src={`/icons/banks/bank_39.png`} className="mr-2 h-3 w-3" alt="" />
      );
    }
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return (
    <>
      <div className="hidden md:block">
        {props.isReview ? (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="ml-[2px] flex w-full py-3 px-4 md:px-7">
              <button
                onClick={() => {
                  props.onSelectAll();
                }}
                className="w-9"
              >
                <img
                  src={
                    props.isAll
                      ? "/icons/ic_transaction_check.png"
                      : "/icons/ic_transaction_uncheck.png"
                  }
                  alt=""
                  className="h-4.5 w-4.5"
                />
              </button>
              <div className="w-1/6">
                <p className="font-bold text-xs text-gray">Шивсэн</p>
              </div>
              <div className="w-1/6">
                <p className="font-bold text-xs text-gray">Төрөл</p>
              </div>
              <div className="w-1/6">
                <p className="font-bold text-xs text-gray">Хаанаас</p>
              </div>
              <div className="w-1/6">
                <p className="font-bold text-xs text-gray">Хаашаа</p>
              </div>
              <div className="w-1/6">
                <p className="font-bold text-xs text-gray">Утга</p>
              </div>
              <div className="w-1/6 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <div
                  className={`flex min-h-[72px] w-full items-center  border-l-2 bg-white px-7 hover:bg-border-100/40 ${
                    item.isSelected
                      ? "border-l-primary"
                      : "border-l-white hover:border-l-border-100/40"
                  }`}
                >
                  <div className="w-9">
                    {props.isReview &&
                      (UserTypeGetter(item.permission).review ? (
                        <button
                          onClick={() => {
                            props.onTransactionSelect(index);
                          }}
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${
                            item.isSelected
                              ? "bg-[url('../public/icons/ic_transaction_check.png')]"
                              : "bg-[url('../public/icons/ic_transaction_uncheck.png')]"
                          }`}
                        />
                      ) : (
                        <button
                          data-tip="Танд уг шилжүүлгийг хянах эрх байхгүй байна."
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${"bg-[url('../public/icons/ic_transaction_uncheck.png')]"}`}
                        />
                      ))}
                  </div>
                  <div
                    onClick={() => {
                      props.onTransactionClick(item);
                    }}
                    className="flex w-full cursor-pointer items-center"
                  >
                    <div className="w-1/6">
                      <p className="font-regular text-base text-black">
                        {item.wrote_last_name + " " + item.wrote_first_name}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                    <div className="w-1/6">
                      <p className="font-regular text-base text-black">
                        {getTypeName(item.type)}
                      </p>
                    </div>
                    <div className="w-1/6">
                      <p className="font-regular text-base text-black">
                        {item.from_account_num + " " + item.from_currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        Харилцах данс
                      </p>
                    </div>
                    <div className="w-1/6">
                      <div className="flex items-center">
                        {getBankIcon(item.to_acct_bank_id)}

                        <p className="font-regular text-base text-black">
                          {item.to_account_num + " " + item.to_currency}
                        </p>
                      </div>
                      <p className="font-regular text-xs text-gray">
                        {item.to_account_name}
                      </p>
                    </div>
                    <div className="w-1/6 overflow-hidden whitespace-nowrap">
                      <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
                        {item.remarks}
                      </p>
                    </div>
                    <div className="w-1/6 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter.format(item.to_amount).replaceAll("$", "") +
                          " " +
                          item.from_currency}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <ReactTooltip
              place="top"
              type="light"
              backgroundColor="white"
              className="tooltip"
            />
          </div>
        ) : (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="ml-[2px] flex w-full py-3 px-4 md:px-7">
              <button
                onClick={() => {
                  props.onSelectAll();
                }}
                className="w-9"
              >
                <img
                  src={
                    props.isAll
                      ? "/icons/ic_transaction_check.png"
                      : "/icons/ic_transaction_uncheck.png"
                  }
                  alt=""
                  className="h-4.5 w-4.5"
                />
              </button>
              <div className="w-2/12">
                <p className="font-bold text-xs text-gray">Шивсэн</p>
              </div>
              <div className="w-2/12">
                <p className="font-bold text-xs text-gray">Хянасан</p>
              </div>
              <div className="w-2/12">
                <p className="font-bold text-xs text-gray">Төрөл</p>
              </div>
              <div className="w-2/12">
                <p className="font-bold text-xs text-gray">Хаанаас</p>
              </div>
              <div className="w-2/12">
                <p className="font-bold text-xs text-gray">Хаашаа</p>
              </div>
              <div className="w-2/12">
                <p className="font-bold text-xs text-gray">Утга</p>
              </div>
              <div className="w-1/12 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <div
                  className={`flex min-h-[72px] w-full items-center  border-l-2 bg-white px-7 hover:bg-border-100/40 ${
                    item.isSelected
                      ? "border-l-primary"
                      : "border-l-white hover:border-l-border-100/40"
                  }`}
                >
                  <div className="w-9">
                    {!props.isReview &&
                      (UserTypeGetter(item.permission).confirm ? (
                        <button
                          onClick={() => {
                            props.onTransactionSelect(index);
                          }}
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${
                            item.isSelected
                              ? "bg-[url('../public/icons/ic_transaction_check.png')]"
                              : "bg-[url('../public/icons/ic_transaction_uncheck.png')]"
                          }`}
                        />
                      ) : (
                        <button
                          data-tip="Танд уг шилжүүлгийг батлах эрх байхгүй байна."
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${"bg-[url('../public/icons/ic_transaction_uncheck.png')]"}`}
                        />
                      ))}
                  </div>
                  <div
                    onClick={() => {
                      props.onTransactionClick(item);
                    }}
                    className="flex w-full cursor-pointer items-center"
                  >
                    <div className="w-2/12">
                      <p className="font-regular text-base text-black">
                        {item.wrote_last_name + " " + item.wrote_first_name}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                    <div className="w-2/12">
                      <p className="font-regular text-base text-black">
                        {item.reviewed_last_name +
                          " " +
                          item.reviewed_first_name}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.reviewed_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                    <div className="w-2/12">
                      <p className="font-regular text-base text-black">
                        {getTypeName(item.type)}
                      </p>
                    </div>
                    <div className="w-2/12">
                      <p className="font-regular text-base text-black">
                        {item.from_account_num + " " + item.from_currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        Харилцах данс
                      </p>
                    </div>
                    <div className="w-2/12">
                      <div className="flex items-center">
                        {getBankIcon(item.to_acct_bank_id)}

                        <p className="font-regular text-base text-black">
                          {item.to_account_num + " " + item.to_currency}
                        </p>
                      </div>
                      <p className="font-regular text-xs text-gray">
                        {item.to_account_name}
                      </p>
                    </div>
                    <div className="w-2/12 overflow-hidden whitespace-nowrap">
                      <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
                        {item.remarks}
                      </p>
                    </div>
                    <div className="w-1/12 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter.format(item.to_amount).replaceAll("$", "") +
                          " " +
                          item.from_currency}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
              <ReactTooltip
                place="top"
                type="light"
                backgroundColor="white"
                className="tooltip"
              />
            </div>
          </div>
        )}
      </div>
      <div className="block md:hidden">
        {props.isReview ? (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="ml-[2px] flex w-full py-3 px-4 md:px-7">
              <button
                onClick={() => {
                  props.onSelectAll();
                }}
                className="w-9"
              >
                <img
                  src={
                    props.isAll
                      ? "/icons/ic_transaction_check.png"
                      : "/icons/ic_transaction_uncheck.png"
                  }
                  alt=""
                  className="h-4.5 w-4.5"
                />
              </button>
              <div className="w-1/2">
                <p className="font-bold text-xs text-gray">Төрөл/Шивсэн</p>
              </div>

              <div className="w-1/2 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <div
                  className={`flex min-h-[72px] w-full items-center  border-l-2 bg-white px-4 hover:bg-border-100/40 md:px-7 ${
                    item.isSelected
                      ? "border-l-primary"
                      : "border-l-white hover:border-l-border-100/40"
                  }`}
                >
                  <div className="w-9">
                    {props.isReview &&
                      (UserTypeGetter(item.permission).review ? (
                        <button
                          onClick={() => {
                            props.onTransactionSelect(index);
                          }}
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${
                            item.isSelected
                              ? "bg-[url('../public/icons/ic_transaction_check.png')]"
                              : "bg-[url('../public/icons/ic_transaction_uncheck.png')]"
                          }`}
                        />
                      ) : (
                        <button
                          data-tip="Танд уг шилжүүлгийг хянах эрх байхгүй байна."
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${"bg-[url('../public/icons/ic_transaction_uncheck.png')]"}`}
                        />
                      ))}
                  </div>
                  <div
                    onClick={() => {
                      props.onTransactionClick(item);
                    }}
                    className="flex w-full cursor-pointer items-center"
                  >
                    <div className="w-1/2">
                      <p className="font-regular text-base text-black md:text-base">
                        {getTypeName(item.type)}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_first_name}
                      </p>
                    </div>

                    <div className="w-1/2 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter.format(item.to_amount).replaceAll("$", "") +
                          " " +
                          item.from_currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <ReactTooltip
              place="top"
              type="light"
              backgroundColor="white"
              className="tooltip"
            />
          </div>
        ) : (
          <div
            className="flex flex-col"
            style={{ maxHeight: "calc(100vh - 280px)" }}
          >
            <div className="ml-[2px] flex w-full py-3 px-4 md:px-7">
              <button
                onClick={() => {
                  props.onSelectAll();
                }}
                className="w-9"
              >
                <img
                  src={
                    props.isAll
                      ? "/icons/ic_transaction_check.png"
                      : "/icons/ic_transaction_uncheck.png"
                  }
                  alt=""
                  className="h-4.5 w-4.5"
                />
              </button>
              <div className="w-1/2">
                <p className="font-bold text-xs text-gray">Төрөл/Хянасан</p>
              </div>

              <div className="w-1/2 text-right">
                <p className="font-bold text-xs text-gray">Нийт дүн</p>
              </div>
            </div>
            <div className="min-h-[1px] bg-tableBorder" />
            <div className="flex flex-col overflow-scroll">
              {props.data.map((item, index) => (
                <div
                  className={`flex min-h-[72px] w-full items-center  border-l-2 bg-white px-4 hover:bg-border-100/40 ${
                    item.isSelected
                      ? "border-l-primary"
                      : "border-l-white hover:border-l-border-100/40"
                  }`}
                >
                  <div className="w-9">
                    {!props.isReview &&
                      (UserTypeGetter(item.permission).confirm ? (
                        <button
                          onClick={() => {
                            props.onTransactionSelect(index);
                          }}
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${
                            item.isSelected
                              ? "bg-[url('../public/icons/ic_transaction_check.png')]"
                              : "bg-[url('../public/icons/ic_transaction_uncheck.png')]"
                          }`}
                        />
                      ) : (
                        <button
                          data-tip="Танд уг шилжүүлгийг батлах эрх байхгүй байна."
                          className={`h-4.5 w-4.5 bg-contain bg-no-repeat ${"bg-[url('../public/icons/ic_transaction_uncheck.png')]"}`}
                        />
                      ))}
                  </div>
                  <div
                    onClick={() => {
                      props.onTransactionClick(item);
                    }}
                    className="flex w-full cursor-pointer items-center"
                  >
                    <div className="w-1/2">
                      <p className="font-regular text-base text-black">
                        {getTypeName(item.type)}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_first_name}
                      </p>
                    </div>

                    <div className="w-1/2 text-right">
                      <p className="font-regular text-base text-black">
                        {formatter.format(item.to_amount).replaceAll("$", "") +
                          " " +
                          item.from_currency}
                      </p>
                      <p className="font-regular text-xs text-gray">
                        {item.wrote_date
                          .replaceAll("T", " ")
                          .replaceAll("Z", "")}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
              <ReactTooltip
                place="top"
                type="light"
                backgroundColor="white"
                className="tooltip"
              />
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default TransactionWaitingTable;
