import React, { useState } from "react";

const TransactionFilterPicker = (props) => {
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  const [filterList, setFilterList] = useState({
    intra: false,
    between: false,
    credit: false,
    debit: false,
  });
  return (
    <>
      <button
        onClick={() => setIsFilterOpen(!isFilterOpen)}
        className="mb-2 flex h-13 w-64 items-center justify-between rounded-lg bg-border-100 px-4"
      >
        <div className="flex flex-col">
          <p className="font-regular text-base text-gray">Шүүлт хийх</p>
        </div>
        <img
          src={
            isFilterOpen
              ? "/icons/ic_menu_picker_green.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          alt=""
          className="h-5 w-5"
        />
      </button>
      {isFilterOpen ? (
        <div className="absolute z-10 flex w-64 flex-col gap-5 rounded-lg bg-white p-4">
          <p className="font-bold text-xs text-black">Шилжүүлгийн төрөл</p>
          <button
            onClick={() => {
              setFilterList((prevState) => ({
                ...prevState,
                between: !prevState.between,
              }));
              props.onFilter(filterList, "between");
            }}
            className="flex items-center"
          >
            <img
              src={
                filterList.between
                  ? "/icons/ic_check_circle.png"
                  : "/icons/ic_check_circle_gray.png"
              }
              className="mr-4 h-5 w-5"
              alt=""
            />
            <p className="font-semibold text-xs text-black">
              Өөрийн данс хооронд
            </p>
          </button>
          <button
            onClick={() => {
              setFilterList((prevState) => ({
                ...prevState,
                intra: !prevState.intra,
              }));
              props.onFilter(filterList, "intra");
            }}
            className="flex items-center"
          >
            <img
              src={
                filterList.intra
                  ? "/icons/ic_check_circle.png"
                  : "/icons/ic_check_circle_gray.png"
              }
              className="mr-4 h-5 w-5"
              alt=""
            />
            <p className="font-semibold text-xs text-black">Бусдын данс руу</p>
          </button>
          <p className="font-bold text-xs text-black">Орлого, зарлага</p>
          <button
            onClick={() => {
              setFilterList((prevState) => ({
                ...prevState,
                credit: !prevState.credit,
              }));
              props.onFilter(filterList, "credit");
            }}
            className="flex items-center"
          >
            <img
              src={
                filterList.credit
                  ? "/icons/ic_check_circle.png"
                  : "/icons/ic_check_circle_gray.png"
              }
              className="mr-4 h-5 w-5"
              alt=""
            />
            <p className="font-semibold text-xs text-black">Орлого</p>
          </button>
          <button
            onClick={() => {
              setFilterList((prevState) => ({
                ...prevState,
                debit: !prevState.debit,
              }));
              props.onFilter(filterList, "debit");
            }}
            className="flex items-center"
          >
            <img
              src={
                filterList.debit
                  ? "/icons/ic_check_circle.png"
                  : "/icons/ic_check_circle_gray.png"
              }
              className="mr-4 h-5 w-5"
              alt=""
            />
            <p className="font-semibold text-xs text-black">Зарлага</p>
          </button>
        </div>
      ) : null}
    </>
  );
};

export default TransactionFilterPicker;
