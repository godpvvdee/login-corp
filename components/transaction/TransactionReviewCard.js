import React, { useState } from "react";
import DetailCard from "../main/DetailCard";

const TransactionReviewCard = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [transaction] = useState(props.transaction);
  const [transactionFees] = useState(props.transactionFees);
  const [selectedBank] = useState(props.selectedBank);

  const getTransactionFee = () => {
    switch (transaction.type) {
      case "OWN_ACCOUNT":
        return transactionFees.own_acct + "₮";
      case "INTERBANK":
        if (transaction.from_amount < 5000000) {
          return transactionFees.sh + "₮";
        } else {
          return transactionFees.rtgs + "₮";
        }
      case "INTRABANK":
        return transactionFees.intrabank + "₮";
      case "LOAN_PAYMENT":
        return transactionFees.intrabank + "₮";
      default:
        return "0₮";
    }
  };

  const getTypeName = (data) => {
    switch (data) {
      case "INTRABANK":
        return "Банк доторх";
      case "OWN_ACCOUNT":
        return "Өөрийн данс руу";
      case "LOAN_PAYMENT":
        return "Зээлийн төлөлт";
      case "INTERBANK":
        return "Банк хооронд";
      default:
        return data;
    }
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return (
    <>
      <button
        onClick={() => setIsOpen(!isOpen)}
        className={`group h-13 justify-between bg-border-100 px-4 hover:bg-primary ${
          isOpen && "bg-primary"
        } flex items-center rounded-lg`}
      >
        <p
          className={`font-semibold text-base text-black ${
            isOpen && "text-white"
          } group-hover:text-white`}
        >
          {"Шилжүүлэг " + (props.index + 1)}
        </p>
        <img
          src={
            isOpen
              ? "/icons/ic_menu_picker_true.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          className="h-5 w-5"
        />
      </button>
      {isOpen && (
        <div className="rounded-b-lg border border-border-100 bg-white p-4">
          <div className="flex flex-col gap-2">
            <DetailCard
              title="Харилцах данс"
              description={
                transaction.from_account_num + " " + transaction.from_currency
              }
            />
            <DetailCard
              title="Шилжүүлгийн төрөл"
              description={getTypeName(transaction.type)}
            />
            {transaction.type === "INTERBANK" ? (
              <div className="flex h-13 items-center rounded-lg bg-border-100 px-4">
                <img
                  src={`/icons/banks/${selectedBank.bankicon}.png`}
                  alt=""
                  className="mr-2 h-6 w-6"
                />
                <p className="mt-[-1px] font-regular text-base text-black">
                  {selectedBank.bankname}
                </p>
              </div>
            ) : (
              <div className="flex h-13 items-center rounded-lg bg-border-100 px-4">
                <img
                  src="/icons/ic_mbank_small.png"
                  alt=""
                  className="mr-2 h-6 w-6"
                />
                <p className="mt-[-1px] font-regular text-base text-black">
                  М банк
                </p>
              </div>
            )}
            <DetailCard title="Нэр" description={transaction.to_account_name} />
            <div className="flex gap-2">
              <DetailCard
                title="Хүлээн авах дүн"
                description={formatter
                  .format(transaction.from_amount)
                  .replaceAll("$", "")}
              />

              <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                <p className="font-regular text-xs text-gray">Валют</p>
                <p className="mt-[-1px] font-regular text-base text-black">
                  {transaction.from_currency}
                </p>
              </div>
            </div>
            <DetailCard title="Шимтгэл" description={getTransactionFee()} />
            <div className="flex w-full flex-col justify-center rounded-lg bg-border-100 px-4 py-2">
              <p className="font-regular text-xs text-gray">Шилжүүлгийн утга</p>
              <p className="mt-[-1px] break-all font-regular text-base leading-tight text-black">
                {transaction.remarks}
              </p>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default TransactionReviewCard;
