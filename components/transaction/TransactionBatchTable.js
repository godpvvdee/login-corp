import React, { useState } from "react";
import ReactTooltip from "react-tooltip";

const TransactionBatchTable = (props) => {
  const [tableData, setTableData] = useState(props.tableData);

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return (
    <div className="flex flex-col items-center rounded-lg bg-defaultBackground p-7">
      <div className={`mb-4 flex w-full`}>
        <div className="mx-4 h-5 w-5" />
        <div className="w-[70px]">
          <p className="font-bold text-xs text-gray">#</p>
        </div>
        <div className="w-1/12">
          <p className="font-bold text-xs text-gray">Банк</p>
        </div>
        <div className="w-2/12">
          <p className="font-bold text-xs text-gray">Хүлээн авах данс</p>
        </div>
        <div className="w-3/12">
          <p className="font-bold text-xs text-gray">Хүлээн авагчийн нэр</p>
        </div>
        <div className="w-2/12">
          <p className="font-bold text-xs text-gray">Дүн</p>
        </div>
        <div className="w-2/12">
          <p className="font-bold text-xs text-gray">Утга</p>
        </div>
      </div>
      {tableData.map((item, index) => (
        <div
          key={index}
          className={`flex w-full ${
            index % 2 === 0 ? "bg-border-100" : "bg-white"
          } h-13 items-center rounded-lg`}
        >
          {props.isChecked ? (
            item.isWrong ? (
              <div
                data-tip={
                  item.errorType === "account"
                    ? "Дансны дугаар, нэр зөрүүтэй байна."
                    : "Account frozen"
                }
              >
                <img
                  src="/icons/ic_warning.png"
                  alt=""
                  className="mx-4 h-5 w-5"
                />
              </div>
            ) : (
              <img
                src="/icons/ic_check_circle.png"
                alt=""
                className="mx-4 h-5 w-5"
              />
            )
          ) : (
            <div className="mx-4 h-5 w-5" />
          )}
          <div className="w-[70px]">
            <p className={`font-regular text-base text-black`}>{item.id}</p>
          </div>
          <div className="w-1/12 overflow-hidden whitespace-nowrap">
            <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
              {item.to_bank_name}
            </p>
          </div>
          <div className="w-2/12 overflow-hidden whitespace-nowrap">
            <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
              {item.to_account_num}
            </p>
          </div>
          <div className="w-3/12 overflow-hidden whitespace-nowrap">
            <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
              {item.to_account_name}
            </p>
          </div>
          <div className="w-2/12 overflow-hidden whitespace-nowrap">
            <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
              {formatter.format(item.to_amount).replaceAll("$", "") + " MNT"}
            </p>
          </div>
          <div className="w-2/12 overflow-hidden whitespace-nowrap">
            <p className="overflow-hidden text-ellipsis font-regular text-base text-black">
              {item.remark}
            </p>
          </div>
        </div>
      ))}
      <ReactTooltip
        place="top"
        type="light"
        backgroundColor="white"
        className="tooltip"
      />
    </div>
  );
};

export default TransactionBatchTable;
