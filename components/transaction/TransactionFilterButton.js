import React, { useState } from "react";

const TransactionFilterButton = (props) => {
  return (
    <button
      onClick={() => {
        props.onClick();
      }}
      className="flex items-center"
    >
      <img
        src={
          props.isChecked
            ? "/icons/ic_check_circle.png"
            : "/icons/ic_check_circle_gray.png"
        }
        alt=""
        className="mr-4 h-5 w-5"
      />
      <p className="mt-[-1px] text-left font-bold text-xs text-gray">
        {props.text}
      </p>
    </button>
  );
};

export default TransactionFilterButton;
