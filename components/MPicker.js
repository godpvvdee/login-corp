import React, { useState } from "react";
import MPickerItem from "./MPickerItem";

const MPicker = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(props.selectedItem);

  const itemHandler = (item) => {
    setSelectedItem(item);
    props.onSelect(item);
    setIsOpen(false);
  };

  return (
    <div className="relative">
      <button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className="flex h-12 w-full items-center justify-between rounded-lg bg-border-100 px-4"
      >
        <div className="inline flex-col text-left">
          <p
            className={`font-regular text-gray ${
              JSON.stringify(selectedItem) === "{}" ? "text-base" : "text-xs"
            }`}
          >
            {props.title}
          </p>
          <p className="mt-[-1px] overflow-hidden whitespace-nowrap font-regular text-base text-black md:w-80">
            {selectedItem.name}
          </p>
        </div>
        <img
          className="h-[18px] w-[18px]"
          src={
            isOpen
              ? "/icons/ic_menu_picker_green.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          alt=""
        />
      </button>
      {isOpen ? (
        <div className="z-[1] w-full overflow-hidden rounded-lg bg-white shadow-xl">
          {props.items.map((item, index) => (
            <MPickerItem key={index} item={item} onSelect={itemHandler} />
          ))}
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default MPicker;
