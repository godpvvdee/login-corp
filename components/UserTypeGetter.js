function UserTypeGetter(data) {
  var temp = {};
  temp.confirm = false;
  temp.write = false;
  temp.review = false;
  temp.nickname = false;
  temp.lien = false;
  temp.unlien = false;
  temp.transactionhistory = false;
  temp.freeze = false;
  temp.unfreeze = false;
  temp.description = false;
  switch (data.length) {
    case 1:
      if (data[0] === "1") {
        temp.confirm = true;
      }
    case 2:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
    case 3:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
    case 4:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
    case 5:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
    case 6:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
      if (data[5] === "1") {
        temp.lien = true;
      }
    case 7:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
      if (data[5] === "1") {
        temp.lien = true;
      }
      if (data[6] === "1") {
        temp.unlien = true;
      }
    case 8:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
      if (data[5] === "1") {
        temp.lien = true;
      }
      if (data[6] === "1") {
        temp.unlien = true;
      }
      if (data[7] === "1") {
        temp.transactionhistory = true;
      }
    case 9:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
      if (data[5] === "1") {
        temp.lien = true;
      }
      if (data[6] === "1") {
        temp.unlien = true;
      }
      if (data[7] === "1") {
        temp.transactionhistory = true;
      }
      if (data[8] === "1") {
        temp.freeze = true;
      }
    case 10:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
      if (data[5] === "1") {
        temp.lien = true;
      }
      if (data[6] === "1") {
        temp.unlien = true;
      }
      if (data[7] === "1") {
        temp.transactionhistory = true;
      }
      if (data[8] === "1") {
        temp.freeze = true;
      }
      if (data[9] === "1") {
        temp.unfreeze = true;
      }
    case 11:
      if (data[0] === "1") {
        temp.confirm = true;
      }
      if (data[1] === "1") {
        temp.write = true;
      }
      if (data[2] === "1") {
        temp.review = true;
      }
      if (data[4] === "1") {
        temp.nickname = true;
      }
      if (data[5] === "1") {
        temp.lien = true;
      }
      if (data[6] === "1") {
        temp.unlien = true;
      }
      if (data[7] === "1") {
        temp.transactionhistory = true;
      }
      if (data[8] === "1") {
        temp.freeze = true;
      }
      if (data[9] === "1") {
        temp.unfreeze = true;
      }
      if (data[10] === "1") {
        temp.description = true;
      }
    default:
  }
  return temp;
}

export default UserTypeGetter;
