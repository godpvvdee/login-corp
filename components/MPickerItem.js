import React from "react";

const MPickerItem = (props) => {
  return (
    <button
      onClick={() => props.onSelect(props.item)}
      className="group flex h-12 w-full flex-col justify-center bg-white px-4 text-left hover:bg-primary"
    >
      <p className="overflow mt-[-1px] font-regular text-base text-black group-hover:text-white">
        {props.item.name}
      </p>
    </button>
  );
};

export default MPickerItem;
