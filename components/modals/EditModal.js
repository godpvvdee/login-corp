import React, { useState } from "react";
import LargeTitle from "../main/LargeTitle";
import MInput from "../main/MInput";

const EditModal = (props) => {
  const [text, setText] = useState("");
  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-auto max-w-3xl">
        <div className="relative flex w-89 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none md:w-113">
          <div className="flex flex-col justify-between rounded-t py-9 pr-9">
            <div className="flex justify-between">
              <LargeTitle title={props.title} />
              <img
                onClick={() => {
                  props.onClose();
                }}
                src="/icons/ic_close_gray.png"
                className="h-5 w-5 cursor-pointer"
                alt=""
              />
            </div>
            <div className="pl-9 text-center">
              <MInput
                placeholder={props.placeholder}
                id="text"
                type="text"
                onChange={(e) => setText(e)}
              />
              <button
                disabled={text === ""}
                onClick={() => {
                  setText("");
                  props.onEdit(text);
                }}
                className="mx-auto mt-12 h-12 w-44 rounded-lg bg-black hover:opacity-90"
              >
                <p className="font-bold text-base text-white">Хадгалах</p>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default EditModal;
