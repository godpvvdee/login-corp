import React, { useState } from "react";
import IconMBankModal from "../../icons/IconMBankModal";
import MediumTitle from "../main/MediumTitle";
import MInput from "../main/MInput";

const PincodeCheckModal = (props) => {
  const [pincode, setPincode] = useState("");
  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-89 md:w-113">
        <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
          {props.isLoading ? (
            <div className="flex flex-col p-9">
              <IconMBankModal />
            </div>
          ) : (
            <div className="flex flex-col p-9">
              <div className="flex justify-between ">
                <MediumTitle title="Баталгаажуулах" />
                <img
                  onClick={() => {
                    props.onClose();
                  }}
                  src="/icons/ic_close_gray.png"
                  alt=""
                  className="mt-1 h-5 w-5 cursor-pointer"
                />
              </div>
              <MInput
                maxLength={6}
                placeholder="Гүйлгээний пин код"
                id="pin"
                type="pin"
                onEnter={() => {
                  if (pincode.length === 6) {
                    props.onClick();
                  }
                }}
                onChange={(e) => {
                  setPincode(e);
                  props.onChange(e);
                }}
              />
              <button
                disabled={pincode.length !== 6}
                onClick={() => {
                  props.onClick();
                }}
                className="group mx-auto mt-11 h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
              >
                <p className="font-bold text-base text-white group-disabled:text-gray">
                  Батлах
                </p>
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  ) : null;
};

export default PincodeCheckModal;
