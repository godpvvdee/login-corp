import React, { useRef, useState } from "react";
import { Link } from "react-router-dom";
import IconSignature from "../../icons/IconSignature";
import LargeTitle from "../main/LargeTitle";
import * as XLSX from "xlsx";

const TransactionGroupModal = (props) => {
  const [excelData, setExcelData] = useState(null);
  const [excelFile, setExcelFile] = useState(null);
  const inputFile = useRef(null);
  const [fileName, setFileName] = useState("");
  const [fileDate, setFileDate] = useState("");
  const [isError, setIsError] = useState(false);

  const handleSubmit = () => {
    let reader = new FileReader();
    reader.readAsArrayBuffer(inputFile.current.files[0]);
    reader.onload = (e) => {
      setExcelFile(e.target.result);
    };
    reader.onloadend = (e) => {
      excel(e.target.result);
    };
  };

  const excel = (exc) => {
    let data = [];
    if (exc !== null) {
      const workbook = XLSX.read(exc, { type: "buffer" });
      const worksheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[worksheetName];
      data = XLSX.utils.sheet_to_json(worksheet, {
        range: "A1:C1000",
      });
      for (var i = 0; i < data.length; i++) {
        if (
          data[i]["Хүлээн авагчийн нэр"] === undefined ||
          data[i]["Хүлээн авах данс"] === undefined ||
          data[i]["Гүйлгээний дүн"] === undefined
        ) {
          props.onError();
          data = [];
        }
      }
    } else {
      setExcelData(null);
    }
    props.onSubmit(data);
  };

  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-auto max-w-3xl">
        <div className="relative flex w-89 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none md:w-113">
          <div className="flex flex-col rounded-t p-9">
            <div className="flex justify-between">
              <LargeTitle title="Багц шилжүүлэг" />
              <img
                onClick={() => props.onCancel()}
                src="/icons/ic_close_gray.png"
                className="mt-2 h-5 w-5 cursor-pointer"
                alt=""
              />
            </div>
            <div className="flex justify-between">
              <Link
                to="/files/batch_template_new.xlsx"
                target="_blank"
                download
              >
                <button className="group flex h-45 w-45 flex-col items-center rounded-lg bg-border-100 pt-10 hover:bg-primary">
                  <div
                    className={`h-12 w-12 rounded-full bg-primary p-3.5 group-hover:bg-white`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className={`h-5 w-5 fill-white group-hover:fill-primary`}
                    >
                      <IconSignature />
                    </svg>
                  </div>
                  <p className="mt-5 font-bold text-base text-black group-hover:text-white">
                    Загвар татах
                  </p>
                </button>
              </Link>

              <button
                onClick={() => {
                  inputFile.current.click();
                }}
                className="group flex h-45 w-45 flex-col items-center rounded-lg bg-border-100 pt-10 hover:bg-primary"
              >
                <input
                  onChange={(e) => {
                    setFileName(e.target.files[0].name);
                    setFileDate(
                      e.target.files[0].lastModifiedDate
                        .toISOString()
                        .substring(0, 10)
                    );
                  }}
                  type="file"
                  id="file"
                  ref={inputFile}
                  style={{ display: "none" }}
                />
                <div
                  className={`h-12 w-12 rounded-full bg-primary p-3.5 group-hover:bg-white`}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className={`h-5 w-5 fill-white group-hover:fill-primary`}
                  >
                    <IconSignature />
                  </svg>
                </div>
                <p className="mt-5 font-bold text-base text-black group-hover:text-white">
                  Загвар оруулах
                </p>
              </button>
            </div>
            <div className="mt-7 flex h-13 items-center rounded-lg bg-border-100 px-4">
              <img src="/icons/ic_file.png" className="mr-4 h-5 w-4" alt="" />
              {fileName === "" || fileDate === "" ? (
                <p className="font-regular text-base text-black">
                  Файл байхгүй байна.
                </p>
              ) : (
                <div className="flex flex-col">
                  <p className="font-regular text-base text-black">
                    {fileName}
                  </p>
                  <p className="font-regular text-xs text-gray">
                    {fileDate.replaceAll("-", ".")}
                  </p>
                </div>
              )}
            </div>
            <button
              onClick={() => {
                handleSubmit();
              }}
              className="mx-auto mt-7 h-12 rounded-lg bg-black px-16 hover:opacity-90"
            >
              <p className="font-bold  text-base text-white">Хуулах</p>
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default TransactionGroupModal;
