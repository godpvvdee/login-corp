import React from "react";
import IconArrowRightSmall from "../../icons/IconArrowRightSmall";
import IconMBankModal from "../../icons/IconMBankModal";
import DetailCard from "../main/DetailCard";
import LargeTitle from "../main/LargeTitle";

const UserModal = (props) => {
  return props.isOpen ? (
    props.loading ? (
      <IconMBankModal />
    ) : (
      <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
        <div className="relative mx-auto w-auto max-w-3xl">
          <div className="relative flex w-89 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none md:w-113">
            <div className="flex flex-col justify-between gap-2 rounded-t p-9">
              <div className="flex justify-between">
                <LargeTitle title="Хувийн мэдээлэл" />
                <img
                  onClick={() => {
                    props.onClose();
                  }}
                  src="/icons/ic_close_gray.png"
                  alt=""
                  className="mt-1 h-5 w-5 cursor-pointer"
                />
              </div>
              <DetailCard title="Овог" description={props.userInfo.lastName} />
              <DetailCard title="Нэр" description={props.userInfo.firstName} />
              <DetailCard
                title="Регистрийн дугаар"
                description={props.userInfo.register}
              />
              <button
                onClick={() => props.onPhoneClick()}
                className="group flex h-13 w-full items-center justify-between rounded-lg bg-border-100 px-4 hover:bg-primary"
              >
                <div className="flex flex-col text-left">
                  <p className="font-regular text-xs text-gray group-hover:text-white">
                    Гар утасны дугаар
                  </p>
                  <p className="mt-[-1px] font-regular text-base text-black group-hover:text-white">
                    {props.userInfo.phone}
                  </p>
                </div>
                <svg
                  className={`h-[10px] w-[10px] fill-border-150 group-hover:fill-white`}
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <IconArrowRightSmall />
                </svg>
              </button>
              <button
                onClick={() => props.onEmailClick()}
                className="group flex h-13 w-full items-center justify-between rounded-lg bg-border-100 px-4 hover:bg-primary"
              >
                <div className="flex flex-col text-left">
                  <p className="font-regular text-xs text-gray group-hover:text-white">
                    И-мэйл хаяг
                  </p>
                  <p className="mt-[-1px] font-regular text-base text-black group-hover:text-white">
                    {props.userInfo.email}
                  </p>
                </div>
                <svg
                  className={`h-[10px] w-[10px] fill-border-150 group-hover:fill-white`}
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <IconArrowRightSmall />
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  ) : null;
};

export default UserModal;
