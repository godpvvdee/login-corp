import React from "react";
import WarningCard from "../main/WarningCard";

const TransactionWarning = (props) => {
  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-auto max-w-3xl">
        <div className="relative flex w-89 flex-col rounded-lg border-0 bg-white p-7 shadow-2xl outline-none focus:outline-none md:w-113">
          <WarningCard
            title="Анхааруулга"
            description="Ажлын өдрийн 16:00 цагаас хойш болон амралт, олон
нийтийн баяр ёслолын өдрүүдэд хийсэн 5 сая төгрөгөөс
дээш дүнтэй эсвэл бусад валютын банк хоорондын
гүйлгээ хүлээн авагчийн дансанд дараагийн ажлын өдөр
орохыг анхаарна уу. "
          />
          <button
            onClick={() => {
              props.onClick();
            }}
            className="group mx-auto mt-12 h-12 rounded-lg bg-black px-16 hover:opacity-90 disabled:bg-border-100"
          >
            <p className="font-bold text-base text-white group-disabled:text-gray">
              Дуусгах
            </p>
          </button>
        </div>
      </div>
    </div>
  ) : null;
};

export default TransactionWarning;
