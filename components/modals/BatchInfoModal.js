import React from "react";
import LargeTitle from "../main/LargeTitle";

var formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

const BatchInfoModal = (props) => {
  return (
    <>
      <div className="hidden md:block">
        {props.isOpen ? (
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
            <div className="relative mx-auto w-auto max-w-3xl">
              <div className="relative flex w-161 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
                <div className="flex flex-col justify-between rounded-t p-9">
                  <LargeTitle title="Багцын дэлгэрэнгүй жагсаалт" />
                  <div className="flex flex-col gap-2">
                    {props.transactions.map((item, index) => (
                      <div
                        key={index}
                        className="flex h-13 rounded-lg bg-border-100 px-4"
                      >
                        <div className="flex w-1/3 flex-col justify-center">
                          <p className="font-regular text-base text-black">
                            {item.to_account_num}
                          </p>
                          <p className="mt-[-1px] font-regular text-xs text-gray">
                            Хүлээн авах данс
                          </p>
                        </div>
                        <div className="flex w-1/3 flex-col justify-center">
                          <p className="font-regular text-base text-black">
                            {item.to_account_name}
                          </p>
                          <p className="mt-[-1px] font-regular text-xs text-gray">
                            Хүлээн авагчийн нэр
                          </p>
                        </div>
                        <div className="flex w-1/3 flex-col justify-center text-right">
                          <p className="font-regular text-base text-black">
                            {formatter
                              .format(item.to_amount)
                              .replaceAll("$", "")}
                          </p>
                          <p className="mt-[-1px] font-regular text-xs text-gray">
                            Мөнгөн дүн
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                  <button
                    onClick={() => {
                      props.onClose();
                    }}
                    className="mx-auto mt-18 h-12 w-52 rounded-lg bg-black"
                  >
                    <p className="font-bold text-base text-white">Хаах</p>
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
      <div className="block md:hidden">
        {props.isOpen ? (
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
            <div className="relative mx-auto w-auto max-w-3xl">
              <div className="relative flex w-82 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
                <div className="flex flex-col justify-between rounded-t p-9">
                  <LargeTitle title="Багцын дэлгэрэнгүй жагсаалт" />
                  <div className="flex flex-col gap-2">
                    {props.transactions.map((item, index) => (
                      <div
                        key={index}
                        className="flex h-16 rounded-lg bg-border-100 px-4"
                      >
                        <div className="flex w-1/2 flex-col justify-center">
                          <p className="font-regular text-base text-black">
                            {item.to_account_num}
                          </p>
                          <p className="mt-[-1px] font-regular text-xs text-gray">
                            {item.to_account_name}
                          </p>
                        </div>

                        <div className="flex w-1/2 flex-col justify-center text-right">
                          <p className="font-regular text-base text-black">
                            {formatter
                              .format(item.to_amount)
                              .replaceAll("$", "")}
                          </p>
                          <p className="mt-[-1px] font-regular text-xs text-gray">
                            Мөнгөн дүн
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                  <button
                    onClick={() => {
                      props.onClose();
                    }}
                    className="mx-auto mt-18 h-12 w-52 rounded-lg bg-black"
                  >
                    <p className="font-bold text-base text-white">Хаах</p>
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default BatchInfoModal;
