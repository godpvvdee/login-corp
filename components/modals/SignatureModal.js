import React, { useState } from "react";
import IconSignature from "../../icons/IconSignature";
import LargeTitle from "../main/LargeTitle";

const SignatureModal = (props) => {
  const [selectedButtonState, setSelectedButtonState] = useState(0);

  return props.isOpen ? (
    <>
      <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
        <div className="relative mx-auto w-auto max-w-3xl">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
            <div className="flex flex-col items-start justify-between rounded-t p-9">
              <LargeTitle title="Тоон гарын үсэг" />
              <div className="mt-4 flex gap-5">
                <button
                  onClick={() => setSelectedButtonState(1)}
                  className={`group flex h-40 w-40 flex-col items-center  justify-center rounded-lg hover:bg-primary ${
                    selectedButtonState === 1 ? "bg-primary" : "bg-border-100"
                  }`}
                >
                  <div
                    className={`h-12  w-12 rounded-full p-3.5 group-hover:bg-white ${
                      selectedButtonState === 1 ? "bg-white" : "bg-primary"
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className={`h-5 w-5 group-hover:fill-primary ${
                        selectedButtonState === 1
                          ? "fill-primary"
                          : "fill-white"
                      }`}
                    >
                      <IconSignature />
                    </svg>
                  </div>
                  <p
                    className={`mt-2 font-semibold text-base text-black group-hover:text-white ${
                      selectedButtonState === 1 ? "text-white" : "text-black"
                    }`}
                  >
                    Tridumkey
                  </p>
                </button>
                <button
                  onClick={() => setSelectedButtonState(2)}
                  className={`group flex h-40 w-40 flex-col items-center  justify-center rounded-lg hover:bg-primary ${
                    selectedButtonState === 2 ? "bg-primary" : "bg-border-100"
                  }`}
                >
                  <div
                    className={`h-12  w-12 rounded-full p-3.5 group-hover:bg-white ${
                      selectedButtonState === 2 ? "bg-white" : "bg-primary"
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className={`h-5 w-5 group-hover:fill-primary ${
                        selectedButtonState === 2
                          ? "fill-primary"
                          : "fill-white"
                      }`}
                    >
                      <IconSignature />
                    </svg>
                  </div>
                  <p
                    className={`mt-2 font-semibold text-base text-black group-hover:text-white ${
                      selectedButtonState === 2 ? "text-white" : "text-black"
                    }`}
                  >
                    Monpass
                  </p>
                </button>
              </div>

              <p
                onClick={props.onFail}
                className="mx-auto mt-9 cursor-pointer font-bold text-base text-gray"
              >
                Танд тоон гарын үсэг байхгүй юу?
              </p>
              <button
                onClick={() => {
                  setSelectedButtonState(0);
                  props.onAccept();
                }}
                disabled={selectedButtonState === 0}
                className="mx-auto mt-12 h-12 rounded-lg bg-black px-8 hover:opacity-90 disabled:bg-gray"
              >
                <p className="font-bold  text-base text-white">
                  Баталгаажуулах
                </p>
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : null;
};

export default SignatureModal;
