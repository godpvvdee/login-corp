import Axios from "axios";
import React, { useState } from "react";
import MInput from "../main/MInput";
import { toast } from "react-toastify";
import ErrorToast from "../main/ErrorToast";
import IconMBank from "../../icons/IconMBank";

const TransactionWriteSuccessModal = (props) => {
  const [isTemplateOpen, setIsTemplateOpen] = useState(false);
  const [templateName, setTemplateName] = useState("");
  const [loading, setLoading] = useState(false);
  const addTransactionTemplate = async () => {
    setLoading(true);
    await Axios.post("/addTransactionTemplate", {
      acct_ccy: props.toAccount.ccy,
      acct_name: props.toAccount.name,
      acct_num: props.toAccount.acct_num,
      template_name: templateName,
      bankid: props.selectedBank.id,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        props.onClick();
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const onClickHandler = () => {
    if (isTemplateOpen) {
      addTransactionTemplate();
    } else {
      props.onClick();
    }
  };

  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-auto max-w-3xl">
        <div className="relative flex w-89 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none md:w-113">
          {loading ? (
            <IconMBank />
          ) : (
            <div className="flex flex-col items-center justify-between rounded-t py-9 px-16">
              <div className="flex h-12 w-12 items-center justify-center rounded-full bg-primary">
                <img
                  src="/icons/ic_check_white.png"
                  className="h-6 w-6"
                  alt=""
                />
              </div>
              <img
                onClick={() => {
                  props.onClick();
                }}
                src="/icons/ic_close_gray.png"
                className="absolute right-4 top-4 mt-1 h-5 w-5 cursor-pointer"
                alt=""
              />
              <p className="mt-6 font-bold text-px16 text-black">Амжилттай</p>
              <p className="mt-5 text-center font-regular text-base text-black">
                Таны шилжүүлэг амжилттайгаар хянах шилжүүлгийн жагсаалтанд
                орлоо.
              </p>
              {props.type === "INTRA" ? (
                <button
                  onClick={() => setIsTemplateOpen(!isTemplateOpen)}
                  className="mt-9 flex h-13 w-full items-center justify-between rounded-lg bg-border-100 px-4"
                >
                  <p className="font-regular text-base text-black">
                    Загвар хадгалах
                  </p>
                  <div
                    className={`flex h-5 w-5 items-center justify-center rounded-full ${
                      isTemplateOpen ? "bg-primary" : "bg-border-200"
                    }`}
                  >
                    <div className="h-2 w-2 rounded-full bg-white"></div>
                  </div>
                </button>
              ) : null}
              {isTemplateOpen ? (
                <div className="mt-2 flex w-full flex-col gap-2">
                  <MInput
                    maxLength={40}
                    placeholder="Товч нэр"
                    id="name"
                    type="text"
                    onChange={(e) => setTemplateName(e)}
                  />
                  <div className="my-2 h-[1px] bg-border-100" />
                  <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                    <p className="font-regular text-base text-black">
                      {props.selectedBank.bankname}
                    </p>
                  </div>
                  <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                    <p className="font-regular text-xs text-gray">
                      Хүлээн авагчийн данс
                    </p>
                    <p className="mt-[-1px] font-regular text-base text-black">
                      {props.toAccount.acct_num}
                    </p>
                  </div>
                  <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                    <p className="font-regular text-xs text-gray">
                      Данс эзэмшигчийн нэр
                    </p>
                    <p className="mt-[-1px] font-regular text-base text-black">
                      {props.toAccount.name}
                    </p>
                  </div>
                </div>
              ) : null}
              <button
                disabled={isTemplateOpen && templateName === ""}
                onClick={() => {
                  onClickHandler();
                }}
                className="group mx-auto mt-12 h-12 rounded-lg bg-black px-16 hover:opacity-90 disabled:bg-border-100"
              >
                <p className="font-bold text-base text-white group-disabled:text-gray">
                  Дуусгах
                </p>
              </button>
            </div>
          )}
        </div>
      </div>
      <ErrorToast />
    </div>
  ) : null;
};

export default TransactionWriteSuccessModal;
