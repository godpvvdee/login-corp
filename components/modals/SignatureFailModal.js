import React from "react";

const SignatureFailModal = (props) => {
  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-auto max-w-3xl">
        <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
          <div className="flex flex-col items-center justify-between rounded-t py-9 px-16">
            <img className="h-12 w-12" src="/icons/ic_information.png" alt="" />

            <p className="mt-6 font-bold text-px16 text-black">
              Тоон гарын үсэг
            </p>
            <p className="mt-5 text-center font-regular text-base text-black">
              Та тоон гарын үсэггүй бол харилцах данс
              <br />
              онлайнаар нээх боломжгүй байна. Та{" "}
              <span className="font-bold text-primary">1800-2828</span>
              <br />
              хандана уу.
            </p>
            <button
              onClick={() => {
                props.onClick();
              }}
              className="mx-auto mt-12 h-12 rounded-lg bg-black px-16 hover:opacity-90"
            >
              <p className="font-bold  text-base text-white">Дуусгах</p>
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default SignatureFailModal;
