import React from "react";
import TransactionReviewResponseCard from "../../pages/transactions/TransactionReviewResponseCard";
import LargeTitle from "../main/LargeTitle";

const CheckboxTransactionModal = (props) => {
  return (
    props.isOpen && (
      <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
        <div className="relative mx-auto h-screen w-89 md:w-113">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white p-9 shadow-2xl outline-none focus:outline-none">
            <LargeTitle title="Хүлээгдэж буй шилжүүлэг" />
            <div className="flex flex-col gap-2">
              {props.transactions.map((item, index) => (
                <TransactionReviewResponseCard
                  transaction={item}
                  index={index}
                  isSuccess={item.response.code.substring(0, 1) === "2"}
                />
              ))}
            </div>
            <div className="mb-16" />
            <button
              onClick={() => {
                props.onClick();
              }}
              className="group mx-auto mt-11 h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
            >
              <p className="font-bold text-base text-white group-disabled:text-gray">
                Дуусгах
              </p>
            </button>
          </div>
        </div>
      </div>
    )
  );
};

export default CheckboxTransactionModal;
