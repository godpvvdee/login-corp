import React from "react";

const NotificationModal = (props) => {
  return props.isOpen ? (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-auto max-w-3xl">
        <div className="relative flex w-89 flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none md:w-113">
          <div className="flex flex-col items-center justify-between rounded-t py-9 px-16">
            <img
              src="/icons/ic_notification.png"
              className="h-12 w-12"
              alt=""
            />
            <p className="mt-6 font-bold text-px16 text-black">
              {props.notification.title}
            </p>
            <p className="mt-5 text-center font-regular text-base text-black">
              {props.notification.message}
            </p>
            <button
              onClick={() => {
                props.onClick();
              }}
              className="mx-auto mt-12 h-12 rounded-lg bg-black px-16 hover:opacity-90"
            >
              <p className="font-bold  text-base text-white">Хаах</p>
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default NotificationModal;
