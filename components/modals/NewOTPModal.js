import Axios from "axios";
import React, { useState } from "react";
import OtpInput from "react-otp-input";
import MediumTitle from "../main/MediumTitle";
import { toast } from "react-toastify";
import ErrorToast from "../main/ErrorToast";
import IconMBankModal from "../../icons/IconMBankModal";

const NewOTPModal = (props) => {
  const [loading, setLoading] = useState(false);
  const [counter, setCounter] = useState(0);
  const [otp, setOtp] = useState("");
  const [pageIndex, setPageIndex] = useState(0);
  const [selectedIndex, setSelectedIndex] = useState(1);
  const maskEmail = (email = "") => {
    const [name, domain] = email.split("@");
    const { length: len } = name;
    const maskedName = name[0] + "***" + name[len - 1];
    const maskedEmail = maskedName + "@" + domain;
    return maskedEmail;
  };

  const sendOTP = async () => {
    setLoading(true);
    await Axios.post("/sendOTP", {
      receiver:
        selectedIndex === 1 ? props.userInfo.email : props.userInfo.phone,
      type: selectedIndex === 1 ? "email" : "phone",
      username: props.userInfo.username,
      register: props.userInfo.register,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setCounter(60);
        setPageIndex(1);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const maskPhoneNum = (number) => {
    return (
      number.substring(0, 2) +
      "****" +
      number.substring(number.length - 2, number.length)
    );
  };

  React.useEffect(() => {
    counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
  }, [counter]);

  return (
    props.isOpen && (
      <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
        <div className="relative mx-auto w-89 md:w-113">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
            {pageIndex === 0 ? (
              loading ? (
                <IconMBankModal />
              ) : (
                <div className="flex flex-col p-9">
                  <div className="flex justify-between">
                    <MediumTitle title="Нэг удаагийн код авах" />
                    <img
                      onClick={() => {
                        props.onClose();
                        setOtp("");
                      }}
                      src="/icons/ic_close_gray.png"
                      className="mt-1 h-5 w-5 cursor-pointer"
                      alt=""
                    />
                  </div>
                  <button
                    onClick={() => setSelectedIndex(1)}
                    className="mb-2 flex h-13 w-full items-center justify-between rounded-lg bg-border-100 px-4"
                  >
                    <p className="font-regular text-base text-black">
                      {maskEmail(props.userInfo.email).toLowerCase()}
                    </p>
                    <div
                      className={`${
                        selectedIndex === 1 ? "bg-primary" : "bg-border-200"
                      } flex h-5 w-5 items-center justify-center rounded-full`}
                    >
                      <div className="h-2 w-2 rounded-full bg-white" />
                    </div>
                  </button>
                  <button
                    onClick={() => setSelectedIndex(2)}
                    className="flex h-13 w-full items-center  justify-between rounded-lg bg-border-100 px-4"
                  >
                    <p className="font-regular text-base text-black">
                      {maskPhoneNum(props.userInfo.phone)}
                    </p>
                    <div
                      className={`${
                        selectedIndex === 2 ? "bg-primary" : "bg-border-200"
                      } flex h-5 w-5 items-center justify-center rounded-full`}
                    >
                      <div className="h-2 w-2 rounded-full bg-white" />
                    </div>
                  </button>

                  <button
                    onClick={() => {
                      sendOTP();
                    }}
                    className="group mx-auto mt-20 h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
                  >
                    <p className="font-bold text-base text-white group-disabled:text-gray">
                      Код илгээх
                    </p>
                  </button>
                </div>
              )
            ) : (
              <div className="flex flex-col p-9">
                <div className="flex justify-between">
                  <MediumTitle title="Баталгаажуулах" />
                  <img
                    onClick={() => {
                      props.onClose();
                      setOtp("");
                    }}
                    src="/icons/ic_close_gray.png"
                    className="mt-1 h-5 w-5 cursor-pointer"
                    alt=""
                  />
                </div>
                <OtpInput
                  value={otp}
                  onChange={(data) => {
                    setOtp(data);
                  }}
                  numInputs={4}
                  containerStyle="outer__otp"
                  inputStyle="otp small"
                  focusStyle="otp__focused"
                />
                <div className="mb-7" />
                {counter != 0 ? (
                  <div className="mx-auto mb-5 flex h-10 w-24 items-center rounded-lg bg-border-100 text-center">
                    <p className="mx-auto font-regular text-xs text-black">
                      {counter} секунд
                    </p>
                  </div>
                ) : (
                  <p
                    onClick={() => {
                      sendOTP();
                    }}
                    className="justify-center px-20 text-center font-regular text-xs text-gray"
                  >
                    Мессеж ирээгүй?
                    <br />
                    <span className="cursor-pointer font-semibold text-primary">
                      Шинэ код авах
                    </span>
                  </p>
                )}
                <button
                  onClick={() => {
                    props.onSuccess({
                      otp: otp,
                      receiver:
                        selectedIndex === 1
                          ? props.userInfo.email
                          : props.userInfo.phone,
                      type: selectedIndex === 1 ? "email" : "phone",
                    });
                  }}
                  className="group mx-auto mt-10 h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
                >
                  <p className="font-bold text-base text-white group-disabled:text-gray">
                    Код илгээх
                  </p>
                </button>
              </div>
            )}
          </div>
        </div>
        <ErrorToast />
      </div>
    )
  );
};

export default NewOTPModal;
