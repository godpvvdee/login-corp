import Calendar from "react-calendar";
import React from "react";

const dateFormatter = (data) => {
  var date = new Date(data);
  var temp = "";
  temp = date.getFullYear() + "." + (date.getMonth() + 1) + " сар";
  return temp;
};

const weeknameFormatter = (data) => {
  var date = new Date(data);
  var temp = "";
  temp = date.toString().substring(0, 3);
  switch (temp) {
    case "Mon":
      return "Да";
    case "Tue":
      return "Мя";
    case "Wed":
      return "Лх";
    case "Thu":
      return "Пү";
    case "Fri":
      return "Ба";
    case "Sat":
      return "Бя";
    case "Sun":
      return "Ня";
    default:
      return "";
  }
};

const MCalendarPicker = (props) => {
  return (
    <div className="absolute mt-14 rounded-lg border border-border-100 bg-white p-2 shadow-md">
      <Calendar
        defaultValue={props.date}
        onClickDay={(date) => props.onClick(date)}
        minDetail="month"
        maxDate={new Date()}
        locale="mn-MN"
        minDate={props.isEnd ? props.start : new Date(2022, 0, 1)}
        formatShortWeekday={(locale, date) => weeknameFormatter(date)}
        nextLabel={
          <img src="/icons/ic_calendar_next.png" className="h-5 w-5" />
        }
        prevLabel={
          <img src="/icons/ic_calendar_prev.png" className="h-5 w-5" />
        }
        navigationLabel={({ date }) => `${dateFormatter(date)}`}
      />
    </div>
  );
};

export default MCalendarPicker;
