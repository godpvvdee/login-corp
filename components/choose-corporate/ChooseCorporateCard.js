import React from "react";

const ChooseCorporateCard = (props) => {
  return (
    <div onClick={props.onClick} className=" group cursor-pointer">
      <div className="flex h-45 w-40 flex-col items-center rounded-t-lg border border-border-100 bg-border-100 pt-9 group-hover:border-border-100 group-hover:bg-white">
        <div
          className={`mb-5 flex h-18 w-18 items-center justify-center rounded-full bg-primary`}
        >
          <p className=" font-bold text-xl text-white">
            {props.companyName.substring(0, 1)}
          </p>
        </div>
        <p className="justify-center text-center font-regular text-base text-black group-hover:font-semibold">
          {props.companyName}
        </p>
      </div>
      <div className="flex h-10 w-40 flex-col items-center justify-center rounded-b-lg bg-border-200 group-hover:bg-primary">
        <p className="mt-[-1px] font-semibold text-xs text-gray group-hover:text-white">
          {props.position}
        </p>
      </div>
    </div>
  );
};

export default ChooseCorporateCard;
