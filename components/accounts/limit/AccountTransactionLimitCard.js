import React, { useState } from "react";

const AccountTransactionLimitCard = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const getStatus = (data) => {
    switch (data) {
      case "pending":
        return (
          <p className="font-semibold text-base text-pastelYellow">
            Хүсэлт хянагдаж байна
          </p>
        );
      case "declined":
        return (
          <p className="font-semibold text-base text-pastelRed">Татгалзсан</p>
        );
      case "approved":
        return <p className="font-semibold text-base text-primary">Баталсан</p>;
    }
  };
  return (
    <div
      onClick={() => setIsOpen(!isOpen)}
      className="flex cursor-pointer flex-col rounded-lg bg-border-100"
    >
      <div className="flex items-center justify-between p-4">
        {getStatus(props.item.status)}
        <img
          src={
            isOpen
              ? "/icons/ic_menu_picker_green.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          className="h-[18px] w-[18px]"
          alt=""
        />
      </div>
      {isOpen ? (
        <div className="flex flex-col">
          <div className="mx-4 mb-4 h-[1px] bg-border-200" />
          <div className="mb-4 flex px-4">
            <div className="mr-6 flex flex-col">
              <p className="font-regular text-xs text-gray">Илгээсэн огноо</p>
              <p className="mt-[-1px] font-regular text-base text-black">
                {props.item.created_date.substring(0, 10)}
              </p>
            </div>
            <div className="flex flex-col">
              <p className="font-regular text-xs text-gray">Хүсэлт илгээсэн</p>
              <p className="mt-[-1px] font-regular text-base text-black">-</p>
            </div>
          </div>
          <div className="mb-4 flex flex-col px-4">
            <p className="font-regular text-xs text-gray">Хүсэлтийн төрөл</p>
            <p className="mt-[-1px] font-regular text-base text-black">
              {props.item.create_reason}
            </p>
          </div>
          <div className="mb-4 flex px-4">
            <div className="mr-6 flex flex-col">
              <p className="font-regular text-xs text-gray">Хуучин утга</p>
              <p className="mt-[-1px] font-regular text-base text-black">
                {props.item.create_reason ===
                "Нэг өдөрт хийх нийт зарлагын гүйлгээний давтамж"
                  ? props.item.prev_limit_count
                  : props.item.prev_limit_amount}
              </p>
            </div>
            <div className="flex flex-col">
              <p className="font-regular text-xs text-gray">Шинэ утга</p>
              <p className="mt-[-1px] font-regular text-base text-black">
                {props.item.create_reason ===
                "Нэг өдөрт хийх нийт зарлагын гүйлгээний давтамж"
                  ? props.item.limit_count
                  : props.item.limit_amount}
              </p>
            </div>
          </div>
          {props.item.status !== "pending" ? (
            <div className="rounded-b-lg border border-border-100 bg-white p-4">
              <div className="flex flex-col">
                <p className="font-regular text-xs text-gray">
                  Шийдвэрлэсэн огноо
                </p>
                <p className="mt-[-1px] font-regular text-base text-black">
                  {props.item.modified_date.substring(0, 10)}
                </p>
              </div>
            </div>
          ) : null}
        </div>
      ) : null}
    </div>
  );
};

export default AccountTransactionLimitCard;
