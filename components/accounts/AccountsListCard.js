import LargeTitle from "../main/LargeTitle";
import AccountCard from "./AccountCard";

const AccountsListCard = (props) => {
  return (
    <div className="rounded-lg bg-defaultBackground py-7 pr-7">
      <LargeTitle
        title="Харилцах данс"
        description="Start building your credit score."
      />
      <div className="grid gap-7 pl-7 md:grid-cols-2">
        {props.caList.map((item, index) =>
          item.currentAccount ? (
            <AccountCard
              caList={props.caList}
              key={index}
              account={item.currentAccount}
              type="MNT"
              approx=""
            />
          ) : (
            ""
          )
        )}
      </div>
    </div>
  );
};

export default AccountsListCard;
