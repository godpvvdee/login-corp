import React from "react";

const AccountCloseRequirementCard = (props) => {
  return (
    <div className="flex rounded-lg border border-border-100 bg-white py-5 px-4">
      <img className="mr-4 h-5 w-5" src="/icons/ic_check_circle.png" alt="" />
      <p className="font-regular text-xs text-black">{props.text}</p>
    </div>
  );
};

export default AccountCloseRequirementCard;
