import React from "react";
import Link from "next/link";
import IconArrowRight from "../../icons/IconArrowRight";

const AccountCard = (props) => {
  const formatBalance = (balance) => {
    const formattedBal = formatter.format(balance).replace("$", "").split(".");
    return (
      <p className="font-bold text-xl text-black group-hover:text-white">
        {formattedBal[0]}
        <span className="text-px18">.{formattedBal[1]}</span>
      </p>
    );
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const getCardIcon = (icon) => {
    // if (icon === "MNT") {
    //   return <img className="h-6" src="/icons/ic_tugrug.png" alt="" />;
    // } else if (icon === "MASTER") {
    //   return <img className="h-5" src="/icons/ic_mastercard.png" alt="" />;
    // } else {
    // }
    return (
      <div className="align-right absolute bottom-0 right-0 h-16 w-24 bg-[url('../public/icons/ic_card.png')] bg-cover group-hover:bg-[url('../public/icons/ic_card_hover.png')]" />
    );
  };
  return (
    <Link
      href={{
        pathname: "/account",
        state: {
          accNo: props.account.acct_num,
        },
      }}
    >
      <div className="group relative flex flex-col  rounded-lg border border-border-100 bg-defaultBackground pl-5 pt-5 transition duration-150 ease-out hover:bg-primary hover:ease-linear ">
        <div className="mb-6 flex items-center justify-between pr-5">
          <p className="font-regular text-xs text-black group-hover:text-white">
            {props.account.nickname === null
              ? props.account.product_name
              : props.account.nickname}
          </p>

          <svg
            className="h-[10px] w-[7px] fill-gray group-hover:fill-white"
            xmlns="http://www.w3.org/2000/svg"
          >
            <IconArrowRight />
          </svg>
        </div>
        {formatBalance(props.account?.positive_balance_amt)}
        <div className="flex">
          <p className="pb-5 font-regular text-base text-gray group-hover:text-white">
            {props.account.acct_num.slice(0, 4) +
              " " +
              props.account.acct_num.slice(4, props.account.acct_num.length) +
              " " +
              props.account.acct_ccy}
          </p>
          {/* {getCardIcon(props.type)} */}
        </div>
      </div>
    </Link>
  );
};

export default AccountCard;
