import React, { useState } from "react";
import SmallTitle from "../main/SmallTitle";

const StockRateCard = (props) => {
  const [selectedIndex, setSelectedIndex] = useState(1);
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return (
    <div className="h-80 rounded-lg  bg-defaultBackground p-7">
      <SmallTitle title="Үнэт цаас" />
      <div className="mb-4 h-9 rounded-lg bg-border-100">
        <button
          onClick={() => {
            setSelectedIndex(1);
          }}
          className={`group h-9 w-1/2 rounded-lg transition ease-in-out ${
            selectedIndex == 1
              ? "bg-primary text-white"
              : "bg-border-100 text-black"
          }`}
        >
          <p className="font-bold text-xs">I ангилал</p>
        </button>
        <button
          onClick={() => {
            setSelectedIndex(2);
          }}
          className={`group h-9 w-1/2 rounded-lg transition ease-in-out ${
            selectedIndex == 2
              ? "bg-primary text-white"
              : "bg-border-100 text-black"
          }`}
        >
          <p className="font-bold text-xs">II ангилал</p>
        </button>
      </div>
      <div className="mb-5 flex">
        <div className="w-1/3">
          <p className="font-semibold text-xs text-gray">Хувьцаа</p>
        </div>
        <div className="w-1/3">
          <p className="font-semibold text-xs text-gray">Ханш</p>
        </div>
        <div className="w-1/3">
          <p className="font-semibold text-xs text-gray">Өөрчлөлт</p>
        </div>
      </div>
      <div className="h-36 overflow-hidden overflow-y-scroll">
        <div className="flex flex-col gap-5">
          {props.firstList
            .filter((item) => item.company_index === selectedIndex)
            .map((item, index) => {
              return (
                <div key={index} className="flex">
                  <div className="flex w-1/3 ">
                    <img
                      className="mr-2 h-5 w-5"
                      src={
                        item.change === "0"
                          ? "/icons/ic_stock_neutral.png"
                          : item.change > 0
                          ? "/icons/ic_stock_up.png"
                          : "/icons/ic_stock_down.png"
                      }
                      alt=""
                    />
                    <p className="font-regular text-base text-black">
                      {item.ticker}
                    </p>
                  </div>
                  <div className="w-1/3">
                    <p className="font-regular text-base text-black">
                      {formatter.format(item.newprice).substring(1)}
                    </p>
                  </div>
                  <div className="w-1/3">
                    <p className="font-regular text-base text-black">
                      {item.change + " %"}
                    </p>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default StockRateCard;
