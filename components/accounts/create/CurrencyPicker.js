import CurrencyPickerItem from "./CurrencyPickerItem";
import React, { useState } from "react";

const CurrencyPicker = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedCurrency, setSelectedCurrency] = useState({});

  const currencyHandler = (data) => {
    setSelectedCurrency(data);
    props.onSelect(data);
    setIsOpen(false);
  };

  return (
    <>
      <button
        onClick={() => {
          setIsOpen(true);
        }}
        className="flex h-12 items-center justify-between rounded-lg bg-border-100 px-4 "
      >
        {selectedCurrency.value === undefined ? (
          <p className="font-regular text-base text-gray">Валют сонгох</p>
        ) : (
          <div className="flex items-center">
            <img className="mr-3 h-6 w-6" src={selectedCurrency.icon} alt="" />
            <p className="font-regular text-base text-black">
              {selectedCurrency.value}
            </p>
          </div>
        )}
        <img
          className="h-[18px] w-[18px]"
          src={isOpen ? "/icons/ic_picker_active.png" : "/icons/ic_picker.png"}
          alt=""
        />
      </button>
      {isOpen ? (
        <div className="overflow-hidden rounded-lg bg-white shadow-xl">
          {props.currencies.map((item, index) => (
            <CurrencyPickerItem
              key={index}
              currency={item}
              onSelect={currencyHandler}
            />
          ))}
        </div>
      ) : (
        <div></div>
      )}
    </>
  );
};

export default CurrencyPicker;
