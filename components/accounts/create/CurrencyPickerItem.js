const CurrencyPickerItem = (props) => {
  return (
    <button
      onClick={() => props.onSelect(props.currency)}
      className="group flex h-12 w-full items-center bg-white px-4 hover:bg-primary"
    >
      <img className="mr-3 h-6 w-6" src={props.currency.icon} alt="" />
      <p className="font-regular text-base text-black group-hover:text-white">
        {props.currency.value}
      </p>
    </button>
  );
};

export default CurrencyPickerItem;
