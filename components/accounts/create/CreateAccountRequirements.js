const CreateAccountRequirements = () => {
  return (
    <>
      <div className="my-4 h-px rounded-lg bg-border-100" />
      <div className="mb-9 flex flex-col gap-2">
        <div className="rounded-lg bg-border-100 px-4 py-2">
          <p className="font-regular text-xs text-gray">Бүтээгдэхүүний нэр</p>
          <p className="font-regular text-base text-black">Харилцах данс</p>
        </div>
        <div className="rounded-lg bg-border-100 px-4 py-2">
          <p className="font-regular text-xs text-gray">Жилийн хүү</p>
          <p className="font-regular text-base text-black">0.00%</p>
        </div>
        <div className="rounded-lg bg-border-100 px-4 py-2">
          <p className="font-regular text-xs text-gray">Доод үлдэгдэл</p>
          <p className="font-regular text-base text-black">20'000.00₮</p>
        </div>
        <div className="rounded-lg bg-border-100 px-4 py-2">
          <p className="font-regular text-xs text-gray">
            Данс хөтөлсний шимтгэл(сар бүр)
          </p>
          <p className="font-regular text-base text-black">1'000.00₮</p>
        </div>
      </div>
    </>
  );
};

export default CreateAccountRequirements;
