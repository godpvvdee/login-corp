import React, { useState } from "react";
import SmallTitle from "../../main/SmallTitle";
import AccountPickerItem from "./AccountPickerItem";

const AccountPicker = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedAccount, setSelectedAccount] = useState(props.caList[0]);

  const accountHandler = (data) => {
    setSelectedAccount(data);
    props.onSelect(data);
    setIsOpen(false);
  };

  return (
    <>
      {props.title === "" ? null : <SmallTitle title={props.title} />}
      <button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className="flex h-12 w-full items-center justify-between rounded-lg bg-border-100 px-4"
      >
        <div className="flex flex-col text-left">
          <p className="font-regular text-xs text-gray">
            {selectedAccount.currentAccount.acct_type}
          </p>
          <p className="mt-[-1px] font-regular text-base text-black">
            {selectedAccount.currentAccount.acct_num +
              " " +
              selectedAccount.currentAccount.acct_ccy}
          </p>
        </div>
        <img
          className="h-[18px] w-[18px]"
          src={
            isOpen
              ? "/icons/ic_menu_picker_green.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          alt=""
        />
      </button>
      {isOpen ? (
        <div className="overflow-hidden rounded-lg bg-white shadow-xl">
          {props.caList.map((item, index) => (
            <AccountPickerItem
              key={index}
              account={item}
              onSelect={accountHandler}
            />
          ))}
        </div>
      ) : (
        <div></div>
      )}
    </>
  );
};

export default AccountPicker;
