const CreateAccountDocumentViewer = (props) => {
  return (
    <>
      <div className="relative h-80 rounded-lg border border-border-100 bg-white text-center">
        <p className="mt-6 font-bold text-px18 text-black">
          М банк -нд хүсэлт гаргах нь
        </p>
        <p className="mt-7 font-regular text-xs text-black">
          <span className="font-bold">9050768</span> тоот регистрийн дугаартай
          <span className="font-bold"> Номин Трейд ХХК</span> -нд төгрөгийн
          харилцах данс нээх хүсэлтэй байна.
        </p>
        <button className="absolute bottom-4 right-4  h-10 w-10 rounded-lg bg-border-100 p-[10px]">
          <img src="/icons/ic_zoom.png" alt="" />
        </button>
      </div>
      <button
        onClick={() => {
          props.onAgreeClicked();
        }}
        className="mt-4 flex justify-between rounded-lg border border-border-100 p-4"
      >
        <p className="font-regular text-base text-black">
          Харилцах данс нээх албан тоот болон гэрээний нөхцөл зөвшөөрч байна
        </p>
        <div
          className={`h-5 w-5 rounded-full ${
            props.isAgreed ? "bg-primary" : "bg-border-100"
          }`}
        />
      </button>
    </>
  );
};

export default CreateAccountDocumentViewer;
