const CreateAccountDocumentsButton = (props) => {
  return (
    <div className="mb-4 flex h-12 rounded-lg bg-border-100">
      <button
        onClick={() => props.onClick(0)}
        className={`w-1/2 rounded-lg transition duration-300 ${
          props.selectedButton === 0 ? "bg-primary " : "bg-border-100"
        }`}
      >
        <p
          className={`font-semibold text-base ${
            props.selectedButton === 0 ? "text-white" : "text-black"
          }`}
        >
          Харилцах данс нээх албан тоот
        </p>
      </button>
      <button
        onClick={() => props.onClick(1)}
        className={`w-1/2 rounded-lg transition duration-300 ${
          props.selectedButton === 1 ? "bg-primary " : "bg-border-100"
        }`}
      >
        <p
          className={`font-semibold text-base ${
            props.selectedButton === 1 ? "text-white" : "text-black"
          }`}
        >
          Харилцах дансны гэрээний нөхцөл
        </p>
      </button>
    </div>
  );
};

export default CreateAccountDocumentsButton;
