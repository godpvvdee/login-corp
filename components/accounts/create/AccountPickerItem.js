const AccountPickerItem = (props) => {
  return (
    <button
      onClick={() => props.onSelect(props.account)}
      className="group flex h-12 w-full flex-col justify-center bg-white px-4 hover:bg-primary"
    >
      <p className="font-regular text-xs text-gray group-hover:text-white">
        {props.account.currentAccount.acct_type}
      </p>
      <p className="mt-[-1px] font-regular text-base text-black group-hover:text-white">
        {props.account.currentAccount.acct_num +
          " " +
          props.account.currentAccount.acct_ccy}
      </p>
    </button>
  );
};

export default AccountPickerItem;
