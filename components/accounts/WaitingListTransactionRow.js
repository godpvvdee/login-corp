const WaitingListTransactionRow = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  return (
    <div className="flex items-center">
      <button
        onClick={() => props.onClick()}
        className={`mr-7 h-5 w-5 bg-contain bg-no-repeat ${
          props.transaction.isSelected
            ? "bg-[url('../public/icons/ic_checkbox.png')]"
            : "bg-[url('../public/icons/ic_checkbox_false.png')]"
        }`}
      />
      <div className="flex w-1/4 flex-col">
        <p className="font-regular text-base text-black">
          {props.transaction.description}
        </p>
        <p className="font-regular text-xs text-gray">
          {props.transaction.date}
        </p>
      </div>
      <div className="flex w-1/4 flex-col">
        <p className="font-regular text-base text-black">
          {props.transaction.fromAccNo}
        </p>
        <p className="font-regular text-xs text-gray">
          {props.transaction.fromAccName}
        </p>
      </div>
      <div className="flex w-1/4 flex-col">
        <p className="font-regular text-base text-black">
          {props.transaction.toAccNo}
        </p>
        <p className="font-regular text-xs text-gray">
          {props.transaction.toAccName}
        </p>
      </div>
      <div className="flex w-1/4 flex-col text-right">
        <p className="font-semibold text-base text-black">
          {formatter.format(props.transaction.amount).substring(1)}
        </p>
        <p className="font-regular text-xs text-gray">
          {props.transaction.amountName}
        </p>
      </div>
    </div>
  );
};

export default WaitingListTransactionRow;
