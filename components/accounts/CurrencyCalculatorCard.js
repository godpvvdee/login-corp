import React from "react";
import MInput from "../main/MInput";
import SmallTitle from "../main/SmallTitle";
import CurrencyCalculatorCurrencyPicker from "./CurrencyCalculatorCurrencyPicker";

const CurrencyCalculatorCard = () => {
  return (
    <div className="flex flex-col rounded-lg bg-white p-7">
      <SmallTitle title="Ханш тооцоолох" />
      <div className="flex">
        <div className="mr-2 w-4/5">
          <MInput
            maxLength={15}
            placeholder="Мөнгөн дүн"
            id="firstAmount"
            numberFormat
            type="text"
            onChange={(e) => {}}
          />
        </div>
        <div className="w-1/5">
          <CurrencyCalculatorCurrencyPicker />
        </div>
      </div>
      <div className="flex">
        <div className="mr-2 w-4/5">
          <MInput
            maxLength={15}
            placeholder="Мөнгөн дүн"
            id="firstAmount"
            numberFormat
            type="text"
            onChange={(e) => {}}
          />
        </div>
        <div className="w-1/5">
          <CurrencyCalculatorCurrencyPicker />
        </div>
      </div>
    </div>
  );
};

export default CurrencyCalculatorCard;
