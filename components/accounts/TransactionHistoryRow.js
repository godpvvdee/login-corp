const TransactionHistoryRow = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  return (
    <>
      <div className="hidden items-center md:flex ">
        <img
          className="mr-3 h-10 w-10"
          src={
            props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
              ? "/icons/ic_income.png"
              : "/icons/ic_outcome.png"
          }
          alt=""
        />
        <div className="flex w-40 flex-col">
          <p className="font-regular text-base text-black">
            {props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
              ? "Орлого"
              : "Зарлага"}
          </p>
          <p className="font-regular text-xs text-gray">
            {props.transaction.created_date.substring(0, 10) +
              " " +
              props.transaction.created_date.substring(11, 19)}
          </p>
        </div>
        <div className="flex w-1/4 flex-col">
          <p className="font-regular text-base text-black">
            {props.transaction.to_acct_num !== null
              ? props.transaction.to_acct_num
              : "-"}
          </p>
          <p className="font-regular text-xs text-gray">
            {props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
              ? "Шилжүүлэгчийн данс"
              : "Хүлээн авагчын данс"}
          </p>
        </div>
        <div className="flex w-1/4 flex-col ">
          <p className="font-regular text-base text-black">
            {props.transaction.to_acct_name !== null
              ? props.transaction.to_acct_name
              : "-"}
          </p>
          <p className="font-regular text-xs text-gray">
            {props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
              ? "Шилжүүлэгчийн нэр"
              : "Хүлээн авагчын нэр"}
          </p>
        </div>
        <div className="ml-2 flex w-1/4 flex-col">
          <p className="font-regular text-base text-black">
            {props.transaction.remarks === null
              ? "-"
              : props.transaction.remarks}
          </p>
          <p className="font-regular text-xs text-gray">Шилжүүлгийн утга</p>
        </div>
        <div className="flex w-1/5 flex-col text-right">
          <p
            className={`font-semibold text-base ${
              props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
                ? "text-primary"
                : "text-black"
            }`}
          >
            {formatter.format(props.transaction.from_amt).replaceAll("$", "")}
          </p>
          <p className="font-regular text-xs text-gray">
            Үлдэгдэл:{" "}
            {formatter.format(props.transaction.balance).replaceAll("$", "")}
          </p>
        </div>
      </div>
      <div className="flex justify-between md:hidden">
        <div className="flex flex-col ">
          <p className="font-regular text-base text-black">
            {props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
              ? "Орлого"
              : "Зарлага"}
          </p>
          <p className="font-regular text-xs text-gray">
            {props.transaction.to_acct_name !== null
              ? props.transaction.to_acct_name
              : "-"}
          </p>
        </div>

        <div className="flex flex-col text-right">
          <p
            className={`font-semibold text-base ${
              props.transaction.type_name.substring(0, 1).toLowerCase() === "c"
                ? "text-primary"
                : "text-black"
            }`}
          >
            {formatter.format(props.transaction.from_amt).replaceAll("$", "")}
          </p>
          <p className="font-regular text-xs text-gray">
            {props.transaction.created_date.substring(0, 10) +
              " " +
              props.transaction.created_date.substring(11, 19)}
          </p>
        </div>
      </div>
    </>
  );
};

export default TransactionHistoryRow;
