import React from "react";

const CurrencyCalculatorCurrencyPicker = () => {
  return (
    <button className="flex h-12 w-full items-center justify-between rounded-lg bg-border-100 px-4">
      <div className="flex">
        <img src="/icons/ic_mnt.png" alt="" className="mr-2 h-5 w-5" />
        <p className="font-regular text-xs text-black">MNT</p>
      </div>
      <img src="/icons/ic_picker.png" alt="" className="h-5 w-5" />
    </button>
  );
};

export default CurrencyCalculatorCurrencyPicker;
