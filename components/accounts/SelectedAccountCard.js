import React from "react";

const SelectedAccountCard = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return (
    <div className="flex h-13 items-center justify-between rounded-lg bg-border-100 px-4">
      <div className="flex flex-col ">
        <p className="font-regular text-xs text-gray">{props.title}</p>
        <p className="mt-[-1px] font-regular text-base text-black">
          {props.acct_num + " " + props.acct_ccy}
        </p>
      </div>
      <p className="font-semibold text-base text-black">
        {formatter.format(props.balance_amt).replaceAll("$", "")}
      </p>
    </div>
  );
};

export default SelectedAccountCard;
