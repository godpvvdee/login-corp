import React, { useEffect, useState } from "react";
import LargeTitle from "../main/LargeTitle";
import MCalendarPicker from "../MCalendarPicker";
import TransactionDurationPicker from "../transaction/TransactionDurationPicker";
import TransactionFilterButton from "../transaction/TransactionFilterButton";
import AccountTransactionHistoryPicker from "./details/AccountTransactionHistoryPicker";
import TransactionHistoryByEmailPicker from "./details/TransactionHistoryByEmailPicker";
import TransactionHistoryByFilePicker from "./details/TransactionHistoryByFilePicker";
import TransactionHistoryCard from "./TransactionHistoryCard";

const filePickerItemsList = [
  {
    name: "Энгийн хуулга",
    type: "pdf",
    isQr: false,
    reportName: "ACCOUNT_STATEMENT_PDF",
  },
  {
    name: "Баталгаат хуулга (QR)",
    type: "pdf",
    isQr: true,
    reportName: "ACCOUNT_STATEMENT_PDF",
  },
  {
    name: "XSLX Файл",
    type: "excel",
    isQr: false,
    reportName: "ACCOUNT_STATEMENT_EXCEL",
  },
];

const TransactionInfoCard = (props) => {
  const [start, setStart] = useState(props.start);
  const [end, setEnd] = useState(props.end);
  const [startCalendarOpen, setStartCalendarOpen] = useState(false);
  const [endCalendarOpen, setEndCalendarOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(props.selectedIndex);
  const [emailPickerOpen, setEmailPickerOpen] = useState(false);
  const [downloadPickerOpen, setDownloadPickerOpen] = useState(false);
  const [downloadPickerOpenRes, setDownloadPickerOpenRes] = useState(false);
  const [filterPickerOpenRes, setFilterPickerOpenRes] = useState(false);
  const [downloadPickerCheckFile, setDownloadPickerCheckFile] = useState("");
  const sendEmailHandler = (data) => {
    props.onEmailSend(data);
    setEmailPickerOpen(false);
  };

  const sendFileHandler = (data) => {
    props.onFileSend(data);
    setDownloadPickerOpen(false);
  };

  return (
    <>
      <div className="hidden rounded-lg bg-defaultBackground  py-7 pr-7 md:block">
        <div className="flex justify-between">
          <LargeTitle title="Дансны хуулга" />
          <div className="flex gap-4">
            <div className="flex flex-col">
              <TransactionHistoryByEmailPicker
                onOutsideClick={() => {
                  setEmailPickerOpen(false);
                }}
                onClick={() => {
                  setEmailPickerOpen(!emailPickerOpen);
                }}
                isOpen={emailPickerOpen}
                onSelect={sendEmailHandler}
                itemsList={filePickerItemsList}
              />
            </div>
            <div className="flex flex-col">
              <TransactionHistoryByFilePicker
                onOutsideClick={() => {
                  setDownloadPickerOpen(false);
                }}
                onClick={() => {
                  setDownloadPickerOpen(!downloadPickerOpen);
                  setEmailPickerOpen(false);
                }}
                isOpen={downloadPickerOpen}
                onSelect={sendFileHandler}
                itemsList={filePickerItemsList}
              />
            </div>
          </div>
        </div>
        <div className="mb-7 flex items-center justify-between pl-7">
          <div className="flex">
            <div className="flex flex-col">
              <button
                disabled={!props.accountInfo.config.transactionhistory}
                onClick={() => {
                  setStartCalendarOpen(!startCalendarOpen);
                }}
                className="mr-3 flex items-center rounded-lg bg-border-100 px-4 py-2"
              >
                <div className="mr-5 flex w-20 flex-col text-left">
                  <p className="font-regular text-xs text-gray">Эхлэх огноо</p>
                  <p className="font-regular text-base text-black">
                    {new Date(start).getFullYear() +
                      "." +
                      (new Date(start).getMonth() + 1) +
                      "." +
                      new Date(start).getDate()}
                  </p>
                </div>
                <img className="h-5 w-5" src="/icons/ic_calendar.png" alt="" />
              </button>
              {startCalendarOpen ? (
                <MCalendarPicker
                  isEnd={false}
                  date={start}
                  onClick={(date) => {
                    setStart(date);
                    setStartCalendarOpen(false);
                    setEndCalendarOpen(true);
                  }}
                />
              ) : null}
            </div>
            <div className="flex flex-col">
              <button
                disabled={!props.accountInfo.config.transactionhistory}
                onClick={() => {
                  setEndCalendarOpen(!endCalendarOpen);
                }}
                className="flex items-center rounded-lg bg-border-100 px-4 py-2"
              >
                <div className="mr-20 flex flex-col text-justify">
                  <p className="font-regular text-xs text-gray">Дуусах огноо</p>
                  <p className="float-left font-regular text-base text-black">
                    {new Date(end).getFullYear() +
                      "." +
                      (new Date(end).getMonth() + 1) +
                      "." +
                      new Date(end).getDate()}
                  </p>
                </div>
                <img className="h-5 w-5" src="/icons/ic_calendar.png" alt="" />
              </button>
              {endCalendarOpen ? (
                <MCalendarPicker
                  isEnd={true}
                  start={start}
                  date={end}
                  onClick={(date) => {
                    setEnd(date);
                    setEndCalendarOpen(false);
                    props.onCalendarFilter(start, date);
                  }}
                />
              ) : null}
            </div>
          </div>
          <div className="flex">
            <TransactionDurationPicker
              selectedIndex={selectedIndex}
              onClick={(data) => {
                if (props.accountInfo.config.transactionhistory) {
                  setSelectedIndex(data);
                  props.onIndexChange(data);
                  if (data === 0) {
                    props.onClick(0);
                  } else if (data === 1) {
                    props.onClick(1);
                  } else {
                    props.onClick(2);
                  }
                }
              }}
            />
          </div>
        </div>

        <TransactionHistoryCard
          accountInfo={props.accountInfo}
          transactions={props.transactionsList}
        />
      </div>
      <div className="block rounded-lg bg-defaultBackground  py-7 px-[18px] md:hidden">
        <div className=" justify-between">
          <LargeTitle title="Дансны хуулга" />
          <div className="grid grid-cols-2 gap-2">
            <div className="flex flex-col">
              <button
                onClick={() => setDownloadPickerOpenRes(true)}
                className="rounded-lg bg-secondaryBackground py-3.5 font-semibold text-base"
              >
                Татах
              </button>
            </div>
            <div className="flex flex-col">
              <button
                onClick={() => setFilterPickerOpenRes(true)}
                className="rounded-lg bg-secondaryBackground py-3.5 font-semibold text-base"
              >
                Шүүлт
              </button>
            </div>
          </div>
        </div>
        <TransactionHistoryCard
          accountInfo={props.accountInfo}
          transactions={props.transactionsList}
        />
        {downloadPickerOpenRes && (
          <div className="absolute inset-0 h-full w-full bg-black/60 ">
            {" "}
            <div className="fixed bottom-0 right-0 left-0 z-50 min-h-[300px] overflow-x-hidden rounded-t-2xl  bg-white  ">
              <div className="px-[18px] pt-4">
                {filePickerItemsList.map((list, i) => (
                  <div key={i} className="py-4 pl-[6px] text-black">
                    <TransactionFilterButton
                      text={list.name}
                      onClick={() => {
                        setDownloadPickerCheckFile(list);
                      }}
                      isChecked={downloadPickerCheckFile === list}
                    />
                  </div>
                ))}
                <button
                  onClick={() => {
                    setDownloadPickerOpenRes(false);
                    sendEmailHandler(downloadPickerCheckFile);
                  }}
                  className="mx-[7px] w-[325px] rounded-lg bg-greenSimpleColor py-[15px] text-xs text-white"
                >
                  Хуулга татах
                </button>
              </div>
            </div>
          </div>
        )}
        {filterPickerOpenRes && (
          <div className="absolute inset-0 h-full w-full bg-black/60 ">
            {" "}
            <div className="fixed bottom-0 right-0 left-0 z-50 min-h-[300px] overflow-x-hidden rounded-t-2xl  bg-white  ">
              <div className="px-6 pt-7">
                <div className="flex">
                  <TransactionDurationPicker
                    selectedIndex={selectedIndex}
                    onClick={(data) => {
                      if (props.accountInfo.config.transactionhistory) {
                        setSelectedIndex(data);
                        props.onIndexChange(data);
                        if (data === 0) {
                          props.onClick(0);
                        } else if (data === 1) {
                          props.onClick(1);
                        } else {
                          props.onClick(2);
                        }
                      }
                    }}
                  />
                </div>
                <div className="mt-6 mb-12 flex">
                  <div className="flex flex-col">
                    <button
                      disabled={!props.accountInfo.config.transactionhistory}
                      onClick={() => {
                        setStartCalendarOpen(!startCalendarOpen);
                      }}
                      className="mr-3 flex items-center rounded-lg bg-border-100 px-4 py-2"
                    >
                      <div className="mr-5 flex flex-col text-left">
                        <p className="font-regular text-xs text-gray">
                          Эхлэх огноо
                        </p>
                        <p className="font-regular text-base text-black">
                          {new Date(start).getFullYear() +
                            "." +
                            (new Date(start).getMonth() + 1) +
                            "." +
                            new Date(start).getDate()}
                        </p>
                      </div>
                      <img
                        className="h-5 w-5"
                        src="/icons/ic_calendar.png"
                        alt=""
                      />
                    </button>
                    {startCalendarOpen ? (
                      <div className="absolute inset-0 h-full w-full bg-black/60 ">
                        <div className="fixed inset-0 left-10 top-40 z-50 overflow-x-hidden">
                          {" "}
                          <MCalendarPicker
                            isEnd={false}
                            date={start}
                            onClick={(date) => {
                              setStart(date);
                              setStartCalendarOpen(false);
                              setEndCalendarOpen(true);
                            }}
                          />
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div className="flex flex-col">
                    <button
                      disabled={!props.accountInfo.config.transactionhistory}
                      onClick={() => {
                        setEndCalendarOpen(!endCalendarOpen);
                      }}
                      className="flex items-center rounded-lg bg-border-100 px-4 py-2"
                    >
                      <div className="mr-5 flex flex-col text-justify">
                        <p className="font-regular text-xs text-gray">
                          Дуусах огноо
                        </p>
                        <p className="float-left font-regular text-base text-black">
                          {new Date(end).getFullYear() +
                            "." +
                            (new Date(end).getMonth() + 1) +
                            "." +
                            new Date(end).getDate()}
                        </p>
                      </div>
                      <img
                        className="h-5 w-5"
                        src="/icons/ic_calendar.png"
                        alt=""
                      />
                    </button>
                    {endCalendarOpen ? (
                      <div className="absolute inset-0 h-full w-full bg-black/60 ">
                        <div className="fixed inset-0 left-10 top-40 z-50 overflow-x-hidden">
                          <MCalendarPicker
                            isEnd={true}
                            start={start}
                            date={end}
                            onClick={(date) => {
                              setEnd(date);
                              setEndCalendarOpen(false);
                              props.onCalendarFilter(start, date);
                            }}
                          />
                        </div>
                      </div>
                    ) : null}
                  </div>
                </div>
                <button
                  onClick={() => {
                    setFilterPickerOpenRes(false);
                  }}
                  className="w-[325px] rounded-lg bg-greenSimpleColor py-[15px]  text-xs text-white"
                >
                  Шүүж харах
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default TransactionInfoCard;
