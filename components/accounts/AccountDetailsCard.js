import React, { useState, useEffect } from "react";
import MediumTitle from "../main/MediumTitle";
import { toast } from "react-toastify";
import Axios from "axios";
import TransactionFilterButton from "../transaction/TransactionFilterButton";
const AccountDetailsCard = (props) => {
  const [caList, setCaList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isFilterTransactionOpen, setIsFilterTransactionOpen] = useState(false);
  const [transactionAmount, setTransactionAmount] = useState("");

  const [isOpen, setIsOpen] = useState(false);
  const pickerHandler = (data) => {
    setIsOpen(false);
    props.onAccountChange(data.currentAccount.acct_num);
  };
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  const formatBalance = (balance) => {
    const formattedBal = formatter.format(balance).replace("$", "").split(".");
    if (balance > 0) {
      return (
        <p className="font-bold text-3xl text-black">
          {formattedBal[0]}
          <span className="text-2xl">.{formattedBal[1]}</span>
        </p>
      );
    } else {
      return (
        <p className="font-bold text-3xl text-black">
          0<span className="text-2xl">.00</span>
        </p>
      );
    }
  };

  useEffect(() => {}, []);
  // console.log("calist", caList);
  return (
    <div className="block items-center justify-between rounded-lg bg-defaultBackground py-7 pr-7 md:flex">
      <div>
        <MediumTitle
          title={
            props.accountInfo.nickname === null
              ? props.accountInfo.product_name
              : props.accountInfo.nickname
          }
        />
        <div className="flex flex-col pl-7">
          <div className="flex items-center">
            {/* {formatBalance(props.accountInfo.positive_balance_amt)} */}
          </div>
          <p className="font-regular text-base text-gray">
            {/* {props.accountInfo.acct_num.substring(0, 4) +
              " " +
              props.accountInfo.acct_num.substring(
                4,
                props.accountInfo.acct_num.length
              ) +
              " " +
              props.accountInfo.acct_ccy} */}
          </p>
        </div>
      </div>
      <div>
        <button
          onClick={() => {
            setIsOpen(!isOpen);
            setIsFilterTransactionOpen(true);
          }}
          className="ml-7 mt-4 flex h-10 w-50  items-center justify-between rounded-lg bg-border-100 px-4  md:m-0"
        >
          <p className="mr-8 font-semibold  text-xs text-black">Данс солих</p>
          <img
            src={
              isOpen
                ? "/icons/ic_menu_picker_green.png"
                : "/icons/ic_menu_picker_gray.png"
            }
            alt=""
            className="h-4 w-4"
          />
        </button>
        <div className="hidden md:block">
          {isOpen ? (
            <div className="absolute mt-2 w-50   overflow-hidden  rounded-b-lg bg-white shadow-xl ">
              {caList.map((acc, idx) => (
                <div className="py-2 px-1">
                  {" "}
                  <TransactionFilterButton
                    className=" group flex w-full items-center bg-white hover:bg-primary"
                    text={
                      acc.currentAccount.nickname + acc.currentAccount.acct_num
                    }
                    key={idx}
                    onClick={() => (
                      pickerHandler(acc),
                      setTransactionAmount(
                        transactionAmount === acc.currentAccount.nickname
                          ? ""
                          : acc.currentAccount.nickname
                      )
                    )}
                    isChecked={
                      transactionAmount === acc.currentAccount.nickname
                    }
                  />
                </div>
              ))}
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>
      <div className="md:hidden">
        {isFilterTransactionOpen && (
          <div className="absolute inset-0 h-full w-full bg-black/60 ">
            <div className="fixed bottom-0 right-0 left-0 z-50 min-h-[300px] overflow-x-hidden rounded-t-2xl  bg-white  ">
              <div className="px-[18px] pt-4">
                {caList.map((acc, idx) => (
                  <div className="py-2 px-1">
                    {" "}
                    <TransactionFilterButton
                      className=" group flex w-full items-center bg-white hover:bg-primary"
                      text={
                        <div className="flex w-[260px] justify-between">
                          <span>
                            {acc.currentAccount.nickname === null
                              ? acc.currentAccount.acct_name
                              : acc.currentAccount.nickname}
                          </span>

                          <span>{acc.currentAccount.acct_num}</span>
                        </div>
                      }
                      key={idx}
                      onClick={() => (
                        pickerHandler(acc),
                        setTransactionAmount(
                          transactionAmount === acc.currentAccount.nickname
                            ? ""
                            : acc.currentAccount.nickname
                        )
                      )}
                      isChecked={
                        transactionAmount === acc.currentAccount.nickname
                      }
                    />
                  </div>
                ))}
                <button
                  onClick={() => {
                    setIsFilterTransactionOpen(false);
                  }}
                  className="mx-[7px] w-[325px] rounded-lg bg-greenSimpleColor py-[15px] text-xs text-white"
                >
                  Сонгох
                </button>
              </div>
            </div>
          </div>
        )}
      </div>

      {/* <div className="h-13 rounded-lg bg-border-100 px-4 flex flex-col justify-center">
        <p className="text-xs text-gray font-regular">Битүүмжлэгдсэн дүн</p>
        <p className="text-base text-black font-semibold">
          {formatter.format(props.accountInfo.lien_amt).replaceAll("$", "")}
        </p>
      </div> */}
      {/* <button className="bg-primary hover:opacity-90 h-10 px-4 rounded-lg items-center flex ≈">
        <img src="/icons/ic_transaction.png" className="w-5 h-5 mr-2" alt="" />
        <p className="text-defaultBackground font-bold text-xs mt-[-1px]">
          Шилжүүлэг
        </p>
      </button> */}
    </div>
  );
};

export default AccountDetailsCard;
