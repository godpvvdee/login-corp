import React, { useState } from "react";
import SmallTitle from "../main/SmallTitle";

const CurrencyRateCard = (props) => {
  const [selectedIndex, setSelectedIndex] = useState(0);
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  return (
    <div className="rounded-lg bg-defaultBackground p-7">
      <SmallTitle title="Валютын ханш" />

      <div className="mb-5 flex">
        <div className="w-1/3">
          <p className="font-semibold text-xs text-gray">Валют</p>
        </div>
        <div className="w-1/3">
          <p className="font-semibold text-xs text-gray">Авах</p>
        </div>
        <div className="w-1/3">
          <p className="font-semibold text-xs text-gray">Зарах</p>
        </div>
      </div>

      <div className="flex flex-col gap-5">
        {props.currencyRateList.map((item, index) => {
          return (
            <div key={index} className="flex">
              <div className="flex w-1/3 ">
                <img
                  className="mr-2 h-5 w-5"
                  src={`/icons/currencies/${item.ccy2.toLowerCase()}.png`}
                  alt=""
                />
                <p className="font-regular text-base text-black">{item.ccy2}</p>
              </div>
              <div className="w-1/3">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.buy).substring(1)}
                </p>
              </div>
              <div className="w-1/3">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.sell).substring(1)}
                </p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CurrencyRateCard;
