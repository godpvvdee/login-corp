import React from "react";
import ReactTooltip from "react-tooltip";
2;
const AccountOptionsSettingsItem = (props) => {
  return props.pathname === "" ? (
    <div className="group w-full rounded-lg bg-border-100 p-4 text-left hover:bg-primary disabled:opacity-30 md:p-5">
      <svg
        className="mb-6 h-6 w-6 fill-black group-hover:fill-white"
        xmlns="http://www.w3.org/2000/svg"
      >
        {props.icon}
      </svg>
      <p className="font-semibold text-xxs text-black group-hover:text-white md:text-base">
        {props.title}
      </p>
    </div>
  ) : (
    // <Link
    //   to={{
    //     pathname: props.pathname,
    //     state: props.state,
    //   }}
    // >
    <button
      onClick={() => {
        history.push({
          pathname: props.pathname,
          state: props.state,
        });
      }}
      disabled={props.isDisabled}
      className=" group relative w-full rounded-lg bg-border-100 text-left hover:bg-primary  disabled:bg-border-100/30"
    >
      <div className="p-4" data-tip={props.isDisabled ? props.tooltip : ""}>
        <svg
          className="mb-6 h-6 w-6 fill-black group-hover:fill-white group-disabled:fill-gray md:mb-9"
          xmlns="http://www.w3.org/2000/svg"
        >
          {props.icon}
        </svg>
        <p className="font-semibold text-xxs text-black group-hover:text-white group-disabled:text-black group-disabled:opacity-30 md:text-base">
          {props.title}
        </p>
      </div>
      <ReactTooltip
        place="top"
        type="light"
        backgroundColor="white"
        className="tooltip"
      />
    </button>
    // </Link>
  );
};

export default AccountOptionsSettingsItem;
