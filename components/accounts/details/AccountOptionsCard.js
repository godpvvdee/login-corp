import React, { useState } from "react";
import LargeTitle from "../../main/LargeTitle";
import AccountOptionsButtonList from "./AccountOptionsButtonList";
import AccountOptionsCardsList from "./AccountOptionsCardsList";
import AccountOptionsDetailsGrid from "./AccountOptionsDetailsGrid";
import AccountOptionsSettingsGrid from "./AccountOptionsSettingsGrid";

const AccountOptionsCard = (props) => {
  const [buttonState, setButtonState] = useState(0);

  const buttonHandler = (id) => {
    setButtonState(id);
  };

  const cardList = [];

  const getOptions = (id) => {
    switch (id) {
      case 0:
        return <AccountOptionsDetailsGrid accountInfo={props.accountInfo} />;
      case 1:
        return <AccountOptionsCardsList cardList={cardList} />;
      case 2:
        return (
          <AccountOptionsSettingsGrid
            liens={props.liens}
            accountInfo={props.accountInfo}
          />
        );
      default:
        return <div></div>;
    }
  };

  return (
    <div className="rounded-lg bg-defaultBackground py-7 pr-7">
      <LargeTitle title="Дансны мэдээлэл" />
      <div className="pl-4 md:pl-7">
        <AccountOptionsButtonList
          selectedId={buttonState}
          onButtonClick={buttonHandler}
        />
        {getOptions(buttonState)}
      </div>
    </div>
  );
};

export default AccountOptionsCard;
