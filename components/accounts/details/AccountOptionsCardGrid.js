import IconArrowRight from "../../../icons/IconArrowRight";

const AccountOptionsCardGrid = (props) => {
  return (
    <div className="group relative flex cursor-pointer flex-col rounded-lg border border-border-100 bg-white pl-5 pt-5 hover:bg-primary">
      <div className="flex items-center justify-between pr-5 pb-20">
        <div className="flex flex-col">
          <p className="font-regular text-xs text-black group-hover:text-white">
            {props.name}
          </p>
          <p className="font-regular text-base text-gray group-hover:text-white">
            {props.number}
          </p>
        </div>
        <svg
          className="h-[10px] w-[7px] fill-gray group-hover:fill-white"
          xmlns="http://www.w3.org/2000/svg"
        >
          <IconArrowRight />
        </svg>
      </div>
      <div className="align-right absolute bottom-0 right-0 h-20 w-32 bg-[url('../public/icons/ic_card.png')] bg-cover group-hover:bg-[url('../public/icons/ic_card_hover.png')]" />
    </div>
  );
};

export default AccountOptionsCardGrid;
