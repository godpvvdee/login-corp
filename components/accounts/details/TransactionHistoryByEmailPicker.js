import React from "react";
import OutsideClickHandler from "react-outside-click-handler";

const TransactionHistoryByEmailPicker = (props) => {
  const pickerHandler = (data) => {
    props.onSelect(data);
  };

  return (
    <>
      <OutsideClickHandler
        onOutsideClick={() => {
          props.onOutsideClick();
        }}
      >
        <button
          onClick={() => {
            props.onClick();
          }}
          className="flex h-10 w-50 items-center justify-between rounded-lg bg-border-100 px-4"
        >
          <p className="mr-8 font-semibold text-xs text-black">
            И-мэйлээр илгээх
          </p>
          <img
            src={
              props.isOpen
                ? "/icons/ic_menu_picker_green.png"
                : "/icons/ic_menu_picker_gray.png"
            }
            alt=""
            className="h-4 w-4"
          />
        </button>
        {props.isOpen && (
          <div className="absolute w-50 overflow-hidden rounded-b-lg bg-white shadow-xl ">
            {props.itemsList.map((item, index) => (
              <button
                onClick={() => pickerHandler(item)}
                key={index}
                className="group flex h-12 w-full items-center bg-white px-4 hover:bg-primary"
              >
                <p className="font-regular text-base text-black group-hover:text-white">
                  {item.name}
                </p>
              </button>
            ))}
          </div>
        )}
      </OutsideClickHandler>
    </>
  );
};

export default TransactionHistoryByEmailPicker;
