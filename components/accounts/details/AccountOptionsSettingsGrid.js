import Link from "next/link";
import IconBank from "../../../icons/IconBank";
import IconCandles from "../../../icons/IconCandles";
import IconClose from "../../../icons/IconClose";
import IconHalfgrid from "../../../icons/IconHalfgrid";
import IconLimit from "../../../icons/IconLimit";
import IconPencil from "../../../icons/IconPencil";
import IconPie from "../../../icons/IconPie";
import IconUnlock from "../../../icons/IconUnlock";
import AccountOptionsSettingsItem from "./AccountOptionsSettingsItem";

const AccountOptionsSettingsGrid = (props) => {
  return (
    <div className="grid grid-cols-2 gap-4">
      <AccountOptionsSettingsItem
        pathname="/account-change-name"
        state={{ accountInfo: props.accountInfo }}
        title="Дансны нэр солих"
        icon={<IconBank />}
        isDisabled={!props.accountInfo.config.nickname}
        tooltip="Танд энэ дансны нэрийг солих эрх байхгүй байна."
      />
      <AccountOptionsSettingsItem
        pathname=""
        title="Эрх тохируулах"
        icon={<IconCandles />}
        isDisabled={false}
      />
      <AccountOptionsSettingsItem
        pathname="/account-description"
        state={{ accountInfo: props.accountInfo }}
        title="Тодорхойлолт авах"
        icon={<IconPencil />}
        isDisabled={!props.accountInfo.config.description}
        tooltip="Танд энэ дансны тодорхойлолтыг авах эрх байхгүй байна."
      />
      <AccountOptionsSettingsItem
        pathname=""
        title="Өөрчлөлтүүд"
        icon={<IconHalfgrid />}
        isDisabled={false}
      />
      <AccountOptionsSettingsItem
        pathname="/account-transaction-limit"
        state={{ accountInfo: props.accountInfo }}
        title="Гүйлгээний лимит"
        icon={<IconLimit />}
        isDisabled={false}
      />

      {props.liens.length === 0 ? (
        <AccountOptionsSettingsItem
          pathname="/account-lien"
          state={{ accountInfo: props.accountInfo }}
          title="Дүнгээр битүүмжлэх"
          icon={<IconUnlock />}
          isDisabled={!props.accountInfo.config.lien}
          tooltip="Танд энэ дансыг мөнгөн дүнгээр битүүмжлэх эрх байхгүй байна."
        />
      ) : (
        <AccountOptionsSettingsItem
          pathname="/account-unlien"
          state={{
            liens: props.liens,
            accountInfo: props.accountInfo,
          }}
          title="Дүнгийн битүүмж цуцлах"
          icon={<IconUnlock />}
          isDisabled={!props.accountInfo.config.unlien}
          tooltip="Танд энэ дансны мөнгөн дүнгийн битүүмжийг цуцлах эрх байхгүй байна."
        />
      )}
      <AccountOptionsSettingsItem
        pathname="/account-freeze"
        state={{
          accNo: props.accountInfo.acct_num,
          freezeCode: props.accountInfo.freeze_code,
        }}
        title={
          props.accountInfo.freeze_code === "T"
            ? "Бүтэн битүүмж цуцлах"
            : "Бүтэн битүүмжлэх"
        }
        icon={<IconPie />}
        isDisabled={
          props.accountInfo.freeze_code === "T"
            ? !props.accountInfo.config.unfreeze
            : !props.accountInfo.config.freeze
        }
        tooltip={
          props.accountInfo.freeze_code === "T"
            ? "Танд энэ дансны битүүмжийг цуцлах эрх байхгүй байна."
            : "Танд энэ дансыг битүүмжлэх эрх байхгүй байна."
        }
      />

      {/* <Link
        to={{
          pathname: "/account-close",
          state: {
            accNo: props.accountInfo.acct_num,
          },
        }}
      >
        <div className="p-5 bg-border-100 rounded-lg hover:bg-error cursor-pointer group">
          <svg
            className="fill-error w-6 h-6 mb-9 group-hover:fill-white"
            xmlns="http://www.w3.org/2000/svg"
          >
            <IconClose />
          </svg>
          <p className="text-black text-base font-semibold group-hover:text-white">
            Данс хаах
          </p>
        </div>
      </Link> */}
    </div>
  );
};

export default AccountOptionsSettingsGrid;
