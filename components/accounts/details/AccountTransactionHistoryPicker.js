import React, { useState } from "react";

const AccountTransactionHistoryPicker = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const pickerHandler = (data) => {
    setIsOpen(false);
  };
  return (
    <>
      <button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className="flex h-10 w-40 items-center justify-between rounded-lg bg-border-100 px-4"
      >
        <p className="mr-8 font-semibold text-xs text-black">Файл татах</p>
        <img
          src={
            isOpen
              ? "/icons/ic_menu_picker_green.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          alt=""
          className="h-4 w-4"
        />
      </button>
      {isOpen ? (
        <div className="absolute mt-10 w-40 overflow-hidden rounded-b-lg bg-white shadow-xl ">
          {props.itemsList.map((item, index) => (
            <button
              onClick={() => pickerHandler(item)}
              key={index}
              className="group flex h-12 w-full items-center bg-white px-4 hover:bg-primary"
            >
              <p className="font-regular text-base text-black group-hover:text-white">
                {item.name}
              </p>
            </button>
          ))}
        </div>
      ) : (
        <div></div>
      )}
    </>
  );
};

export default AccountTransactionHistoryPicker;
