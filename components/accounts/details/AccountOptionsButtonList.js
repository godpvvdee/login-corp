const AccountOptionsButtonList = (props) => {
  return (
    <div className="mb-5 flex w-75 rounded-lg bg-border-100 xl:w-full">
      <button
        onClick={() => {
          props.onButtonClick(0);
        }}
        className={`flex h-10 w-32 items-center justify-center rounded-lg border border-border-100 text-center xl:w-1/3 ${
          props.selectedId === 0 ? "rounded-lg bg-white" : "bg-border-100"
        }`}
      >
        <p
          className={`mt-[-1px] font-bold text-xs ${
            props.selectedId === 0 ? "text-primary" : "text-black"
          }`}
        >
          Дэлгэрэнгүй
        </p>
      </button>
      <div className="my-auto h-4 w-[2px] rounded-lg bg-border-200" />
      <button
        onClick={() => {
          props.onButtonClick(1);
        }}
        className={`flex h-10 w-32 items-center justify-center rounded-lg border border-border-100 text-center xl:w-1/3 ${
          props.selectedId === 1 ? "rounded-lg bg-white" : "bg-border-100"
        }`}
      >
        <p
          className={`mt-[-1px] font-bold text-xs ${
            props.selectedId === 1 ? "text-primary" : "text-black"
          }`}
        >
          Картууд
        </p>
      </button>

      <div className="my-auto h-4 w-[2px] rounded-lg bg-border-200" />
      <button
        onClick={() => {
          props.onButtonClick(2);
        }}
        className={`flex h-10 w-35 items-center justify-center rounded-lg border border-border-100 text-center xl:w-1/3 ${
          props.selectedId === 2 ? "rounded-lg bg-white" : "bg-border-100"
        }`}
      >
        <p
          className={`mt-[-1px] font-bold text-xs ${
            props.selectedId === 2 ? "text-primary" : "text-black"
          }`}
        >
          Тохируулга
        </p>
      </button>
    </div>
  );
};

export default AccountOptionsButtonList;
