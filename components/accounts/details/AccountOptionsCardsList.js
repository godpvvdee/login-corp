import ErrorCard from "../../main/ErrorCard";
import AccountOptionsCardGrid from "./AccountOptionsCardGrid";

const AccountOptionsCardsList = (props) => {
  return props.cardList.length > 0 ? (
    <div className="grid grid-cols-2 gap-4">
      {props.cardList.map((item, index) => (
        <AccountOptionsCardGrid
          key={index}
          name={item.name}
          number={item.number}
        />
      ))}
    </div>
  ) : (
    <ErrorCard title="Танд холбогдсон карт байхгүй байна." />
  );
};

export default AccountOptionsCardsList;
