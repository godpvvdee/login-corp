import DetailCard from "../../main/DetailCard";

const AccountOptionsDetailsGrid = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const getStatusText = () => {
    if (props.accountInfo.freeze_code !== "T") {
      return props.accountInfo.acct_status;
    } else {
      if (props.accountInfo.acct_status_code === "A") {
        return "Битүүмжтэй";
      } else {
        return props.accountInfo.acct_status;
      }
    }
  };
  return (
    <div className="grid gap-2 md:grid-cols-2 3xl:flex 3xl:flex-col">
      <DetailCard
        title="Дансны нэр"
        description={
          props.accountInfo.nickname != null ? props.accountInfo.nickname : "-"
        }
      />
      <DetailCard
        title="Дансны дугаар"
        description={props.accountInfo.acct_num}
      />
      <DetailCard
        title="Данс эзэмшигчийн нэр"
        description={props.accountInfo.acct_name}
      />

      <DetailCard
        title="Дансны дугаар IBAN"
        description={props.accountInfo.iban}
      />
      <DetailCard
        title="Бүтээгдэхүүний нэр"
        description={props.accountInfo.product_name}
      />
      <DetailCard title="Валют" description={props.accountInfo.acct_ccy} />
      <DetailCard
        title="Харилцах дансны хүү"
        description={parseFloat(props.accountInfo.interest).toFixed(2) + "%"}
      />
      <DetailCard title="Дансны төлөв" description={getStatusText()} />
      <DetailCard
        title="Битүүмжлэгдсэн дүн"
        description={formatter
          .format(props.accountInfo.lien_amt)
          .replace("$", "")}
      />
      <DetailCard
        title="Боломжит үлдэгдэл"
        description={
          props.accountInfo.balance_amt > 0
            ? formatter.format(props.accountInfo.balance_amt).replace("$", "")
            : "0.00"
        }
      />
      <DetailCard
        title="Нийт үлдэгдэл"
        description={formatter
          .format(props.accountInfo.actual_balance_amt)
          .replace("$", "")}
      />
      <DetailCard
        title="Данс нээгдсэн огноо"
        description={props.accountInfo.open_dt.replaceAll("-", ".")}
      />
    </div>
  );
};

export default AccountOptionsDetailsGrid;
