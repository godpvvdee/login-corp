import React from "react";
var formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

const AccountsLatestTransactionRow = () => {
  return (
    <div className="flex items-center">
      <img className="mr-3 h-10 w-10" src="/icons/ic_income.png" alt="" />
      <div className="flex w-1/4 flex-col">
        <p className="font-regular text-base text-black">Орлого</p>
        <p className="font-regular text-xs text-gray">2020.03.08 20:00</p>
      </div>
      <div className="flex w-1/4 flex-col">
        <p className="font-regular text-base text-black">9008 500 060</p>
        <p className="font-regular text-xs text-gray">Харилцах данс</p>
      </div>
      <div className="flex w-1/4 flex-col">
        <p className="font-regular text-base text-black">9008 500 060</p>
        <p className="font-regular text-xs text-gray">Nomin Trade</p>
      </div>
      <div className="flex w-1/4 flex-col text-right">
        <p className="font-semibold text-base text-black">
          {formatter.format("350000").replace("$", "")}
        </p>
        <p className="font-regular text-xs text-gray">Huyagbat Ochirbat</p>
      </div>
    </div>
  );
};

export default AccountsLatestTransactionRow;
