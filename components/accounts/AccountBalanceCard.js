const AccountBalanceCard = (props) => {
  const formatBalance = (balance) => {
    var formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });
    const formattedBal = formatter.format(balance).replace("$", "").split(".");
    return (
      <p className="p-0 font-bold text-3xl text-black">
        {formattedBal[0]}
        <span className="text-2xl">.{formattedBal[1]}</span>
      </p>
    );
  };
  return (
    <div className="flex items-center justify-between rounded-lg bg-defaultBackground p-7">
      <div>
        <p className="p-0 font-semibold text-xs text-gray">Дансны үлдэгдэл</p>
        {formatBalance(props.total)}
      </div>
      {/* <Link to="/account-create">
        <button className="bg-primary hover:opacity-90 h-10 px-4 rounded-lg items-center flex justify-center">
          <img src="/icons/ic_add_circle.png" className="w-5 h-5 mr-2" alt="" />
          <p className="text-defaultBackground font-bold text-xs mt-[-1px]">
            Данс нээх
          </p>
        </button>
      </Link> */}
    </div>
  );
};

export default AccountBalanceCard;
