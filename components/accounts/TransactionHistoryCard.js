import ErrorCard from "../main/ErrorCard";
import TransactionHistoryRow from "./TransactionHistoryRow";

const TransactionHistoryCard = (props) => {
  return props.accountInfo.config.transactionhistory ? (
    props.transactions.length === 0 ? (
      <div className="pl-7">
        <ErrorCard title="Заасан хугацаанд уг дансанд гүйлгээ гараагүй байна." />
      </div>
    ) : (
      <div className="flex flex-col gap-5 md:pl-7">
        {props.transactions.map((item, index) => (
          <TransactionHistoryRow key={index} transaction={item} />
        ))}
      </div>
    )
  ) : (
    <div className="flex flex-col items-center py-40 pl-7">
      <img src="/icons/ic_folder.png" alt="" className="mb-5 h-14 w-14" />
      <p className="font-semibold text-base text-black">
        Танд дансны хуулга харах эрх байхгүй байна.
      </p>
    </div>
  );
};

export default TransactionHistoryCard;
