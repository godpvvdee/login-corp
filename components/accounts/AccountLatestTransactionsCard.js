import React from "react";
import LargeTitle from "../main/LargeTitle";
import AccountsLatestTransactionRow from "./AccountsLatestTransactionRow";

const AccountLatestTransactionsCard = (props) => {
  return (
    <div className="flex w-full flex-col rounded-lg bg-white p-7">
      <LargeTitle
        title="Гүйлгээний түүх"
        description="Start building your credit score"
      />
      <div className="flex flex-col gap-5">
        <AccountsLatestTransactionRow />
        <AccountsLatestTransactionRow />
        <AccountsLatestTransactionRow />
      </div>
    </div>
  );
};

export default AccountLatestTransactionsCard;
