import React, { useState } from "react";
import IconArrowRight from "../../icons/IconArrowRight";
import LargeTitle from "../main/LargeTitle";
import WaitingListTransactionRow from "./WaitingListTransactionRow";

const transactionList = [
  {
    description: "Зарлага",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
  {
    description: "Ахдаа",
    date: "2020.03.08 20:00",
    fromAccNo: "9008 500 060",
    fromAccName: "Харилцах данс",
    toAccNo: "9090 060 500",
    toAccName: "Nomin Trade",
    amount: 350000,
    amountName: "Huyagbat Ochirbat",
  },
];

const WaitingList = () => {
  const [buttonState, setButtonState] = useState(0);
  const [selectedTransactions, setSelectedTransactions] = useState(0);
  const [transactions, setTransactions] = useState(transactionList);

  const transactionSelectHandler = (index) => {
    let temp = [...transactions];
    temp[index].isSelected = !temp[index].isSelected;
    setTransactions(temp);
    setSelectedTransactions(
      transactions.filter((item) => item.isSelected === true).length
    );
  };

  return (
    <div className="rounded-lg bg-defaultBackground p-7">
      <div className="flex justify-between">
        <LargeTitle title="Батлах шилжүүлэг" />
        <svg className="h-[10px] w-[7px] fill-gray">
          <IconArrowRight />
        </svg>
      </div>
      <div className="flex justify-between">
        <div className="flex h-10 w-64 rounded-lg bg-border-100">
          <button
            onClick={() => setButtonState(0)}
            className={`h-10 w-32 rounded-lg border border-border-100 transition duration-300 ${
              buttonState === 0 ? "bg-white" : "bg-border-100"
            }`}
          >
            <p
              className={`font-bold text-xs ${
                buttonState === 0 ? "text-primary" : "text-black"
              }`}
            >
              Шилжүүлэг
            </p>
          </button>
          <button
            onClick={() => setButtonState(1)}
            className={`h-10 w-32 rounded-lg border border-border-100 transition duration-300 ${
              buttonState === 1 ? "bg-white" : "bg-border-100"
            }`}
          >
            <p
              className={`font-bold text-xs ${
                buttonState === 1 ? "text-primary" : "text-black"
              }`}
            >
              Багц шилжүүлэг
            </p>
          </button>
        </div>
        {selectedTransactions > 0 ? (
          <button className="flex h-10 items-center rounded-lg bg-primaryText px-4 text-center hover:bg-opacity-75">
            <p className="mt-[-1px] mr-5 font-bold text-xs text-white">
              Шилжүүлэг батлах
            </p>
            <div className="h-6 w-6 rounded-full bg-white bg-opacity-25">
              <p className="mt-[2.5px] font-bold text-xs text-white">
                {selectedTransactions}
              </p>
            </div>
          </button>
        ) : (
          <div></div>
        )}
      </div>
      <div className="mt-7 flex max-h-[400px] flex-col gap-5 overflow-y-auto pr-5">
        {transactions.map((item, index) => (
          <WaitingListTransactionRow
            key={index}
            transaction={item}
            onClick={() => transactionSelectHandler(index)}
          />
        ))}
      </div>
    </div>
  );
};

export default WaitingList;
