import React from "react";

const MButton = (props) => {
  return (
    <button
      disabled={props.disabled}
      onClick={props.onClick}
      className="group h-12 w-full rounded-lg bg-black disabled:bg-border-100"
    >
      <p className="mt-[-1px] font-bold text-base text-white group-disabled:text-gray">
        {props.title}
      </p>
    </button>
  );
};

export default MButton;
