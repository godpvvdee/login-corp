import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import ErrorToast from "../../main/ErrorToast";
import LargeTitle from "../../main/LargeTitle";
import MInput from "../../main/MInput";
import SuccessModal from "../../modals/SuccessModal";
import TransactionPincodeModal from "../../modals/TransactionPincodeModal";
import UserTypeGetter from "../../UserTypeGetter";
import { toast } from "react-toastify";
import IconMBank from "../../../icons/IconMBank";
import TransactionAccountPicker from "../../transaction/TransactionAccountPicker";
import { sha256 } from "js-sha256";
import TransactionConfirmSuccessModal from "../../modals/TransactionConfirmSuccessModal";
import TransactionReviewSuccessModal from "../../modals/TransactionReviewSuccessModal";
import TransactionWriteSuccessModal from "../../modals/TransactionWriteSuccessModal";
import ErrorCard from "../../main/ErrorCard";
import SelectedAccountCard from "../../accounts/SelectedAccountCard";
import DetailCard from "../../main/DetailCard";
import MButton from "../../buttons/MButton";
import NewOTPModal from "../../modals/NewOTPModal";

const LoanRepayment = () => {
  var history = useHistory();
  const location = useLocation();
  const loanInfo = location.state.loanInfo;
  const [description, setDescription] = useState("");
  const [amount, setAmount] = useState("");
  const [pageState, setPageState] = useState(0);
  const [pinModal, setPinModal] = useState(false);
  const [reviewModal, setReviewModal] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);
  const [writeModal, setWriteModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [loading, setLoading] = useState(true);
  const [caList, setCaList] = useState([]);
  const [fromAccount, setFromAccount] = useState({});
  const [accountsEmpty, setAccountsEmpty] = useState(false);
  const [otpModal, setOtpModal] = useState(false);
  const [userInfo, setUserInfo] = useState({});

  const getCAList = async () => {
    setLoading(true);
    await Axios.post(`/getCorpCAList`, {}).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        if (res.data.data.length > 0) {
          let temp = [...res.data.data];
          temp.map(
            (item, index) =>
              (temp[index].config = UserTypeGetter(item.permission))
          );
          if (temp.filter((item) => item.config.write === true).length > 0) {
            setCaList(temp.filter((item) => item.config.write === true));
            setFromAccount(
              temp.filter((item) => item.config.write === true)[0]
            );
          } else {
            setAccountsEmpty(true);
          }
        } else {
          setAccountsEmpty(true);
        }
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      setLoading(false);
    });
  };

  const clearData = () => {
    getCAList();
    setPageState(1);
    setDescription("");
    setAmount("");
  };

  const doTransaction = () => {
    if (parseFloat(amount) >= 20000000) {
    } else {
      writeLoanTransaction({
        otp: "",
        type: "",
        receiver: "",
      });
    }
  };

  const fromAccountPickerHandler = (data) => {
    setFromAccount(data);
  };

  const pinHandler = (data) => {
    setPincode(data);
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  useEffect(() => {
    getCAList();
  }, []);

  return loading ? (
    <IconMBank />
  ) : (
    <div className="mx-auto mb-7 w-89 rounded-lg bg-white py-7 pr-7 md:w-113">
      <LargeTitle title="Эргэн төлөлт" />
      <div className="pl-7">
        {accountsEmpty ? (
          <ErrorCard title="Танд шилжүүлэг илгээх боломжтой данс байхгүй байна." />
        ) : pageState === 0 ? (
          <>
            <TransactionAccountPicker
              title="Илгээх данс"
              accountList={caList}
              selectedAccount={fromAccount}
              onChange={fromAccountPickerHandler}
            />
            <div className="my-4 h-[1px] bg-border-100" />
            <div className="flex">
              <MInput
                maxLength={11}
                placeholder="Төлөх дүн оруулах"
                numberFormat
                onChange={(e) => {
                  setAmount(e);
                }}
              />
              <div className="ml-2 flex h-12 w-20 items-center justify-between rounded-lg bg-border-100 px-4">
                <div className="flex flex-col">
                  <p className="font-regular text-xs text-gray">Валют</p>
                  <p className="mt-[-1px] font-regular text-base text-black">
                    MNT
                  </p>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-2">
              <MInput
                maxLength={60}
                placeholder="Шилжүүлгийн утга"
                id="description"
                type="text"
                onChange={(e) => setDescription(e)}
              />
            </div>
            <div className="mb-20" />
            {parseFloat(amount) > fromAccount.currentAccount.balance_amt ? (
              <ErrorCard title="Илгээх дансны үлдэгдэл хүрэлцэхгүй байна." />
            ) : (
              <button
                disabled={amount === "" || description === ""}
                onClick={() => {
                  setPageState(1);
                }}
                className="group h-12 w-full rounded-lg bg-black disabled:bg-border-100"
              >
                <p className="font-bold text-base text-white group-disabled:text-gray">
                  Эргэн төлөлт хийх
                </p>
              </button>
            )}
          </>
        ) : (
          <div className="flex flex-col gap-2">
            <SelectedAccountCard
              title="Харилцах данс"
              acct_num={fromAccount.currentAccount.acct_num}
              acct_ccy={fromAccount.currentAccount.acct_ccy}
              balance_amt={fromAccount.currentAccount.positive_balance_amt}
            />
            <div className="my-2 h-[1px] bg-border-100" />
            <div className="flex gap-2">
              <DetailCard
                title="Төлөх дүн "
                description={formatter.format(amount).replaceAll("$", "")}
              />
              <div className="ml-2 flex h-13 w-20 items-center justify-between rounded-lg bg-border-100 px-4">
                <div className="flex flex-col">
                  <p className="font-regular text-xs text-gray">Валют</p>
                  <p className="mt-[-1px] font-regular text-xs text-black">
                    MNT
                  </p>
                </div>
              </div>
            </div>
            <DetailCard title="Шилжүүлгийн утга" description={description} />

            <div className="mb-18" />
            <MButton
              onClick={() => {
                setPinModal(true);
              }}
              disabled={false}
              title="Эргэн төлөлт баталгаажуулах"
            />
          </div>
        )}
        <TransactionPincodeModal
          onClose={() => {
            setPinModal(false);
          }}
          isOpen={pinModal}
          onChange={pinHandler}
          onClick={() => {
            setPinModal(false);
            doTransaction();
          }}
        />
        <NewOTPModal
          onSuccess={(data) => {
            setOtpModal(false);
            writeLoanTransaction(data);
          }}
          userInfo={userInfo}
          isOpen={otpModal}
          onClose={() => setOtpModal(false)}
        />
        <TransactionConfirmSuccessModal
          type="BETWEEN"
          isOpen={confirmModal}
          onClick={() => {
            setConfirmModal(false);
            clearData();
            history.goBack();
          }}
        />
        <TransactionReviewSuccessModal
          type="BETWEEN"
          isOpen={reviewModal}
          onClick={() => {
            setReviewModal(false);
            clearData();
            history.goBack();
          }}
        />
        <TransactionWriteSuccessModal
          type="BETWEEN"
          isOpen={writeModal}
          onClick={() => {
            setWriteModal(false);
            clearData();
            history.goBack();
          }}
        />
        <ErrorToast />
      </div>
    </div>
  );
};

export default LoanRepayment;
