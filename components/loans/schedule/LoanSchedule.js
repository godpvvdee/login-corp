import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import IconMBank from "../../../icons/IconMBank";
import ErrorToast from "../../main/ErrorToast";
import LargeTitle from "../../main/LargeTitle";
import { toast } from "react-toastify";

const LoanSchedule = () => {
  const location = useLocation();
  const loanInfo = location.state.loanInfo;
  const [loading, setLoading] = useState(true);
  const [scheduleList, setScheduleList] = useState([]);
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const getLoanSchedule = async () => {
    setLoading(true);
    await Axios.post("/getLoanSchedule", {
      acct_num: loanInfo.acct_num,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setScheduleList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  useEffect(() => {
    getLoanSchedule();
  }, []);

  return loading ? (
    <IconMBank />
  ) : (
    <div className="mx-7 rounded-lg bg-defaultBackground py-7 pr-7">
      <LargeTitle title="Төлбөрийн хуваарь" />

      <div className="flex flex-col gap-5 pl-7">
        {scheduleList.map((item, index) => (
          <>
            <div key={index} className="flex items-center">
              <div className="flex w-1/5 flex-col">
                <p className="font-regular text-base text-black">
                  {item.flow_dt.replaceAll("-", ".")}
                </p>
                <p className="mt-[-1px] font-regular text-xs text-gray">
                  Зээл төлөх огноо
                </p>
              </div>
              <div className="flex w-1/5 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.instl_amt).replaceAll("$", "")}
                </p>
                <p className="mt-[-1px] font-regular text-xs text-gray">
                  Төлөх дүн
                </p>
              </div>

              <div className="flex w-1/5 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.int_amt).replaceAll("$", "")}
                </p>
                <p className="mt-[-1px] font-regular text-xs text-gray">
                  Зээлийн хүү
                </p>
              </div>
              <div className="flex w-1/5 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.princ_amt).replaceAll("$", "")}
                </p>
                <p className="mt-[-1px] font-regular text-xs text-gray">
                  Үндсэн зээлээс хасагдах дүн
                </p>
              </div>
              <div className="flex w-1/5 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.princ_outstnding).replaceAll("$", "")}
                </p>
                <p className="mt-[-1px] font-regular text-xs text-gray">
                  Зээлийн үлдэгдэл
                </p>
              </div>
            </div>
            <div className="h-[1px] rounded-lg bg-border-100" />
          </>
        ))}
      </div>
      <ErrorToast />
    </div>
  );
};

export default LoanSchedule;
