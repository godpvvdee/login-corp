import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import ErrorToast from "../../main/ErrorToast";
import LargeTitle from "../../main/LargeTitle";
import MInput from "../../main/MInput";
import SuccessModal from "../../modals/SuccessModal";
import TransactionPincodeModal from "../../modals/TransactionPincodeModal";
import UserTypeGetter from "../../UserTypeGetter";
import { toast } from "react-toastify";
import IconMBank from "../../../icons/IconMBank";
import TransactionAccountPicker from "../../transaction/TransactionAccountPicker";
import TransactionConfirmSuccessModal from "../../modals/TransactionConfirmSuccessModal";
import TransactionReviewSuccessModal from "../../modals/TransactionReviewSuccessModal";
import TransactionWriteSuccessModal from "../../modals/TransactionWriteSuccessModal";
import { sha256 } from "js-sha256";
import ErrorCard from "../../main/ErrorCard";
import NewOTPModal from "../../modals/NewOTPModal";

const LoanClose = () => {
  var history = useHistory();
  const location = useLocation();
  const loanInfo = location.state.loanInfo;
  const [pageState, setPageState] = useState(0);
  const [pinModal, setPinModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [loading, setLoading] = useState(true);
  const [caList, setCaList] = useState([]);
  const [fromAccount, setFromAccount] = useState({});
  const [reviewModal, setReviewModal] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);
  const [writeModal, setWriteModal] = useState(false);
  const [accountsEmpty, setAccountsEmpty] = useState(false);
  const [otpModal, setOtpModal] = useState(false);
  const [userInfo, setUserInfo] = useState({});

  const clearData = () => {
    getCAList();
    setPageState(1);
  };

  const fromAccountPickerHandler = (data) => {
    setFromAccount(data);
  };

  const pinHandler = (data) => {
    setPincode(data);
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  useEffect(() => {
    getCAList();
  }, []);

  return loading ? (
    <IconMBank />
  ) : (
    <div className="mx-auto mb-7 w-89 rounded-lg bg-white py-7 pr-7 md:w-113">
      <LargeTitle title="Эргэн төлөлт" />
      <div className="pl-7">
        {accountsEmpty ? (
          <ErrorCard title="Танд шилжүүлэг илгээх боломжтой данс байхгүй байна." />
        ) : pageState === 0 ? (
          <>
            <TransactionAccountPicker
              title="Илгээх данс"
              accountList={caList}
              selectedAccount={fromAccount}
              onChange={fromAccountPickerHandler}
            />
            <div className="my-4 h-[1px] bg-border-100" />

            <div className="flex flex-col gap-2">
              <div className="flex">
                <div className="flex h-13 w-full flex-col justify-center rounded-lg bg-border-100 px-4">
                  <p className="font-regular text-xs text-gray">
                    Зээл хаах дүн
                  </p>
                  <p className="mt-[-1px] font-regular text-base text-black">
                    {formatter.format(loanInfo.payoff_amt).replaceAll("$", "")}
                  </p>
                </div>
                <div className="ml-2 flex h-12 w-20 items-center justify-between rounded-lg bg-border-100 px-4">
                  <div className="flex flex-col">
                    <p className="font-regular text-xs text-gray">Валют</p>
                    <p className="mt-[-1px] font-regular text-base text-black">
                      MNT
                    </p>
                  </div>
                </div>
              </div>
              <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                <p className="font-regular text-xs text-gray">
                  Шилжүүлгийн ута
                </p>
                <p className="mt-[-1px] font-regular text-base text-black">
                  {loanInfo.schm_name} хаав
                </p>
              </div>
            </div>
            <div className="mb-20" />
            {loanInfo.payoff_amt >
            fromAccount.currentAccount.positive_balance_amt ? (
              <ErrorCard title="Илгээх дансны үлдэгдэл хүрэлцэхгүй байна." />
            ) : (
              <button
                onClick={() => {
                  setPageState(1);
                }}
                className="group h-12 w-full rounded-lg bg-black disabled:bg-border-100"
              >
                <p className="font-bold text-base text-white group-disabled:text-gray">
                  Зээл хаах
                </p>
              </button>
            )}
          </>
        ) : (
          <div className="flex flex-col gap-2">
            <div className="flex h-16 items-center justify-between rounded-lg bg-border-100 px-4">
              <div className="flex flex-col">
                <p className="font-regular text-xs text-gray">Харилцах данс</p>
                <p className="mt-[-1px] font-regular text-base text-black">
                  {fromAccount.currentAccount.acct_num +
                    " " +
                    fromAccount.currentAccount.acct_ccy}
                </p>
              </div>
              <p className="font-semibold text-base text-black">
                {formatter
                  .format(fromAccount.currentAccount.positive_balance_amt)
                  .replaceAll("$", "")}
              </p>
            </div>
            <div className="my-2 h-[1px] bg-border-100" />
            <div className="flex">
              <div className="flex h-12 w-full items-center justify-between rounded-lg bg-border-100 px-4">
                <div className="flex flex-col">
                  <p className="font-regular text-xs text-gray">
                    Зээл хаах дүн
                  </p>
                  <p className="mt-[-1px] font-regular text-base text-black">
                    {formatter.format(loanInfo.payoff_amt).replaceAll("$", "")}
                  </p>
                </div>
              </div>
              <div className="ml-2 flex h-12 w-20 items-center justify-between rounded-lg bg-border-100 px-4">
                <div className="flex flex-col">
                  <p className="font-regular text-xs text-gray">Валют</p>
                  <p className="mt-[-1px] font-regular text-xs text-black">
                    MNT
                  </p>
                </div>
              </div>
            </div>
            <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
              <p className="font-regular text-xs text-gray">Шилжүүлгийн утга</p>
              <p className="mt-[-1px] font-regular text-base text-black">
                {loanInfo.schm_name} хаав
              </p>
            </div>
            <div className="mb-18" />
            <button
              onClick={() => {
                setPinModal(true);
              }}
              className="h-12 w-full rounded-lg bg-black"
            >
              <p className="font-bold text-base text-white">
                Зээл хаахыг баталгаажуулах
              </p>
            </button>
          </div>
        )}
        <TransactionPincodeModal
          onClose={() => {
            setPinModal(false);
          }}
          isOpen={pinModal}
          onChange={pinHandler}
          onClick={() => {
            setPinModal(false);
            doTransaction();
          }}
        />
        <NewOTPModal
          onSuccess={(data) => {
            setOtpModal(false);
            writeLoanTransaction(data);
          }}
          userInfo={userInfo}
          isOpen={otpModal}
          onClose={() => setOtpModal(false)}
        />
        <TransactionConfirmSuccessModal
          type="BETWEEN"
          isOpen={confirmModal}
          onClick={() => {
            setConfirmModal(false);
            clearData();
            history.goBack();
          }}
        />
        <TransactionReviewSuccessModal
          type="BETWEEN"
          isOpen={reviewModal}
          onClick={() => {
            setReviewModal(false);
            clearData();
            history.goBack();
          }}
        />
        <TransactionWriteSuccessModal
          type="BETWEEN"
          isOpen={writeModal}
          onClick={() => {
            setWriteModal(false);
            clearData();
            history.goBack();
          }}
        />
        <ErrorToast />
      </div>
    </div>
  );
};

export default LoanClose;
