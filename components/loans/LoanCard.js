import IconArrowRight from "../../icons/IconArrowRight";
import Link from "next/link";
const LoanCard = (props) => {
  const formatBalance = (balance) => {
    const formattedBal = formatter.format(balance).replace("$", "").split(".");
    return (
      <p className="font-bold text-xl text-black group-hover:text-white">
        {formattedBal[0]}
        <span className="text-px18">.{formattedBal[1]}</span>
      </p>
    );
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  return (
    <Link
      href={{
        pathname: "/loan",
        state: {
          accNo: props.accountNo,
        },
      }}
    >
      <div className="group flex flex-col rounded-lg border border-border-100 bg-defaultBackground pl-5 pt-5 hover:bg-primary ">
        <div className="mb-6 flex items-center justify-between pr-5">
          <p className="font-regular text-xs text-black group-hover:text-white">
            {props.name}
          </p>

          <svg
            className="h-[10px] w-[7px] fill-gray group-hover:fill-white"
            xmlns="http://www.w3.org/2000/svg"
          >
            <IconArrowRight />
          </svg>
        </div>
        {formatBalance(props.balance)}
        <div className="flex">
          <p className="pb-5 font-regular text-base text-gray group-hover:text-white">
            {props.accountNo.slice(0, 4) +
              " " +
              props.accountNo.slice(4, props.accountNo.length) +
              " " +
              props.currency}
          </p>
        </div>
      </div>
    </Link>
  );
};

export default LoanCard;
