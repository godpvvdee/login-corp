import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import IconMBank from "../../icons/IconMBank";
import ErrorToast from "../main/ErrorToast";
import LoanBalanceCard from "./details/LoanBalanceCard";
import LoanOptionsCard from "./details/LoanOptionsCard";
import LoanPaymentList from "./details/LoanPaymentList";
import { toast } from "react-toastify";

const LoanDetails = (props) => {
  const location = useLocation();
  const accountNo = location.state.accNo;
  const [loanInfo, setLoanInfo] = useState({});
  const [loanTransactionList, setLoanTransactionList] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    getLoanDetails();
  }, []);

  const getLoanDetails = async () => {
    setLoading(true);
    await Axios.post("/getLoanDetails", {
      acct_num: accountNo,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setLoanInfo(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    getLoanTransactionList();
  };

  const getLoanTransactionList = async () => {
    await Axios.post("/getLoanTransactionList", {
      acct_num: accountNo,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setLoanTransactionList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  return loading ? (
    <IconMBank />
  ) : (
    <div className="flex flex-col gap-7 p-7 md:flex-row">
      <div className="flex flex-col gap-7 md:w-2/3">
        <LoanBalanceCard loanInfo={loanInfo} />
        <LoanPaymentList loanTransactionList={loanTransactionList} />
      </div>
      <div className="flex flex-col gap-7 md:w-1/3">
        <LoanOptionsCard loanInfo={loanInfo} />
      </div>
      <ErrorToast />
    </div>
  );
};

export default LoanDetails;
