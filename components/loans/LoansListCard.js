import LargeTitle from "../main/LargeTitle";
import LoanCard from "./LoanCard";

const LoansListCard = (props) => {
  return props.loanList.length > 0 ? (
    <div className="rounded-lg bg-defaultBackground py-7 pr-7">
      <LargeTitle title="Зээлийн данс" />
      <div className="grid gap-7 pl-7 md:grid-cols-2">
        {props.loanList.map((item, index) => (
          <LoanCard
            key={index}
            currency={item.acct_ccy}
            balance={item.princ_amt}
            accountNo={item.acct_num}
            name={item.schm_name}
          />
        ))}
      </div>
    </div>
  ) : null;
};

export default LoansListCard;
