import React, { useState } from "react";
import AccountTransactionHistoryPicker from "../../accounts/details/AccountTransactionHistoryPicker";
import LargeTitle from "../../main/LargeTitle";

const LoanPaymentList = (props) => {
  const [loanTransactionList] = useState(props.loanTransactionList);
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  const filePickerItemsList = [
    {
      name: "PDF Файл",
    },
    {
      name: "XSLX Файл",
    },
  ];

  return (
    <div className="rounded-lg bg-defaultBackground py-7 pr-7">
      <div className="flex justify-between">
        <LargeTitle title="Зээл төлөлтийн түүх" />
        {/* <div className="flex flex-col">
          <AccountTransactionHistoryPicker itemsList={filePickerItemsList} />
        </div> */}
      </div>
      <div className="mt-2 hidden flex-col gap-5 pl-7 md:flex">
        {loanTransactionList.map((item, index) =>
          item.trn_type === "D" ? (
            <div key={index} className="flex items-center">
              <img
                src="/icons/ic_income.png"
                className="mr-2 h-10 w-10"
                alt=""
              />
              <div className="flex w-1/2 flex-col">
                <p className="font-regular text-base text-black">
                  {item.trn_desc}
                </p>
                <p className="font-regular text-xs text-gray">
                  {item.trn_time}
                </p>
              </div>
              <div className="flex w-1/2 flex-col text-right">
                <p className="font-semibold text-base text-black">
                  {formatter.format(item.disb_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">
                  Зээлийн нийт үлдэгдэл
                </p>
              </div>
            </div>
          ) : (
            <div key={index} className="flex items-center">
              <img
                src="/icons/ic_outcome.png"
                className="mr-2 h-10 w-10"
                alt=""
              />
              <div className="flex w-3/12 flex-col">
                <p className="font-regular text-base text-black">
                  {item.trn_desc}
                </p>
                <p className="font-regular text-xs text-gray">
                  {item.trn_time}
                </p>
              </div>
              <div className="flex w-2/12 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.trn_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">Зээлийн төлөлт</p>
              </div>
              <div className="flex w-2/12 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.int_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">Зээлийн хүү</p>
              </div>
              <div className="flex w-1/6 flex-col">
                <p className="font-regular text-base text-black">
                  {formatter.format(item.princ_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">
                  Үндсэн зээлээс хасагдах дүн
                </p>
              </div>
              <div className="flex w-3/12 flex-col text-right">
                <p className="font-semibold text-base text-black">
                  {formatter.format(item.aft_blnce_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">
                  Зээлийн нийт үлдэгдэл
                </p>
              </div>
            </div>
          )
        )}
      </div>
      <div className="mt-2 flex flex-col gap-5 pl-4 md:hidden">
        {loanTransactionList.map((item, index) =>
          item.trn_type === "D" ? (
            <div key={index} className="flex items-center">
              <div className="flex w-1/2 flex-col">
                <p className="font-regular text-base text-black">
                  {item.trn_desc}
                </p>
                <p className="font-regular text-xs text-gray">
                  {item.trn_time}
                </p>
              </div>
              <div className="flex w-1/2 flex-col text-right">
                <p className="font-semibold text-base text-greenSimpleColor">
                  {formatter.format(item.disb_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">
                  Зээлийн нийт үлдэгдэл
                </p>
              </div>
            </div>
          ) : (
            <div key={index} className="flex items-center">
              <div className="flex w-6/12 flex-col">
                <p className="font-regular text-base text-black">
                  {item.trn_desc}
                </p>
                <p className="font-regular text-xs text-gray">
                  {item.trn_time}
                </p>
              </div>

              <div className="flex w-6/12 flex-col text-right">
                <p className="font-semibold text-base text-black">
                  {formatter.format(item.trn_amt).replaceAll("$", "")}
                </p>
                <p className="font-regular text-xs text-gray">
                  {formatter.format(item.aft_blnce_amt).replaceAll("$", "")}
                </p>
              </div>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default LoanPaymentList;
