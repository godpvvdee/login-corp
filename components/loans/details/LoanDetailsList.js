import React from "react";
import DetailCard from "../../main/DetailCard";

const LoanDetailsList = (props) => {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  const detailsList = [
    {
      title: "Зээлийн дансны дугаар",
      description: props.loanInfo.acct_num,
    },
    {
      title: "Данс эзэмшигчийн нэр",
      description: props.loanInfo.acct_name,
    },
    {
      title: "Дансны төлөв",
      description: props.loanInfo.loan_stat,
    },
    {
      title: "Зээл олгосон дүн",
      description: formatter
        .format(props.loanInfo.loan_amt)
        .replaceAll("$", ""),
    },
    {
      title: "Зээлийн хүү (жилээр)",
      description: props.loanInfo.int_rate + "%",
    },
    {
      title: "Зээлийн хугацаа",
      description: props.loanInfo.loan_priod_mnths + " сар",
    },
    {
      title: "Үндсэн төлбөр",
      description: formatter
        .format(props.loanInfo.loan_amt)
        .replaceAll("$", ""),
    },
    {
      title: "Хүүгийн төлбөр",
      description: formatter.format(props.loanInfo.int_amt).replaceAll("$", ""),
    },
    {
      title: "Зээл хаах дүн",
      description: formatter
        .format(props.loanInfo.payoff_amt)
        .replaceAll("$", ""),
    },
    {
      title: "Дараагийн төлбөр хийх өдөр",
      description: props.loanInfo.next_pay_day,
    },
    {
      title: "Дараагийн төлбөрийн дүн",
      description: formatter
        .format(props.loanInfo.next_pay_amt)
        .replaceAll("$", ""),
    },
    {
      title: "Дуусах хугацаа",
      description:
        props.loanInfo.close_dt === null
          ? "-"
          : props.loanInfo.close_dt.replaceAll("-", "."),
    },
  ];
  return (
    <div className="flex flex-col gap-2">
      {detailsList.map((item, index) => (
        <DetailCard
          key={index}
          title={item.title}
          description={item.description}
        />
      ))}
    </div>
  );
};

export default LoanDetailsList;
