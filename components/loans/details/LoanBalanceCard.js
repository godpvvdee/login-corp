import React from "react";

const LoanBalanceCard = (props) => {
  const formatBalance = (balance) => {
    var formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });
    const formattedBal = formatter.format(balance).replace("$", "").split(".");
    return (
      <p className="font-bold text-3xl text-black">
        {formattedBal[0]}
        <span className="text-2xl">.{formattedBal[1]}</span>
      </p>
    );
  };
  return (
    <div className="flex flex-col rounded-lg bg-defaultBackground p-7 md:flex-row md:items-center md:justify-between">
      <div>
        <p className="font-semibold text-xs text-gray">Зээлийн үлдэгдэл</p>
        {formatBalance(props.loanInfo.princ_amt)}
      </div>
      <button className="≈ flex h-10 w-35 items-center rounded-lg bg-primary px-4 hover:opacity-90 md:w-auto">
        <img src="/icons/ic_transaction.png" className="mr-2 h-5 w-5" alt="" />
        <p className="mt-[-1px] font-bold text-xs text-defaultBackground">
          Зээл төлөх
        </p>
      </button>
    </div>
  );
};

export default LoanBalanceCard;
