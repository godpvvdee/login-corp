import React from "react";

const LoanOptionsButtonList = (props) => {
  return (
    <div className="mb-5 flex rounded-lg bg-border-100">
      <button
        onClick={() => {
          props.onButtonClick(0);
        }}
        className={`flex h-10 w-1/2 items-center justify-center rounded-lg border border-border-100 text-center ${
          props.selectedId === 0 ? "rounded-lg bg-white" : "bg-border-100"
        }`}
      >
        <p
          className={`mt-[-1px] font-bold text-xs ${
            props.selectedId === 0 ? "text-primary" : "text-black"
          }`}
        >
          Зээлийн дэлгэрэнгүй
        </p>
      </button>
      <button
        onClick={() => {
          props.onButtonClick(1);
        }}
        className={`flex h-10 w-1/2 items-center justify-center rounded-lg border border-border-100 text-center ${
          props.selectedId === 1 ? "rounded-lg bg-white" : "bg-border-100"
        }`}
      >
        <p
          className={`mt-[-1px] font-bold text-xs ${
            props.selectedId === 1 ? "text-primary" : "text-black"
          }`}
        >
          Дансны дэлгэрэнгүй
        </p>
      </button>
    </div>
  );
};

export default LoanOptionsButtonList;
