import React, { useState } from "react";
import LargeTitle from "../../main/LargeTitle";
import LoanDetailsList from "./LoanDetailsList";
import LoanOptionsButtonList from "./LoanOptionsButtonList";
import LoanOptionsList from "./LoanOptionsList";

const LoanOptionsCard = (props) => {
  const [buttonState, setButtonState] = useState(0);

  const buttonHandler = (id) => {
    setButtonState(id);
  };

  const getOptions = (id) => {
    switch (id) {
      case 0:
        return <LoanOptionsList loanInfo={props.loanInfo} />;
      case 1:
        return <LoanDetailsList loanInfo={props.loanInfo} />;
      default:
        return <div></div>;
    }
  };
  return (
    <div className="rounded-lg bg-defaultBackground py-7 pr-7">
      <LargeTitle title="Дансны мэдээлэл" />
      <div className="pl-4 md:pl-7">
        <LoanOptionsButtonList
          selectedId={buttonState}
          onButtonClick={buttonHandler}
        />
        {getOptions(buttonState)}
      </div>
    </div>
  );
};

export default LoanOptionsCard;
