import React from "react";
import { Link } from "react-router-dom";
import IconArrowRight from "../../../icons/IconArrowRight";

const LoanOptionsList = (props) => {
  const buttonsList = [
    {
      path: "/loan-schedule",
      title: "Төлбөрийн хуваарь",
    },
    {
      path: "/loan-repayment",
      title: "Зээл төлөх",
    },
    {
      path: "/loan-close",
      title: "Зээл хаах",
    },
  ];
  return (
    <div className="flex flex-col gap-2">
      {buttonsList.map((item, index) => (
        <Link
          to={{
            pathname: item.path,
            state: {
              loanInfo: props.loanInfo,
            },
          }}
        >
          <button
            key={index}
            className="group flex h-13 w-full items-center justify-between rounded-lg border border-border-100 bg-white px-4 hover:bg-primary"
          >
            <p className="font-regular text-base text-black group-hover:text-white">
              {item.title}
            </p>
            <svg
              className="h-[10px] w-[7px] fill-gray group-hover:fill-white"
              xmlns="http://www.w3.org/2000/svg"
            >
              <IconArrowRight />
            </svg>
          </button>
        </Link>
      ))}
    </div>
  );
};

export default LoanOptionsList;
