import React, { useState, useEffect } from "react";
import MInput from "../main/MInput";
import Axios from "axios";
import IconMBank from "../../icons/IconMBank";
import PhoneUsedYearsPicker from "./PhoneUsedYearsPicker";
import ProfileOTPModal from "./ProfileOTPModal";
import SuccessModal from "../modals/SuccessModal";
import { toast } from "react-toastify";
import ErrorToast from "../main/ErrorToast";
import UserModal from "../modals/UserModal";
import SmallTitle from "../main/SmallTitle";
import IconMBankModal from "../../icons/IconMBankModal";

const ProfilePersonalInfo = (props) => {
  const [outerLoading, setOuterLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isEmailOpen, setIsEmailOpen] = useState(false);
  const [isPhoneOpen, setIsPhoneOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [phoneYearSelectedId, setPhoneYearSelectedId] = useState(1);
  const [modalId, setModalId] = useState(1);
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [userInfo, setUserInfo] = useState({});
  const [otp, setOtp] = useState("");
  const [emailButtonState, setEmailButtonState] = useState(true);
  const [phoneButtonState, setPhoneButtonState] = useState(true);
  const [phoneSuccessModal, setPhoneSuccessModal] = useState(false);
  const [emailSuccessModal, setEmailSuccessModal] = useState(false);

  const phoneYears = [
    {
      id: 1,
      text: "1 жил хүртэл",
    },
    {
      id: 2,
      text: "1-2 жил",
    },
    {
      id: 3,
      text: "2-3 жил",
    },
    {
      id: 4,
      text: "3-аас дээш жил",
    },
  ];

  const getUserInfo = async () => {
    setLoading(true);
    await Axios.post("/getUserInfo").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const updateEmail = async () => {
    setLoading(true);
    await Axios.post("/updateEmail", {
      email: email,
      otp: otp,
    }).then((res) => {
      setOtp("");
      if (res.data.code.substring(0, 1) === "2") {
        setIsEmailOpen(false);
        setEmailSuccessModal(true);
        getUserInfo();
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const updatePhone = async () => {
    setLoading(true);
    await Axios.post("/updatePhone", {
      phone: phone,
      duration: phoneYearSelectedId,
      otp: otp,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setOtp("");
        setIsPhoneOpen(false);
        setPhoneSuccessModal(true);
        getUserInfo();
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  function validatePhoneNumber(phone) {
    var re = RegExp("^[0-9]{8}$");
    setPhoneButtonState(re.test(phone));
  }

  function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    setEmailButtonState(re.test(email));
  }

  const phoneYearHandler = (data) => {
    setPhoneYearSelectedId(data);
  };

  const otpChangeHandler = (data) => {
    setOtp(data);
  };

  const getPhoneOTP = async () => {
    setLoading(true);
    await Axios.post("/sendOTP", {
      receiver: phone,
      type: "phone",
      username: "",
      register: "",
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setModalId(2);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const getMailOTP = async () => {
    setLoading(true);
    await Axios.post("/sendOTP", {
      receiver: email,
      type: "email",
      username: "",
      register: "",
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setModalId(2);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  return outerLoading ? (
    <IconMBankModal />
  ) : (
    <>
      <div className="w-full">
        <button
          onClick={() => {
            props.onClick();
            setIsOpen(!isOpen);
          }}
          className={`group flex h-12 w-full items-center justify-between px-4 hover:bg-primary ${"mb-4 rounded-lg bg-border-100"}`}
        >
          <p
            className={`font-semibold text-base group-hover:text-white ${"text-black"}`}
          >
            Миний мэдээлэл
          </p>
        </button>
      </div>
      {isEmailOpen ? (
        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
          <div className="relative mx-auto w-89 md:w-113">
            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
              {loading ? (
                <IconMBank />
              ) : modalId === 2 ? (
                <ProfileOTPModal
                  onChange={otpChangeHandler}
                  value={otp}
                  loading={loading}
                  onCancel={() => {
                    props.onCancel();
                    setOtp("");
                    setModalId(1);
                    setIsEmailOpen(false);
                  }}
                  onValidate={() => {
                    setModalId(1);
                    updateEmail();
                  }}
                />
              ) : (
                <div className="flex flex-col p-9">
                  <div className="flex items-center justify-between">
                    <SmallTitle title="Шинэ и-мэйл хаягаа оруулна уу" />
                    <button
                      onClick={() => {
                        setIsEmailOpen(false);
                        props.onCancel();
                      }}
                    >
                      <img
                        src="/icons/ic_close_gray.png"
                        className="mt-[-20px] h-5 w-5"
                        alt=""
                      />
                    </button>
                  </div>
                  <MInput
                    maxLength={40}
                    placeholder="И-мэйл хаяг"
                    id="email"
                    value={email}
                    type="text"
                    onChange={(e) => {
                      validateEmail(e);
                      setEmail(e);
                    }}
                  />
                  <div className="mb-8" />
                  <button
                    onClick={() => getMailOTP()}
                    disabled={!emailButtonState}
                    className="group mx-auto h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
                  >
                    <p className="font-bold text-base text-white group-disabled:text-gray">
                      Хадгалах
                    </p>
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : null}
      {isPhoneOpen ? (
        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
          <div className="relative mx-auto w-89 md:w-113">
            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
              {loading ? (
                <IconMBank />
              ) : modalId === 2 ? (
                <ProfileOTPModal
                  onChange={otpChangeHandler}
                  value={otp}
                  loading={loading}
                  onCancel={() => {
                    setOtp("");
                    setModalId(1);
                    setIsPhoneOpen(false);
                    props.onCancel();
                  }}
                  onValidate={() => {
                    setModalId(1);
                    updatePhone();
                  }}
                />
              ) : (
                <div className="flex flex-col p-9">
                  <div className="flex items-center justify-between">
                    <SmallTitle title="Та шинэ гар утасны дугаараа оруулна уу" />
                    <button
                      onClick={() => {
                        setIsPhoneOpen(false);
                        props.onCancel();
                      }}
                    >
                      <img
                        src="/icons/ic_close_gray.png"
                        className="mt-[-20px] h-5 w-5"
                        alt=""
                      />
                    </button>
                  </div>
                  <MInput
                    maxLength={8}
                    placeholder="Утасны дугаар"
                    id="phone"
                    value={phone}
                    type="text"
                    onChange={(e) => {
                      validatePhoneNumber(e);
                      setPhone(e);
                    }}
                  />
                  <PhoneUsedYearsPicker
                    selectedId={phoneYearSelectedId}
                    yearsList={phoneYears}
                    onSelect={phoneYearHandler}
                  />
                  <div className="mb-8" />
                  <button
                    onClick={() => getPhoneOTP()}
                    disabled={!phoneButtonState}
                    className="group mx-auto h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
                  >
                    <p className="font-bold text-base text-white group-disabled:text-gray">
                      Хадгалах
                    </p>
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : null}
      <UserModal
        loading={loading}
        isOpen={isOpen}
        userInfo={userInfo}
        onClose={() => {
          setIsOpen(false);
          props.onCancel();
        }}
        onPhoneClick={() => {
          setIsOpen(false);
          setIsPhoneOpen(true);
        }}
        onEmailClick={() => {
          setIsOpen(false);
          setIsEmailOpen(true);
        }}
      />
      <SuccessModal
        isOpen={emailSuccessModal}
        title="Амжилттай"
        description="Таны и-мэйл солигдлоо, цаашид энэ хаяг руу нь холбоотой мэдээлэл очих болно."
        onClick={() => {
          setEmailSuccessModal(false);
          props.onCancel();
        }}
      />
      <SuccessModal
        isOpen={phoneSuccessModal}
        title="Амжилттай"
        description="Таны утасны дугаар солигдлоо."
        onClick={() => {
          setPhoneSuccessModal(false);
          props.onCancel();
        }}
      />
      <ErrorToast />
    </>
  );
};

export default ProfilePersonalInfo;
