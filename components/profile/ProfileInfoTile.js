import React from "react";

const ProfileInfoTile = (props) => {
  return (
    <div className="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
      <p className="font-regular text-xs text-gray">{props.title}</p>
      <p className="mt-[-1px] font-regular text-base text-black">
        {props.description}
      </p>
    </div>
  );
};

export default ProfileInfoTile;
