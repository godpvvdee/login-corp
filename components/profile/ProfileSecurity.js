import React, { useState } from "react";
import ErrorToast from "../main/ErrorToast";
import Link from "next/link";
const ProfileSecurity = (props) => {
  const [openModal, setOpenModal] = useState(false);

  return (
    <>
      <button
        onClick={() => {
          props.onClick();
          setOpenModal(true);
        }}
        className="group mb-4 flex h-12 items-center rounded-lg bg-border-100 px-4 hover:bg-primary"
      >
        <p className="font-semibold text-base text-black group-hover:text-white">
          Нууцлал, баталгаажуулалт
        </p>
      </button>
      {openModal ? (
        <div className="fixed inset-0 z-50 flex  items-center justify-center  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
          <div className="relative mx-auto max-h-screen w-89 md:w-113 ">
            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
              <div className="flex flex-col items-center p-9">
                <div className="flex flex-col items-center">
                  <div className="mb-[26px] rounded-full bg-[#f3f3f3] p-2">
                    <img
                      src={"/images/auto_logo.png"}
                      alt="LOGOGOGOGOGOGO"
                      height={"34px"}
                      width={"34px"}
                      data-aos-duration="1000"
                      data-aos="zoom-in"
                    />
                  </div>
                  <p className="mb-8 font-bold text-px16">
                    Давхар баталгаажуулалт
                  </p>
                </div>
                <Link href="/twofa" onClick={() => setOpenModal(false)}>
                  <a>
                    <button className="mb-2 flex h-16 w-full items-center justify-between rounded-lg bg-[#f4f6f8] pr-4 font-semibold text-base md:w-[400px]">
                      <div className="flex items-center">
                        <div className="ml-6 mr-3 h-10 w-10 rounded-full bg-white p-2">
                          <img
                            src={"/images/auto_logo.png"}
                            alt="LOGOGOGOGOGOGO"
                            height={"24px"}
                            width={"24px"}
                            data-aos-duration="1000"
                            data-aos="zoom-in"
                          />
                        </div>
                        Authenticator апп-аар код авах
                      </div>
                      <div className="-rotate-90 ">
                        <img
                          src={"/icons/ic_menu_picker_gray.png"}
                          alt="LOGOGOGOGOGOGO"
                          height={"20px"}
                          width={"20px"}
                          data-aos-duration="1000"
                          data-aos="zoom-in"
                        />
                      </div>
                    </button>
                  </a>
                </Link>
                <button className="mb-9 flex h-16 w-full items-center justify-between rounded-lg bg-[#f4f6f8] pr-4 font-semibold text-base md:w-[400px]">
                  <div className="flex items-center">
                    <div className="ml-6 mr-3 h-10 w-10 rounded-full bg-white">
                      <img
                        src={"/images/message.png"}
                        alt="LOGOGOGOGOGOGO"
                        height={"40px"}
                        width={"40px"}
                        data-aos-duration="1000"
                        data-aos="zoom-in"
                      />
                    </div>
                    Мессеж-ээр код авах
                  </div>
                  <div className="-rotate-90">
                    <img
                      src={"/icons/ic_menu_picker_gray.png"}
                      alt="LOGOGOGOGOGOGO"
                      height={"20px"}
                      width={"20px"}
                      data-aos-duration="1000"
                      data-aos="zoom-in"
                    />
                  </div>
                </button>

                <div className="text-center">
                  <button
                    onClick={() => setOpenModal(false)}
                    className={` mb-7 w-50 rounded-lg bg-black p-3 font-bold text-base text-white hover:shadow-xl`}
                  >
                    Буцах
                  </button>
                </div>
              </div>
            </div>
          </div>
          <ErrorToast />
        </div>
      ) : null}
    </>
  );
};

export default ProfileSecurity;
