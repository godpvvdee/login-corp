import React, { useState } from "react";
import IconMBankModal from "../../icons/IconMBankModal";
import LargeTitle from "../main/LargeTitle";
import MInput from "../main/MInput";

const ProfileChangePinCode = (props) => {
  const [oldPincode, setOldPincode] = useState("");
  const [newPincode, setNewPincode] = useState("");
  const [newPincodeRepeat, setNewPincodeRepeat] = useState("");
  return (
    <>
      <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
        <div className="relative mx-auto w-89 md:w-113">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
            {props.loading ? (
              <IconMBankModal />
            ) : (
              <div className="flex flex-col p-9">
                <div className="flex items-center justify-between">
                  <LargeTitle title="Пин код солих" />
                  <button onClick={() => props.onClose()}>
                    <img
                      src="/icons/ic_close_gray.png"
                      className="mt-[-20px] h-5 w-5"
                    />
                  </button>
                </div>
                <MInput
                  maxLength={6}
                  placeholder="Хуучин пин код"
                  id="oldPass"
                  type="pin"
                  onChange={(e) => setOldPincode(e)}
                />
                <MInput
                  maxLength={6}
                  placeholder="Пин код"
                  id="oldPass"
                  type="pin"
                  onChange={(e) => setNewPincode(e)}
                />
                <MInput
                  maxLength={6}
                  placeholder="Пин код давтах"
                  id="oldPass"
                  type="pin"
                  onChange={(e) => setNewPincodeRepeat(e)}
                />
                <div className="mb-8" />
                <button
                  onClick={() =>
                    props.onClick({
                      oldPincode: oldPincode,
                      newPincode: newPincode,
                    })
                  }
                  disabled={
                    oldPincode === "" ||
                    newPincode === "" ||
                    newPincodeRepeat === "" ||
                    newPincode !== newPincodeRepeat ||
                    newPincode.length < 6 ||
                    newPincode === oldPincode
                  }
                  className="group mx-auto h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
                >
                  <p className="font-bold text-base text-white group-disabled:text-gray">
                    Хадгалах
                  </p>
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileChangePinCode;
