import axios from "axios";
import React, { useState } from "react";
import IconMBank from "../../icons/IconMBank";
import LargeTitle from "../main/LargeTitle";
import SmallTitle from "../main/SmallTitle";
import ProfileInfoTile from "./ProfileInfoTile";
import { toast } from "react-toastify";
import ErrorToast from "../main/ErrorToast";

const ProfileCorporateInfo = (props) => {
  const [openModal, setOpenModal] = useState(false);
  const [isFirstDropdownOpen, setIsFirstDropdownOpen] = useState(false);
  const [isSecondDropdownOpen, setIsSecondDropdownOpen] = useState(false);
  const [corporateInfo, setCorporateInfo] = useState({});
  const [signatures, setSignatures] = useState([]);
  const [loading, setLoading] = useState(false);

  const getCorporateInfo = async () => {
    setLoading(true);
    await axios.post("/getCorporateInfo").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setCorporateInfo(res.data.data);
        setSignatures(res.data.data.signatures);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  return (
    <>
      <button
        onClick={() => {
          props.onClick();
          setOpenModal(true);
          getCorporateInfo();
        }}
        className="group mb-4 flex h-12 items-center rounded-lg bg-border-100 px-4 hover:bg-primary"
      >
        <p className="font-semibold text-base text-black group-hover:text-white">
          Байгууллагын мэдээлэл
        </p>
      </button>
      {openModal ? (
        <div className="fixed inset-0 z-50 flex  items-center justify-center  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
          <div className="relative mx-auto max-h-screen w-89 md:w-113 ">
            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
              {loading ? (
                <IconMBank />
              ) : (
                <div className="flex flex-col p-9">
                  <div className="flex items-center justify-between">
                    <LargeTitle title="Байгууллагын мэдээлэл" />
                    <button onClick={() => setOpenModal(false)}>
                      <img
                        src="/icons/ic_close_gray.png"
                        className="mt-[-20px] h-5 w-5"
                        alt=""
                      />
                    </button>
                  </div>
                  <div className="flex flex-col gap-2">
                    <ProfileInfoTile
                      title="Байгууллагын нэр"
                      description={corporateInfo.general.name}
                    />
                    <ProfileInfoTile
                      title="Регистрийн дугаар"
                      description={corporateInfo.general.register}
                    />
                    <ProfileInfoTile
                      title="Байгууллагын утас"
                      description={
                        corporateInfo.general.contactInformation.phoneNumber
                          .phoneNumOne
                      }
                    />
                    <ProfileInfoTile
                      title="Байгууллагын и-мэйл"
                      description={
                        corporateInfo.general.contactInformation.email.email
                      }
                    />
                  </div>
                  <div className="mb-9" />
                  <SmallTitle title="Хариуцсан эрх" />
                  <button
                    onClick={() => setIsFirstDropdownOpen(!isFirstDropdownOpen)}
                    className={`flex h-13  items-center justify-between rounded-lg px-4 ${
                      isFirstDropdownOpen
                        ? "mb-0 bg-primary"
                        : "mb-4 bg-border-100"
                    }`}
                  >
                    <p
                      className={`font-regular  text-base ${
                        isFirstDropdownOpen ? "text-white" : "text-black"
                      }`}
                    >
                      1-р түвшний эрхүүд
                    </p>
                    <img
                      src={
                        isFirstDropdownOpen
                          ? "/icons/ic_menu_picker_true.png"
                          : "/icons/ic_menu_picker_gray.png"
                      }
                      alt=""
                      className="h-5 w-5"
                    />
                  </button>
                  {isFirstDropdownOpen ? (
                    signatures.map((item) => {
                      if (item.signatureNum === "1") {
                        return (
                          <div className="mb-4 flex flex-col gap-2 rounded-b-lg border border-border-100 bg-white p-4">
                            <ProfileInfoTile
                              title="Овог нэр"
                              description={
                                item.general.personName.lastName +
                                " " +
                                item.general.personName.firstName
                              }
                            />
                            <ProfileInfoTile
                              title="Албан тушаал"
                              description={item.employment.position}
                            />
                            <ProfileInfoTile
                              title="И-мэйл хаяг"
                              description={
                                item.general.contactInfo.emails[0].email
                              }
                            />
                            <ProfileInfoTile
                              title="Утасны дугаар"
                              description={
                                item.general.contactInfo.phoneNumbers[0]
                                  .phoneNum
                              }
                            />
                          </div>
                        );
                      } else {
                        return <div />;
                      }
                    })
                  ) : (
                    <div />
                  )}
                  <button
                    onClick={() =>
                      setIsSecondDropdownOpen(!isSecondDropdownOpen)
                    }
                    className={`flex h-13 items-center justify-between rounded-lg px-4 ${
                      isSecondDropdownOpen ? "bg-primary" : "bg-border-100 "
                    }`}
                  >
                    <p
                      className={`font-regular  text-base ${
                        isSecondDropdownOpen ? "text-white" : "text-black"
                      }`}
                    >
                      2-р түвшний эрхүүд
                    </p>
                    <img
                      src={
                        isSecondDropdownOpen
                          ? "/icons/ic_menu_picker_true.png"
                          : "/icons/ic_menu_picker_gray.png"
                      }
                      alt=""
                      className="h-5 w-5"
                    />
                  </button>
                  {isSecondDropdownOpen ? (
                    signatures.map((item) => {
                      if (item.signatureNum === "2") {
                        return (
                          <div className="mb-4 flex flex-col gap-2 rounded-b-lg border border-border-100 bg-white p-4">
                            <ProfileInfoTile
                              title="Овог нэр"
                              description={
                                item.general.personName.lastName +
                                " " +
                                item.general.personName.firstName
                              }
                            />
                            <ProfileInfoTile
                              title="Албан тушаал"
                              description={item.employment.position}
                            />
                            <ProfileInfoTile
                              title="И-мэйл хаяг"
                              description={
                                item.general.contactInfo.emails[0].email
                              }
                            />
                            <ProfileInfoTile
                              title="Утасны дугаар"
                              description={
                                item.general.contactInfo.phoneNumbers[0]
                                  .phoneNum
                              }
                            />
                          </div>
                        );
                      } else {
                        return <div />;
                      }
                    })
                  ) : (
                    <div />
                  )}
                </div>
              )}
            </div>
          </div>
          <ErrorToast />
        </div>
      ) : null}
    </>
  );
};

export default ProfileCorporateInfo;
