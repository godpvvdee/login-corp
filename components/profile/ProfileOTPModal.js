import React, { useState } from "react";
import LargeTitle from "../main/LargeTitle";
import OtpInput from "react-otp-input";
import IconMBank from "../../icons/IconMBank";

const ProfileOTPModal = (props) => {
  const [counter, setCounter] = React.useState(60);
  const [otp, setOtp] = useState("");

  React.useEffect(() => {
    counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
  }, [counter]);

  return props.loading ? (
    <IconMBank />
  ) : (
    <div className="flex flex-col p-9">
      <div className="flex items-center justify-between">
        <LargeTitle title="Баталгаажуулах" />
        <button onClick={() => props.onCancel()}>
          <img
            src="/icons/ic_close_gray.png"
            className="mt-[-20px] h-5 w-5"
            alt=""
          />
        </button>
      </div>
      <OtpInput
        value={props.value}
        onChange={(data) => {
          setOtp(data);
          props.onChange(data);
        }}
        numInputs={4}
        containerStyle="outer__otp"
        inputStyle="otp small"
        focusStyle="otp__focused"
      />

      <div className="mb-7" />
      {counter != 0 ? (
        <div className="mx-auto mb-5 flex h-10 w-24 items-center rounded-lg bg-border-100 text-center">
          <p className="mx-auto font-regular text-xs text-black">
            {counter} секунд
          </p>
        </div>
      ) : (
        <p
          onClick={() => {
            props.onRestart();
            setCounter(60);
          }}
          className="justify-center px-20 text-center font-regular text-xs text-gray"
        >
          Мессеж ирээгүй?
          <br />
          <span className="cursor-pointer font-semibold text-primary">
            Шинэ код авах
          </span>
        </p>
      )}
      <div className="mb-11" />
      <button
        disabled={otp.length != 4}
        onClick={() => props.onValidate()}
        className="group mx-auto h-12 w-48 items-center rounded-lg bg-black disabled:bg-border-100"
      >
        <p className="font-bold text-base text-white group-disabled:text-gray">
          Батлах
        </p>
      </button>
    </div>
  );
};

export default ProfileOTPModal;
