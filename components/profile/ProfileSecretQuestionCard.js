import React, { useState } from "react";
import IconMBankModal from "../../icons/IconMBankModal";
import DetailCard from "../main/DetailCard";
import LargeTitle from "../main/LargeTitle";
import MInput from "../main/MInput";
import MPicker from "../MPicker";

const ProfileSecretQuestionCard = (props) => {
  const [questionModalState, setQuestionModalState] = useState(
    props.questionModalState
  );
  const [questionList, setQuestionList] = useState(props.questionsList);
  const [availableQuestions, setavailableQuestions] = useState(
    props.questionsList.filter(
      (item) =>
        item.name !== props.secretQA.question1 &&
        item.name !== props.secretQA.question2 &&
        item.name !== props.secretQA.question3
    )
  );
  const [answer1, setAnswer1] = useState("");
  const [answer2, setAnswer2] = useState("");
  const [answer3, setAnswer3] = useState("");
  const [question1, setQuestion1] = useState({
    name: props.secretQA.question1,
  });
  const [question2, setQuestion2] = useState({
    name: props.secretQA.question2,
  });
  const [question3, setQuestion3] = useState({
    name: props.secretQA.question3,
  });

  const questionHandler = (data, index) => {
    var temp = questionList;
    temp = temp.filter((item) => item.name !== data.name);

    if (index === 1) {
      setQuestion1(data);
      temp = temp.filter((item) => item.name !== question2.name);
      temp = temp.filter((item) => item.name !== question3.name);
    } else if (index === 2) {
      setQuestion2(data);
      temp = temp.filter((item) => item.name !== question1.name);
      temp = temp.filter((item) => item.name !== question3.name);
    } else {
      setQuestion3(data);
      temp = temp.filter((item) => item.name !== question1.name);
      temp = temp.filter((item) => item.name !== question2.name);
    }
    setavailableQuestions(temp);
  };

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
      <div className="relative mx-auto w-89 md:w-113">
        <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
          {props.loading ? (
            <IconMBankModal />
          ) : questionModalState === 0 ? (
            <div className="flex flex-col p-9">
              <div className="flex items-center justify-between">
                <LargeTitle title="Нууц асуулт, хариулт" />
                <button
                  onClick={() => {
                    props.onCancel();
                  }}
                >
                  <img
                    src="/icons/ic_close_gray.png"
                    className="mt-[-20px] h-5 w-5"
                    alt=""
                  />
                </button>
              </div>
              <div className="flex flex-col gap-2">
                <DetailCard title="Асуулт 1" description={question1.name} />
                <DetailCard title="Асуулт 2" description={question2.name} />
                <DetailCard title="Асуулт 3" description={question3.name} />
              </div>
              <div className="mb-8" />
              <button
                onClick={() => {
                  setQuestionModalState(1);
                }}
                disabled={false}
                className="group mx-auto h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
              >
                <p className="font-bold text-base text-white group-disabled:text-gray">
                  Дахин шинэчлэх
                </p>
              </button>
            </div>
          ) : questionModalState === 1 ? (
            <div className="flex max-h-[90vh] flex-col overflow-y-scroll p-9">
              <div className="flex items-center justify-between">
                <LargeTitle title="Нууц асуулт, хариулт бөглөнө үү" />
                <button
                  onClick={() => {
                    props.onCancel();
                  }}
                >
                  <img
                    src="/icons/ic_close_gray.png"
                    alt=""
                    className="mt-[-20px] h-5 w-5"
                  />
                </button>
              </div>
              <div className="mt-4 flex flex-col gap-2">
                <MPicker
                  selectedItem={question1}
                  title="Асуулт 1"
                  items={availableQuestions}
                  onSelect={(data) => {
                    questionHandler(data, 1);
                  }}
                />
                <MInput
                  placeholder="Хариулт"
                  id="answer1"
                  value={answer1}
                  type="text"
                  maxLength={30}
                  onChange={(e) => {
                    setAnswer1(e);
                  }}
                />
                <div className="mb-2 h-[1px] bg-border-100" />
                <MPicker
                  selectedItem={question2}
                  title="Асуулт 2"
                  items={availableQuestions}
                  onSelect={(data) => {
                    questionHandler(data, 2);
                  }}
                />
                <MInput
                  placeholder="Хариулт"
                  id="answer1"
                  value={answer2}
                  type="text"
                  maxLength={30}
                  onChange={(e) => {
                    setAnswer2(e);
                  }}
                />
                <div className="mb-2 h-[1px] bg-border-100" />
                <MPicker
                  selectedItem={question3}
                  title="Асуулт 3"
                  items={availableQuestions}
                  onSelect={(data) => {
                    questionHandler(data, 3);
                  }}
                />
                <MInput
                  placeholder="Хариулт"
                  id="answer1"
                  value={answer3}
                  type="text"
                  maxLength={30}
                  onChange={(e) => {
                    setAnswer3(e);
                  }}
                />
              </div>
              <div className="mb-8" />
              <button
                onClick={() => {
                  props.onClick({
                    question1: question1.name,
                    question2: question2.name,
                    question3: question3.name,
                    answer1: answer1,
                    answer2: answer2,
                    answer3: answer3,
                  });
                }}
                disabled={
                  answer1.length < 1 || answer2.length < 1 || answer3.length < 1
                }
                className="group mx-auto min-h-[3rem] w-48 rounded-lg bg-black disabled:bg-border-100"
              >
                <p className="font-bold text-base text-white group-disabled:text-gray">
                  Шинэчлэх
                </p>
              </button>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default ProfileSecretQuestionCard;
