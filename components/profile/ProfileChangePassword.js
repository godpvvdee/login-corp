import Axios from "axios";
import React, { useState } from "react";
import IconMBank from "../../icons/IconMBank";
import LargeTitle from "../main/LargeTitle";
import MInput from "../main/MInput";
import SuccessModal from "../modals/SuccessModal";
import ProfileOTPModal from "./ProfileOTPModal";
import { toast } from "react-toastify";
import ErrorToast from "../main/ErrorToast";
import RequirementCheckbox from "../main/RequirementCheckbox";
import { UAParser } from "ua-parser-js";
import ProfileSecretQuestionCard from "./ProfileSecretQuestionCard";
import ProfileChangePinCode from "./ProfileChangePinCode";
import { sha256 } from "js-sha256";
import MInputPassword from "../main/MInputPassword";

const ProfileChangePassword = (props) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [questionModal, setQuestionModal] = useState(false);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [newPasswordRepeat, setNewPasswordRepeat] = useState("");
  const [loading, setLoading] = useState(false);
  const [successModal, setSuccessModal] = useState(false);
  const [modalId, setModalId] = useState(1);
  const [otp, setOtp] = useState("");
  const [phone, setPhone] = useState("");
  const [questionModalState, setQuestionModalState] = useState(0);
  const [questionsList, setQuestionsList] = useState([]);
  const [secretQA, setSecretQA] = useState();
  const [isCap, setIsCap] = useState(false);
  const [isLength, setIsLength] = useState(false);
  const [isSpecial, setIsSpecial] = useState(false);
  const [isNumber, setIsNumber] = useState(false);
  const [questionChangeModal, setQuestionChangeModal] = useState(false);
  const [pincodeChangeModal, setPincodeChangeModal] = useState(false);
  const [pincodeSuccessModal, setPincodeSuccessModal] = useState(false);
  const parser = UAParser();
  var CryptoJS = require("crypto-js");
  var capRegex = RegExp("[A-Z]");
  var numberRegex = RegExp("[0-9]");
  var specialRegex = RegExp('[!@#$%^&*(),.?":{}|<>]');
  const getSecurityQuestions = async () => {
    setLoading(true);
    await Axios.post("/getSecurityQuestions").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        var temp = [];
        res.data.data.map((item) => {
          temp.push({ name: item.question });
        });
        setQuestionsList(temp);
        getUserSecurityQuestions();
      } else {
        setLoading(false);
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
  };

  const getUserSecurityQuestions = async () => {
    await Axios.post("/getUserSecurityQuestions").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setSecretQA(res.data.data.securityAnswers);
        if (res.data.data.securityAnswers.question1 === undefined) {
          setQuestionModal(false);
        } else {
          setQuestionModal(true);
        }
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const updatePassword = async () => {
    setLoading(true);
    await Axios.post("/updatePassword", {
      newPassword: CryptoJS.AES.encrypt(
        JSON.stringify(newPassword),
        process.env.REACT_APP_HASH
      ).toString(),
      oldPassword: CryptoJS.AES.encrypt(
        JSON.stringify(oldPassword),
        process.env.REACT_APP_HASH
      ).toString(),
      deviceId: parser.ua,
      otp: otp,
      otpType: "phone",
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setOpenModal(false);
        setSuccessModal(true);
      } else {
        setOtp("");
        setIsCap(false);
        setIsLength(false);
        setIsSpecial(false);
        setIsNumber(false);
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const getUserPhone = async () => {
    setLoading(true);
    await Axios.post("/getUserPhone").then((res) => {
      if (res.status.toString().substring(0, 1) === "2") {
        setPhone(res.data.phone);
        getPhoneOTP(res.data.phone);
      } else {
        setPhone("");
      }
    });
  };

  const otpChangeHandler = (data) => {
    setOtp(data);
  };

  const getPhoneOTP = async (data) => {
    await Axios.post("/sendOTP", {
      receiver: data,
      type: "phone",
      username: "",
      register: "",
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setModalId(2);
      } else {
        setIsCap(false);
        setIsLength(false);
        setIsSpecial(false);
        setIsNumber(false);
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const updateSecurityQuestions = async (data) => {
    setLoading(true);
    await Axios.post("/updateSecurityQuestions", {
      question1: data.question1,
      question2: data.question2,
      question3: data.question3,
      answer1: data.answer1,
      answer2: data.answer2,
      answer3: data.answer3,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setQuestionModalState(0);
        setQuestionModal(false);
        setQuestionChangeModal(true);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const changePincode = async (data) => {
    setLoading(true);
    await Axios.post("/changePincode", {
      oldPincode: sha256(data.oldPincode),
      newPincode: sha256(data.newPincode),
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setPincodeChangeModal(false);
        setPincodeSuccessModal(true);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const firstPasswordHandler = (text) => {
    setNewPassword(text);
    setIsCap(capRegex.test(text));
    setIsLength(text.length >= 8);
    setIsSpecial(specialRegex.test(text));
    setIsNumber(numberRegex.test(text));
  };

  return (
    <>
      <button
        onClick={() => {
          props.onClick();
          setIsMenuOpen(!isMenuOpen);
        }}
        className={`${
          isMenuOpen ? "rounded-t-lg bg-primary" : "rounded-lg bg-border-100"
        } group flex h-12 items-center justify-between  px-4 hover:bg-primary`}
      >
        <p
          className={`${
            isMenuOpen ? "text-white" : "text-black"
          } font-semibold text-base group-hover:text-white `}
        >
          Хувийн тохиргоо
        </p>
        <img
          className="h-5 w-5"
          src={
            isMenuOpen
              ? "/icons/ic_menu_picker_true.png"
              : "/icons/ic_menu_picker_gray.png"
          }
          alt=""
        />
      </button>
      {isMenuOpen ? (
        <div className="flex flex-col gap-[1px] bg-border-100">
          <button
            onClick={() => {
              setOpenModal(true);
            }}
            className="group flex h-12 items-center border-l-2 border-white bg-white px-4 hover:border-primary hover:bg-primary/10"
          >
            <p className="font-semibold text-xs text-black group-hover:text-primary">
              Нууц үг солих
            </p>
          </button>
          <button
            onClick={() => {
              setPincodeChangeModal(true);
            }}
            className="group flex h-12 items-center border-l-2 border-white bg-white px-4 hover:border-primary hover:bg-primary/10"
          >
            <p className="font-semibold text-xs text-black group-hover:text-primary">
              Гүйлгээний пин код солих
            </p>
          </button>
          <button
            onClick={() => {
              getSecurityQuestions();
            }}
            className="group flex h-12 items-center border-l-2 border-white bg-white px-4 hover:border-primary hover:bg-primary/10"
          >
            <p className="font-semibold text-xs text-black group-hover:text-primary">
              Нууц асуулт, хариулт солих
            </p>
          </button>
        </div>
      ) : null}
      {pincodeChangeModal && (
        <ProfileChangePinCode
          loading={loading}
          onClose={() => setPincodeChangeModal(false)}
          onClick={(data) => changePincode(data)}
        />
      )}
      {questionModal && (
        <ProfileSecretQuestionCard
          loading={loading}
          questionsList={questionsList}
          secretQA={secretQA}
          questionModalState={questionModalState}
          questionModal={questionModal}
          onClick={(data) => updateSecurityQuestions(data)}
          onCancel={() => {
            props.onCancel();
            setQuestionModal(false);
          }}
        />
      )}
      {openModal ? (
        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
          <div className="relative mx-auto w-89 md:w-113">
            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
              {loading ? (
                <IconMBank />
              ) : modalId === 1 ? (
                <div className="flex flex-col p-9">
                  <div className="flex items-center justify-between">
                    <LargeTitle title="Нууц үг солих" />
                    <button
                      onClick={() => {
                        setOpenModal(false);
                        props.onCancel();
                      }}
                    >
                      <img
                        src="/icons/ic_close_gray.png"
                        className="mt-[-20px] h-5 w-5"
                        alt=""
                      />
                    </button>
                  </div>
                  <MInputPassword
                    maxLength={16}
                    placeholder="Хуучин нууц үг"
                    type="password"
                    onChange={(e) => setOldPassword(e)}
                  />
                  <MInputPassword
                    maxLength={16}
                    placeholder="Нууц үг"
                    id="pin"
                    type="password"
                    onChange={(e) => firstPasswordHandler(e)}
                  />
                  <MInputPassword
                    maxLength={16}
                    placeholder="Нууц үг давтах"
                    id="pin"
                    type="password"
                    onChange={(e) => setNewPasswordRepeat(e)}
                  />
                  <div className="h-7" />
                  <RequirementCheckbox
                    isTrue={isCap}
                    title="Том үсэг заавал орно"
                  />
                  <RequirementCheckbox
                    isTrue={isLength}
                    title="8 болон түүнээс дээш урттай"
                  />
                  <RequirementCheckbox
                    isTrue={isSpecial}
                    title="Тусгай тэмдэгт орно"
                  />
                  <RequirementCheckbox isTrue={isNumber} title="Тоо орно" />
                  <div className="mb-8" />
                  <button
                    onClick={() => getUserPhone()}
                    disabled={
                      oldPassword === "" ||
                      !isSpecial ||
                      !isNumber ||
                      !isCap ||
                      !isLength ||
                      newPassword !== newPasswordRepeat
                    }
                    className="group mx-auto h-12 w-48 rounded-lg bg-black disabled:bg-border-100"
                  >
                    <p className="font-bold text-base text-white group-disabled:text-gray">
                      Хадгалах
                    </p>
                  </button>
                </div>
              ) : (
                <ProfileOTPModal
                  onRestart={() => {
                    getUserPhone();
                  }}
                  onChange={otpChangeHandler}
                  value={otp}
                  loading={loading}
                  onCancel={() => {
                    setOtp("");
                    setModalId(1);
                    setOpenModal(false);
                    props.onCancel();
                  }}
                  onValidate={() => {
                    setModalId(1);
                    updatePassword();
                  }}
                />
              )}
            </div>
          </div>
        </div>
      ) : null}
      <SuccessModal
        isOpen={successModal}
        title="Амжилттай"
        description="Таны нууц үг солигдлоо, аюулгүй байдлыг нь сайн хангаарай."
        onClick={() => {
          setSuccessModal(false);
          props.onCancel();
        }}
      />
      <SuccessModal
        isOpen={questionChangeModal}
        title="Амжилттай"
        description="Таны нууц асуулт хариултын мэдээлэл шинэчлэгдлээ."
        onClick={() => {
          setQuestionModal(false);
          props.onCancel();
        }}
      />
      <SuccessModal
        isOpen={pincodeSuccessModal}
        title="Амжилттай"
        description="Таны гүйлгээний пин код солигдлоо, аюулгүй байдлыг нь сайн хангаарай."
        onClick={() => {
          setPincodeSuccessModal(false);
          props.onCancel();
        }}
      />
      <ErrorToast />
    </>
  );
};

export default ProfileChangePassword;
