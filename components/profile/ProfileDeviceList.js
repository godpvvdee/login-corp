import Axios from "axios";
import React, { useEffect, useState } from "react";
import LargeTitle from "../main/LargeTitle";
import { toast } from "react-toastify";
import ErrorToast from "../main/ErrorToast";
import IconMBank from "../../icons/IconMBank";

const ProfileDeviceList = (props) => {
  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [deviceList, setDeviceList] = useState([]);

  return (
    <>
      <button
        onClick={() => {
          props.onClick();
          setOpenModal(true);
        }}
        className="group mb-4 flex h-12 items-center rounded-lg bg-border-100 px-4 hover:bg-primary"
      >
        <p className="font-semibold text-base text-black group-hover:text-white">
          Хандсан төхөөрөмж
        </p>
      </button>
      {openModal ? (
        <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto  overflow-x-hidden bg-black bg-opacity-80 outline-none focus:outline-none">
          <div className="relative mx-auto w-89 md:w-113">
            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-2xl outline-none focus:outline-none">
              {loading ? (
                <IconMBank />
              ) : (
                <div className="flex flex-col p-9">
                  <LargeTitle title="Хандсан төхөөрөмж" />
                  <div className="flex flex-col gap-2">
                    {deviceList.map((item, index) => (
                      <div
                        key={index}
                        className="flex items-center justify-between rounded-lg bg-border-100 p-4"
                      >
                        <div className="flex items-center">
                          <div className="mr-4 w-5">
                            <img
                              src={
                                item.deviceType === "mobile"
                                  ? "/icons/ic_device_mobile.png"
                                  : "/icons/ic_device_laptop.png"
                              }
                              className={
                                item.deviceType === "mobile"
                                  ? "h-5 w-4"
                                  : "h-4 w-5"
                              }
                              alt=""
                            />
                          </div>

                          <div className=" flex flex-col">
                            <p className="font-regular text-base text-black">
                              {item.os + " • " + item.phone_mark}
                            </p>
                            <p className="w-[250px] font-regular text-xs text-gray">
                              {item.mac}
                            </p>
                            <p className="font-regular text-xs text-gray">
                              {item.created_on
                                .replaceAll("T", " ")
                                .replaceAll("Z", "")}
                            </p>
                          </div>
                        </div>
                        {/* <button>
                          <img
                            src="/icons/ic_close_gray.png"
                            className="w-5 h-5"
                            alt=""
                          />
                        </button> */}
                      </div>
                    ))}
                  </div>
                  <button
                    onClick={() => {
                      setOpenModal(false);
                    }}
                    className="mx-auto mt-4 h-12 rounded-lg bg-black px-16 hover:opacity-90"
                  >
                    <p className="font-bold  text-base text-white">Хаах</p>
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : null}
      <ErrorToast />
    </>
  );
};

export default ProfileDeviceList;
