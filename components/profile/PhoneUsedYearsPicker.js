import React, { useState } from "react";

const PhoneUsedYearsPicker = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedId, setSelectedId] = useState(props.selectedId);
  return (
    <>
      <button
        onClick={() => {
          setIsOpen(true);
        }}
        className="flex h-13 items-center justify-between rounded-lg bg-border-100 px-4"
      >
        <div className="flex flex-col text-left">
          <p className="font-regular text-xs text-gray">Ашигласан жил</p>
          <p className="font-regular text-base text-black">
            {props.yearsList.filter((item) => item.id === selectedId)[0].text}
          </p>
        </div>
        <img src="/icons/ic_menu_picker_gray.png" className="h-5 w-5" alt="" />
      </button>
      {isOpen ? (
        <div className="rounded-lg bg-white shadow-lg">
          {props.yearsList.map((item) => (
            <button
              onClick={() => {
                props.onSelect(item.id);
                setSelectedId(item.id);
                setIsOpen(false);
              }}
              className="group flex h-12 w-full items-center bg-white px-4 hover:bg-primary"
            >
              <p className="font-regular text-base text-black group-hover:text-white">
                {item.text}
              </p>
            </button>
          ))}
        </div>
      ) : null}
    </>
  );
};

export default PhoneUsedYearsPicker;
