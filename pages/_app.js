import "../styles/globals.css";
import SideBar from "../components/main/Sidebar";
import Appbar from "../components/main/Appbar";
import { useState, useEffect } from "react";
import { apiClient } from "@/config/api";
import { AuthProvider, ProtectRoute, useAuth } from "../contexts/auth";

function MyApp({ Component, pageProps }) {
  const [corporateList, setCorporateList] = useState([]);
  const [firstSelectedCorporate, setFirstSelectedCorporate] = useState({});

  const [loading, setLoading] = useState(false);

  const [isAuth, setIsAuth] = useState(false);
  const { isAuthenticated } = useAuth();
  const CorporateList = async () => {
    setLoading(true);
    await apiClient
      .get("/api", {
        params: {
          name: "corporateList",
          value: ``,
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          setCorporateList(res.data);
        } else {
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
  };
  const loginHandler = (data) => {
    setIsAuth(data);
  };
  const onCorporateListLogin = (data, cif) => {
    setFirstSelectedCorporate(
      data.filter((item) => item.corporate_cif == cif)[0]
    );
    setFilteredList(data.filter((item) => item.corporate_cif != cif));
  };
  const initChooseCorporate = (data) => {
    setFirstSelectedCorporate(data);
    setFilteredList(
      corporateList.filter((item) => item.corporate_cif != data.corporate_cif)
    );
  };
  const appbarChooseCorporateHandler = (data) => {
    setFirstSelectedCorporate(data);
    setFilteredList(
      corporateList.filter((item) => item.corporate_cif != data.corporate_cif)
    );
  };
  useEffect(() => {
    CorporateList();
  }, []);
  console.log("corporateList..", corporateList);
  return (
    <AuthProvider>
      <ProtectRoute>
        <div className="bg-border-100 lg:block lg:min-h-screen lg:w-full lg:bg-border-100 ">
          <Appbar
            chosenCorporate={firstSelectedCorporate}
            onLogout={isAuthenticated}
          >
            <div className="flex">
              <div className=" ml-0 min-h-screen w-full md:ml-75">
                <Component {...pageProps} />
              </div>
            </div>
          </Appbar>
        </div>
      </ProtectRoute>
    </AuthProvider>
  );
}

export default MyApp;
