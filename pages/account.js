import Axios from "axios";
import React, { useEffect, useState } from "react";
import AccountDetailsCard from "../components/accounts/AccountDetailsCard";
import AccountOptionsCard from "../components/accounts/details/AccountOptionsCard";
import TransactionInfoCard from "../components/accounts/TransactionInfoCard";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import IconMBank from "../icons/IconMBank";
import ErrorToast from "../components/main/ErrorToast";
import UserTypeGetter from "../components/UserTypeGetter";
import SuccessModal from "../components/modals/SuccessModal";
import { sha256 } from "js-sha256";
import PincodeCheckModal from "../components/modals/PincodeCheckModal";

const AccountDetails = (props) => {
  const [accountNo, setAccountNo] = useState();

  const [accountInfo, setAccountInfo] = useState([]);
  const [transactionsList, setTransactionsList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [lienList, setLienList] = useState([]);
  const [tranLoading, setTranLoading] = useState(true);
  const [beginDate, setBeginDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [selectedFilterIndex, setSelectedFilterIndex] = useState(0);
  const [emailSuccessModal, setEmailSuccessModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [pinModal, setPinModal] = useState(false);
  const [pinmodalLoading, setPinmodalLoading] = useState(false);
  const [emailData, setEmailData] = useState({});
  const [sendType, setSendType] = useState("");
  const [totalPages, setTotalPages] = useState(0);
  const [pagesList, setPagesList] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  const pinHandler = (data) => {
    setPincode(data);
  };

  const checkPincode = async () => {
    setPinmodalLoading(true);
    await Axios.post("/checkPincode", {
      pincode: sha256(pincode),
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setPincode("");
        setPinModal(false);
        sendType === "email"
          ? sendTransactionHistoryByEmail()
          : sendTransactionHistoryByFile();
      } else {
        setPincode("");
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setPinModal(false);
      }
    });
    setPinmodalLoading(false);
  };

  let today = new Date();
  let lastweek = new Date();
  lastweek.setDate(today.getDate() - 7);
  lastweek = new Date(lastweek);
  let lastmonth = new Date();
  lastmonth.setDate(today.getDate() - 30);
  lastmonth = new Date(lastmonth);
  let lasthalf = new Date();
  lasthalf.setDate(today.getDate() - 180);
  lasthalf = new Date(lasthalf);

  const getAccountDetailsHandler = async (data) => {
    let temp = {};
    await Axios.post(`/getCADetails`, {
      accNo: data,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        temp = res.data.data;
        temp.config = UserTypeGetter(temp.permissions);
        setAccountInfo(temp);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    getLienList(temp, data);
  };

  const getTransactionList = async (start, end, pageNumber, accNo) => {
    setBeginDate(start);
    setEndDate(end);
    setTranLoading(true);

    await Axios.post(`/getTransactionList`, {
      accountNo: accNo,
      page: pageNumber,
      endDate:
        new Date(end).getFullYear() +
        "-" +
        (new Date(end).getMonth() + 1) +
        "-" +
        new Date(end).getDate(),
      beginDate:
        new Date(start).getFullYear() +
        "-" +
        (new Date(start).getMonth() + 1) +
        "-" +
        new Date(start).getDate(),
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setTransactionsList(res.data.data);
        if (res.data.data.length > 0) {
          setTotalPages(res.data.data[0].total_num_pages);
          getPages(res.data.data[0].total_num_pages);
        } else {
          setTotalPages(0);
          setPagesList([]);
        }
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setTranLoading(false);
    setLoading(false);
  };

  const getPages = (data) => {
    var temp = [];
    for (var i = 1; i <= data; i++) {
      temp.push(i);
    }
    setPagesList(temp);
  };

  const getLienList = async (temp, accNo) => {
    await Axios.post(`/getLienList`, {
      accountNo: accNo,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setLienList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    if (temp.config.transactionhistory) {
      getTransactionList(lastweek, today, 1, accNo);
    } else {
      setBeginDate(lastweek);
      setEndDate(today);
      setLoading(false);
      setTranLoading(false);
    }
  };

  const handleAccountChange = (data) => {
    setLoading(true);
    setAccountNo(data);
    getAccountDetailsHandler(data);
    setCurrentPage(1);
    setSelectedFilterIndex(0);
  };

  const filterHandler = (data) => {
    setCurrentPage(1);
    if (data === 0) {
      getTransactionList(lastweek, today, 1, accountNo);
    } else if (data === 1) {
      getTransactionList(lastmonth, today, 1, accountNo);
    } else {
      getTransactionList(lasthalf, today, 1, accountNo);
    }
  };

  const sendTransactionHistoryByEmail = async () => {
    setLoading(true);
    await Axios.post("/getTransactionHistoryByEmail", {
      formatName: emailData.reportName,
      format: emailData.type,
      qr: emailData.isQr,
      accId: accountNo,
      start:
        (new Date(beginDate).getMonth() + 1).toString().padStart(2, "0") +
        "-" +
        new Date(beginDate).getDate().toString().padStart(2, "0") +
        "-" +
        new Date(endDate).getFullYear(),
      end:
        (new Date(endDate).getMonth() + 1).toString().padStart(2, "0") +
        "-" +
        new Date(endDate).getDate().toString().padStart(2, "0") +
        "-" +
        new Date(endDate).getFullYear(),
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setEmailSuccessModal(true);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const sendTransactionHistoryByFile = async () => {
    setLoading(true);
    await Axios.post("/getTransactionHistoryByFile", {
      formatName: emailData.reportName,
      format: emailData.type,
      qr: emailData.isQr,
      accId: accountNo,
      start:
        (new Date(beginDate).getMonth() + 1).toString().padStart(2, "0") +
        "-" +
        new Date(beginDate).getDate().toString().padStart(2, "0") +
        "-" +
        new Date(endDate).getFullYear(),
      end:
        (new Date(endDate).getMonth() + 1).toString().padStart(2, "0") +
        "-" +
        new Date(endDate).getDate().toString().padStart(2, "0") +
        "-" +
        new Date(endDate).getFullYear(),
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        downloadPDF(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  function downloadPDF(pdf) {
    if (emailData.type === "pdf") {
      const linkSource = `data:application/pdf;base64,${pdf}`;
      const downloadLink = document.createElement("a");
      const fileName = "AccountStatement.pdf";
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
    } else {
      const linkSource = `data:application/vnd.ms-excel;base64,${pdf}`;
      const downloadLink = document.createElement("a");
      const fileName = "AccountStatement.xls";
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
    }
  }

  useEffect(() => {
    setLoading(true);
    getAccountDetailsHandler(accountNo);
  }, []);
  return loading ? (
    <IconMBank />
  ) : (
    <div className="flex flex-col gap-7 p-7 3xl:flex-row">
      <div className="flex w-full flex-col gap-7 3xl:w-2/3">
        <AccountDetailsCard
          onAccountChange={handleAccountChange}
          accountInfo={accountInfo}
          accountNo={accountNo}
        />
        {tranLoading ? (
          <IconMBank />
        ) : (
          <TransactionInfoCard
            onEmailSend={(data) => {
              setSendType("email");
              setEmailData(data);
              setPinModal(true);
            }}
            onFileSend={(data) => {
              setSendType("file");
              setEmailData(data);
              setPinModal(true);
            }}
            accountInfo={accountInfo}
            selectedIndex={selectedFilterIndex}
            onIndexChange={(index) => {
              setSelectedFilterIndex(index);
            }}
            start={beginDate}
            end={endDate}
            onClick={(data) => filterHandler(data)}
            transactionsList={transactionsList}
            onCalendarFilter={(data1, data2) => {
              setCurrentPage(1);
              getTransactionList(data1, data2, 1, accountNo);
            }}
          />
        )}
        <div className="m-table mx-auto mt-5 grid w-1/2 gap-1 text-center">
          {pagesList.map((item, i) => (
            <div
              key={i}
              onClick={() => {
                setCurrentPage(item);
                getTransactionList(beginDate, endDate, item, accountNo);
              }}
              className={`group min-h-[26px] min-w-[26px] cursor-pointer rounded-md border bg-white ${
                currentPage === item
                  ? "border-primary"
                  : "border-white hover:bg-primary "
              }`}
            >
              <p
                className={`mt-[3px] text-xs  ${
                  currentPage === item
                    ? "font-bold text-primary"
                    : "font-regular text-black group-hover:text-white"
                }`}
              >
                {item}
              </p>
            </div>
          ))}
        </div>
      </div>
      <div className="flex w-full flex-col gap-7 3xl:w-1/3">
        <AccountOptionsCard accountInfo={accountInfo} liens={lienList} />
      </div>
      <ErrorToast />
      <PincodeCheckModal
        onClose={() => {
          setPinModal(false);
        }}
        isLoading={pinmodalLoading}
        isOpen={pinModal}
        onChange={pinHandler}
        onClick={() => {
          checkPincode();
        }}
      />
      <SuccessModal
        isOpen={emailSuccessModal}
        title="Амжилттай"
        description="Таны имэйл-рүү илгээгдлээ."
        onClick={() => {
          setEmailSuccessModal(false);
        }}
      />
    </div>
  );
};

export default AccountDetails;
