import Axios from "axios";
import React, { useEffect, useState } from "react";
import LargeTitle from "@/components/main/LargeTitle";
import IconMBank from "/icons/IconMBank";
import { toast } from "react-toastify";
import ErrorToast from "@/components/main/ErrorToast";
import TransactionDurationPicker from "@/components/transaction/TransactionDurationPicker";
import UserTypeGetter from "@/components/UserTypeGetter";
import MCalendarPicker from "@/components/MCalendarPicker";
import ErrorCard from "@/components/main/ErrorCard";
import TransactionCheckTable from "@/components/transaction/TransactionCheckTable";
import TransactionFilterButton from "@/components/transaction/TransactionFilterButton";

const arr = [
  { name: "Шилжүүлгийн дүн", val: "amount", mobName: "Дүн" },
  { name: "Шилжүүлгийн дугаар", val: "tranid", mobName: "Дугаар" },
  { name: "Харьцсан данс", val: "recievedAcc", mobName: "Данс" },
  { name: "Шилжүүлгийн утга", val: "desc", mobName: "Утга" },
];
// const arrCreditDebit = [
//   { name: "Орлого", val: "C" },
//   { name: "Зарлага", val: "D" },
// ];
// const arrTransactionType = [
//   { name: "Өөрийн данс хооронд", val: "OWN_ACC" },
//   { name: "Бусдын данс хооронд", val: "FROM_OTHER_ACC" },
//   { name: "Бусдын данс руу", val: "TO_OTHER_ACC" },
// ];
const TransactionCheck = () => {
  const [counters, setCounters] = useState("1");
  const [caList, setCaList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [transactionLoading, setTransactionLoading] = useState(false);
  const [filteredTransactions, setFilteredTransactions] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [beginDate, setBeginDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [startCalendarOpen, setStartCalendarOpen] = useState(false);
  const [endCalendarOpen, setEndCalendarOpen] = useState(false);
  const [banksList, setBanksList] = useState([]);
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  const [isFilterTransactionOpen, setIsFilterTransactionOpen] = useState(false);
  const [isCredit, setIsCredit] = useState(false);
  const [isDebit, setIsDebit] = useState(false);
  const [isOwnAcc, setIsOwnAcc] = useState(false);
  const [isFromOtherAcc, setIsFromOtherAcc] = useState(false);
  const [isToOtherAcc, setIsToOtherAcc] = useState(false);
  const [transactionAmount, setTransactionAmount] = useState("");
  const [transactionAmountValue, setTransactionAmountValue] = useState(false);
  const [isAccounts, setIsAccounts] = useState("");
  const [transactionFilterValues, setTransactionFilterValues] = useState("");
  const [transactionSearchFilterValues, setTransactionSearchFilterValues] =
    useState("");
  const [transactionFilters, setTransactionFilters] = useState(false);
  const [pagesList, setPagesList] = useState([]);
  const [currentPage, setCurrentPage] = useState("1");
  let today = new Date();
  let lastweek = new Date();
  lastweek.setDate(today.getDate() - 7);
  lastweek = new Date(lastweek);
  let lastmonth = new Date();
  lastmonth.setDate(today.getDate() - 30);
  lastmonth = new Date(lastmonth);
  let lasthalf = new Date();
  lasthalf.setDate(today.getDate() - 180);
  lasthalf = new Date(lasthalf);

  const getApprovedTransactions = async (start, end, pagenumber) => {
    if (start) {
      setBeginDate(start);
    } else {
      setBeginDate(lastweek);
    }
    setEndDate(end);
    setTransactionLoading(true);
    var creditDebit = `${isDebit ? isDebit + "," : ""}${
      isCredit ? isCredit + "," : ""
    }`;
    var transactionType = `${isOwnAcc ? isOwnAcc + "," : ""}${
      isFromOtherAcc ? isFromOtherAcc + "," : ""
    }${isToOtherAcc ? isToOtherAcc + "," : ""}`;
    setTransactionFilterValues(
      `${isDebit ? "Орлого" + "," : ""}${isCredit ? "Зарлага" + "," : ""}${
        isOwnAcc ? "Өөрийн данс хооронд" + "," : ""
      }${isFromOtherAcc ? "Бусдын данс хооронд" + "," : ""}${
        isToOtherAcc ? "Бусдын данс руу" + "," : ""
      }${isAccounts ? isAccounts + "," : ""}`
    );
    await Axios.post("/getApprovedTransactions", {
      enddate:
        new Date(end).getFullYear() +
        "-" +
        (new Date(end).getMonth() + 1) +
        "-" +
        new Date(end).getDate(),
      firstdate:
        new Date(start).getFullYear() +
        "-" +
        (new Date(start).getMonth() + 1) +
        "-" +
        new Date(start).getDate(),

      // pagenumber: pagenumber,
      creditDebit: creditDebit,
      transactionType: transactionType,
      amount: transactionAmount === "amount" ? transactionAmountValue : "",
      tranid: transactionAmount === "tranid" ? transactionAmountValue : "",
      desc: transactionAmount === "desc" ? transactionAmountValue : "",
      recievedAcc:
        transactionAmount === "recievedAcc" ? transactionAmountValue : "",
      accounts: isAccounts,
      pagenumber: pagenumber,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        if (res.data.data.length > 0) {
          let temp = [...res.data.data];
          temp.map(
            (item, index) =>
              (temp[index].config = UserTypeGetter(item.permission))
          );
          setFilteredTransactions(temp);
          getPages(res.data.data[0].total_num_pages);
        } else {
          setFilteredTransactions(res.data.data);
          setPagesList([]);
        }
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    getBanksList();
  };
  const getPages = (data) => {
    var temp = [];
    for (var i = 1; i <= data; i++) {
      temp.push(i);
    }
    setPagesList(temp);
  };

  const getBanksList = async () => {
    await Axios.post("/getBanksList").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setBanksList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setTransactionLoading(false);
        setLoading(false);
      }
    });
    setTransactionLoading(false);
    setLoading(false);
  };
  const getCAList = async () => {
    setLoading(true);

    await Axios.post(`/getCorpCAList`, {}).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setCaList(res?.data?.data);

        console.log("calist data", res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",

          autoClose: 3000,

          hideProgressBar: false,

          closeOnClick: true,

          pauseOnHover: true,

          draggable: true,

          progress: undefined,
        });
      }
    });
  };

  useEffect(() => {
    getCAList();
    if (counters === "1") {
      getApprovedTransactions(lastweek, today, 1);
    } else {
      getApprovedTransactions(beginDate, today, 1);
    }
    setCounters("2");
  }, [isCredit, isDebit, isOwnAcc, isFromOtherAcc, isToOtherAcc, isAccounts]);
  console.log("TRAS DASD ", transactionAmount);
  return !loading ? (
    <IconMBank />
  ) : (
    <div class="px-4 pb-7 md:px-7 ">
      <div class="w-full overflow-hidden rounded-lg bg-white pt-7">
        <div class="flex justify-between">
          <LargeTitle title="Шилжүүлэг лавлах" />
        </div>

        <div class="hidden px-7 md:block">
          <div class="mb-7 flex items-center justify-between">
            <div class="flex gap-2">
              <div class="flex flex-col">
                <button
                  onClick={() => {
                    setStartCalendarOpen(!startCalendarOpen);
                  }}
                  class="mr-5 flex h-13 items-center rounded-lg bg-border-100 px-4 py-2"
                >
                  <div class="mr-20 flex flex-col text-left">
                    <p class="font-regular text-xs text-gray">Эхлэх огноо </p>
                    <p class="font-regular text-base text-black">
                      {new Date(beginDate).getFullYear() +
                        "." +
                        (new Date(beginDate).getMonth() + 1) +
                        "." +
                        new Date(beginDate).getDate()}
                    </p>
                  </div>
                  <img class="h-5 w-5" src="/icons/ic_calendar.png" alt="" />
                </button>
                {startCalendarOpen ? (
                  <div>
                    {" "}
                    <div
                      className="absolute inset-0 h-full w-full"
                      onClick={() => {
                        setStartCalendarOpen(false);
                        setEndCalendarOpen(false);
                        setIsFilterOpen(false);
                        setIsFilterTransactionOpen(false);
                      }}
                    ></div>
                    <MCalendarPicker
                      isEnd={false}
                      date={beginDate}
                      onClick={(date) => {
                        setBeginDate(date);
                        setStartCalendarOpen(false);
                        setEndCalendarOpen(true);
                      }}
                    />
                  </div>
                ) : null}
              </div>
              <div class="flex flex-col">
                <button
                  onClick={() => {
                    setEndCalendarOpen(!endCalendarOpen);
                  }}
                  class="flex h-13 items-center rounded-lg bg-border-100 px-4 py-2"
                >
                  <div class="mr-20 flex flex-col text-justify">
                    <p class="font-regular text-xs text-gray">Дуусах огноо</p>
                    <p class="float-left font-regular text-base text-black">
                      {new Date(endDate).getFullYear() +
                        "." +
                        (new Date(endDate).getMonth() + 1) +
                        "." +
                        new Date(endDate).getDate()}
                    </p>
                  </div>
                  <img class="h-5 w-5" src="/icons/ic_calendar.png" alt="" />
                </button>
                {endCalendarOpen ? (
                  <div>
                    <div
                      className="absolute inset-0 h-full w-full"
                      onClick={() => {
                        setStartCalendarOpen(false);
                        setEndCalendarOpen(false);
                        setIsFilterOpen(false);
                        setIsFilterTransactionOpen(false);
                      }}
                    ></div>
                    <MCalendarPicker
                      isEnd={true}
                      start={beginDate}
                      date={endDate}
                      onClick={(date) => {
                        setEndDate(date);
                        setEndCalendarOpen(false);
                        getApprovedTransactions(beginDate, date, 1);
                      }}
                    />
                  </div>
                ) : null}
              </div>
              <div class="mx-3 h-13 w-[1px] bg-border-100" />
              <div class="flex flex-col">
                <button
                  onClick={() => {
                    setIsFilterOpen(!isFilterOpen);
                  }}
                  class="flex h-13 w-60 items-center justify-between rounded-lg bg-border-100 px-4 py-2"
                >
                  <p class="truncate font-regular text-base text-gray ">
                    {transactionFilterValues
                      ? transactionFilterValues
                      : "Шүүлт хийх"}{" "}
                  </p>

                  <img
                    class="h-5 w-5"
                    src={
                      isFilterOpen
                        ? "/icons/ic_menu_picker_green.png"
                        : "/icons/ic_menu_picker_gray.png"
                    }
                    alt=""
                  />
                </button>
                {isFilterOpen && (
                  <>
                    <div
                      className="absolute inset-0 h-full w-full"
                      onClick={() => {
                        setStartCalendarOpen(false);
                        setEndCalendarOpen(false);
                        setIsFilterOpen(false);
                        setIsFilterTransactionOpen(false);
                      }}
                    ></div>
                    <div class="absolute mt-14 w-60 rounded-lg border border-border-100 bg-white p-4 shadow-md">
                      <div class="flex flex-col gap-6">
                        <div class="flex flex-col gap-5">
                          <p class="font-bold text-base text-black">
                            Орлого, зарлага
                          </p>
                          <TransactionFilterButton
                            onClick={() => {
                              setIsDebit(isDebit ? "" : "C");
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                            isChecked={isDebit}
                            text="Орлого"
                          />
                          <TransactionFilterButton
                            onClick={() => {
                              setIsCredit(isCredit ? "" : "D");
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                            isChecked={isCredit}
                            text="Зарлага"
                          />
                        </div>
                        <div class="flex flex-col gap-5">
                          <p class="font-bold text-base text-black">
                            Шилжүүлгийн төрөл
                          </p>
                          <TransactionFilterButton
                            text="Өөрийн данс хооронд"
                            isChecked={isOwnAcc}
                            onClick={() => {
                              setIsOwnAcc(isOwnAcc ? "" : "OWN_ACC");
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                          />
                          <TransactionFilterButton
                            text="Бусдын данснаас"
                            isChecked={isFromOtherAcc}
                            onClick={() => {
                              setIsFromOtherAcc(
                                isFromOtherAcc ? "" : "FROM_OTHER_ACC"
                              );
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                          />
                          <TransactionFilterButton
                            text="Бусдын данс руу"
                            isChecked={isToOtherAcc}
                            onClick={() => {
                              setIsToOtherAcc(
                                isToOtherAcc ? "" : "TO_OTHER_ACC"
                              );
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                          />
                        </div>

                        <div class="flex flex-col gap-5">
                          <p class="font-bold text-base text-black">Данс</p>
                          {caList.map((acc, idx) => (
                            <TransactionFilterButton
                              key={idx}
                              text={acc.currentAccount?.acct_num}
                              onClick={() => {
                                setIsAccounts(
                                  isAccounts === acc.currentAccount?.acct_num
                                    ? ""
                                    : acc.currentAccount?.acct_num
                                );
                              }}
                              isChecked={
                                isAccounts === acc.currentAccount?.acct_num
                              }
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>

              {/* <div>
                <TransactionFilterPicker onFilter={filterHandler} />
              </div> */}
            </div>
            <div class="flex">
              <TransactionDurationPicker
                selectedIndex={selectedIndex}
                onClick={(data) => {
                  setSelectedIndex(data);
                  setCurrentPage(1);
                  if (data === 0) {
                    getApprovedTransactions(lastweek, today, 1);
                  } else if (data === 1) {
                    getApprovedTransactions(lastmonth, today, 1);
                  } else {
                    getApprovedTransactions(lasthalf, today, 1);
                  }
                }}
              />
            </div>
          </div>
          <div className="flex justify-end">
            <div class="flex flex-col">
              <button
                onClick={() => {
                  setIsFilterTransactionOpen(!isFilterTransactionOpen);
                }}
                class="flex h-10 w-50 items-center justify-between rounded-l-lg border-r-border-150 bg-secondaryBackground px-4 py-2"
              >
                <p class="font-semibold text-xs text-black">
                  {transactionSearchFilterValues
                    ? transactionSearchFilterValues
                    : "Шүүлт сонгох"}{" "}
                </p>
                <img
                  class="h-5 w-5"
                  src={
                    isFilterTransactionOpen
                      ? "/icons/ic_menu_picker_green.png"
                      : "/icons/ic_menu_picker_gray.png"
                  }
                  alt=""
                />
              </button>
              {isFilterTransactionOpen && (
                <>
                  <div
                    className="absolute inset-0 h-full w-full"
                    onClick={() => {
                      setStartCalendarOpen(false);
                      setEndCalendarOpen(false);
                      setIsFilterOpen(false);
                      setIsFilterTransactionOpen(false);
                    }}
                  ></div>
                  <div class="absolute mt-14 w-50 rounded-lg border border-border-100 bg-white p-4 shadow-md">
                    <div class="flex flex-col  gap-5">
                      {arr.map((aa, i) => (
                        <div key={i}>
                          <TransactionFilterButton
                            text={`${aa.name}`}
                            onClick={() => {
                              setTransactionAmount(
                                transactionAmount === aa.val ? "" : aa.val
                              );
                              setTransactionSearchFilterValues(
                                transactionAmount === aa.val ? "" : aa.name
                              );
                            }}
                            isChecked={transactionAmount === aa.val}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                </>
              )}
            </div>
            {transactionAmount ? (
              <input
                type="text"
                className="h-[40px] w-[154px] rounded-r-md bg-[#f4f6f8]  px-4 py-2 font-bold text-xs outline-none"
                placeholder="Шүүх утга оруулах"
                onChange={(e) => setTransactionAmountValue(e.target.value)}
              />
            ) : (
              <input
                disabled
                className="h-[40px] w-[154px] rounded-r-md bg-[#f4f6f8]  px-4 py-2 font-bold text-xs outline-none"
                placeholder="Шүүлтээ сонгон уу"
              />
            )}

            <button
              onClick={() => getApprovedTransactions(beginDate, today, 1)}
              className="ml-3 h-10 w-[56px] rounded-xl bg-[#22cda8] font-bold text-xs text-white"
            >
              Хайх
            </button>
          </div>
        </div>
        <div className="flex flex-col md:hidden ">
          <div className="mx-[18px] flex">
            <div class="flex flex-col">
              <button
                onClick={() => {
                  setIsFilterTransactionOpen(!isFilterTransactionOpen);
                }}
                class="flex h-11 w-[110px] items-center justify-between rounded-lg border-r-border-150 bg-secondaryBackground px-4 py-2"
              >
                <p class="font-semibold text-xs text-black">
                  {transactionSearchFilterValues
                    ? transactionSearchFilterValues
                    : "Төрөл"}{" "}
                </p>
                <img
                  class="h-5 w-5"
                  src={
                    isFilterTransactionOpen
                      ? "/icons/ic_menu_picker_green.png"
                      : "/icons/ic_menu_picker_gray.png"
                  }
                  alt=""
                />
              </button>
              {isFilterTransactionOpen && (
                <div
                  className="absolute inset-0 h-full w-full bg-black/60 "
                  onClick={() => setIsFilterTransactionOpen(false)}
                >
                  <div className="fixed bottom-0 right-0 left-0 z-50 min-h-[300px] overflow-x-hidden rounded-t-2xl  bg-white  ">
                    <div className="px-[18px] pt-4">
                      {arr.map((aa, i) => (
                        <div key={i} className="py-4 pl-[6px] text-black">
                          <TransactionFilterButton
                            text={`${aa.name}`}
                            onClick={() => {
                              setTransactionAmount(
                                transactionAmount === aa.val ? "" : aa.val
                              );
                              setTransactionSearchFilterValues(
                                transactionAmount === aa.val ? "" : aa.mobName
                              );
                            }}
                            isChecked={transactionAmount === aa.val}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              )}

              {transactionFilters && (
                <div className="absolute inset-0 h-screen w-full overflow-hidden bg-black/60">
                  <div className="fixed bottom-0 right-0 left-0 z-50 h-[500px] overflow-scroll overflow-x-hidden rounded-t-2xl  bg-white  ">
                    <div className="flex flex-col px-8 pt-6 pb-10">
                      <div className="flex">
                        {" "}
                        <div class="flex flex-col">
                          <button
                            onClick={() => {
                              setStartCalendarOpen(!startCalendarOpen);
                            }}
                            class="mr-3 flex w-40 items-center rounded-lg bg-border-100 px-4 py-2"
                          >
                            <div class="mr-5 flex flex-col text-left">
                              <p class="font-regular text-xs text-gray">
                                Эхлэх огноо{" "}
                              </p>
                              <p class="font-regular text-base text-black">
                                {new Date(beginDate).getFullYear() +
                                  "." +
                                  (new Date(beginDate).getMonth() + 1) +
                                  "." +
                                  new Date(beginDate).getDate()}
                              </p>
                            </div>
                            <img
                              class="h-4 w-4"
                              src="/icons/ic_calendar.png"
                              alt=""
                            />
                          </button>

                          {startCalendarOpen ? (
                            <div
                              className="absolute inset-0 h-full w-full bg-black/60 "
                              onClick={() => setStartCalendarOpen(false)}
                            >
                              <div className="fixed inset-0 left-10 top-40 z-50 overflow-x-hidden">
                                <MCalendarPicker
                                  isEnd={false}
                                  date={beginDate}
                                  onClick={(date) => {
                                    setBeginDate(date);
                                    setStartCalendarOpen(false);
                                    setEndCalendarOpen(true);
                                  }}
                                />
                              </div>
                            </div>
                          ) : null}
                        </div>
                        <div class="flex flex-col">
                          <button
                            onClick={() => {
                              setEndCalendarOpen(!endCalendarOpen);
                            }}
                            class="flex w-40 items-center rounded-lg bg-border-100 px-4 py-2"
                          >
                            <div class="mr-5 flex flex-col text-justify">
                              <p class="font-regular text-xs text-gray">
                                Дуусах огноо
                              </p>
                              <p class="float-left font-regular text-base text-black">
                                {new Date(endDate).getFullYear() +
                                  "." +
                                  (new Date(endDate).getMonth() + 1) +
                                  "." +
                                  new Date(endDate).getDate()}
                              </p>
                            </div>
                            <img
                              class="h-4 w-4"
                              src="/icons/ic_calendar.png"
                              alt=""
                            />
                          </button>
                          {endCalendarOpen ? (
                            <div className="absolute inset-0 h-full w-full bg-black/60 ">
                              <div className="fixed inset-0 left-10 top-40 z-50 overflow-x-hidden">
                                <MCalendarPicker
                                  isEnd={true}
                                  start={beginDate}
                                  date={endDate}
                                  onClick={(date) => {
                                    setEndDate(date);
                                    setEndCalendarOpen(false);
                                    getApprovedTransactions(beginDate, date, 1);
                                  }}
                                />
                              </div>
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <div class="flex py-5">
                        <TransactionDurationPicker
                          selectedIndex={selectedIndex}
                          onClick={(data) => {
                            setSelectedIndex(data);
                            if (data === 0) {
                              getApprovedTransactions(lastweek, today, 1);
                            } else if (data === 1) {
                              getApprovedTransactions(lastmonth, today, 1);
                            } else {
                              getApprovedTransactions(lasthalf, today, 1);
                            }
                          }}
                        />
                      </div>
                      <div class="flex flex-col gap-6">
                        <div class="flex flex-col gap-5">
                          <p class="font-bold text-base text-black">
                            Орлого, зарлага
                          </p>
                          <div className="flex gap-6">
                            <TransactionFilterButton
                              onClick={() => {
                                setIsDebit(isDebit ? "" : "C");
                                // getApprovedTransactions(beginDate, today, 1);
                              }}
                              isChecked={isDebit}
                              text="Орлого"
                            />
                            <TransactionFilterButton
                              onClick={() => {
                                setIsCredit(isCredit ? "" : "D");
                                // getApprovedTransactions(beginDate, today, 1);
                              }}
                              isChecked={isCredit}
                              text="Зарлага"
                            />
                          </div>
                        </div>
                        <div class="flex flex-col gap-5">
                          <p class="font-bold text-base text-black">
                            Шилжүүлгийн төрөл
                          </p>
                          <TransactionFilterButton
                            text="Өөрийн данс хооронд"
                            isChecked={isOwnAcc}
                            onClick={() => {
                              setIsOwnAcc(isOwnAcc ? "" : "OWN_ACC");
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                          />
                          <TransactionFilterButton
                            text="Бусдын данснаас"
                            isChecked={isFromOtherAcc}
                            onClick={() => {
                              setIsFromOtherAcc(
                                isFromOtherAcc ? "" : "FROM_OTHER_ACC"
                              );
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                          />
                          <TransactionFilterButton
                            text="Бусдын данс руу"
                            isChecked={isToOtherAcc}
                            onClick={() => {
                              setIsToOtherAcc(
                                isToOtherAcc ? "" : "TO_OTHER_ACC"
                              );
                              // getApprovedTransactions(beginDate, today, 1);
                            }}
                          />
                        </div>

                        <div class="flex flex-col gap-5">
                          <p class="font-bold text-base text-black">Данс</p>
                          <div className="mb-9 flex flex-col gap-6 ">
                            {caList.map((acc, idx) => (
                              <TransactionFilterButton
                                key={idx}
                                text={acc.currentAccount?.acct_num}
                                onClick={() => {
                                  setIsAccounts(
                                    isAccounts === acc.currentAccount?.acct_num
                                      ? ""
                                      : acc.currentAccount?.acct_num
                                  );
                                }}
                                isChecked={
                                  isAccounts === acc.currentAccount?.acct_num
                                }
                              />
                            ))}
                          </div>
                        </div>
                      </div>
                      <button
                        onClick={() => {
                          setTransactionFilters(false);
                        }}
                        className="w-[325px] rounded-lg bg-greenSimpleColor py-[15px] text-xs text-white"
                      >
                        Шүүлт сонгох
                      </button>
                    </div>
                  </div>
                  <div
                    className="h-full w-screen"
                    onClick={() => {
                      setTransactionFilters(false);
                    }}
                  ></div>
                </div>
              )}
            </div>
            {transactionAmount ? (
              <input
                type="text"
                className="ml-2 h-11 w-full rounded-lg bg-[#f4f6f8]  px-4 py-2 font-bold text-xs outline-none"
                placeholder="Хайх утга"
                onChange={(e) => {
                  setTransactionAmountValue(e.target.value);
                  getApprovedTransactions(beginDate, today, 1);
                }}
              />
            ) : (
              <input
                type="text"
                disabled
                className="ml-2 h-11 w-full rounded-lg bg-[#f4f6f8]  px-4 py-2 font-bold text-xs outline-none"
                placeholder="Хайх төрлөө сонгон уу"
              />
            )}
          </div>
          <button
            onClick={() => setTransactionFilters(true)}
            className="mx-[18px] mt-2 h-11 rounded-lg bg-secondaryBackground text-base text-[#000000]"
          >
            Шүүлт
          </button>
        </div>

        {transactionLoading ? (
          <IconMBank />
        ) : filteredTransactions.length > 0 ? (
          <>
            {" "}
            <TransactionCheckTable
              data={filteredTransactions}
              banksList={banksList}
            />
            <div className="m-table my-5 mx-auto grid w-1/2 gap-1 text-center">
              {pagesList.map((item) => (
                <div
                  onClick={() => {
                    setCurrentPage(item);
                    getApprovedTransactions(beginDate, endDate, item);
                  }}
                  className={`group min-h-[26px] min-w-[26px] cursor-pointer rounded-md border bg-white ${
                    currentPage === item
                      ? "border-primary"
                      : "border-white hover:bg-primary "
                  }`}
                >
                  <p
                    className={`mt-[3px] text-xs  ${
                      currentPage === item
                        ? "font-bold text-primary"
                        : "font-regular text-black group-hover:text-white"
                    }`}
                  >
                    {item}
                  </p>
                </div>
              ))}
            </div>
          </>
        ) : (
          <ErrorCard title="Заасан хугацаанд шилжүүлэг хийгдээгүй байна." />
        )}
      </div>
      <ErrorToast />
    </div>
  );
};

export default TransactionCheck;
