import Axios from "axios";
import React, { useEffect, useState } from "react";
import LargeTitle from "@/components/main/LargeTitle";
import { toast } from "react-toastify";
import ErrorToast from "@/components/main/ErrorToast";
import IconMBank from "/icons/IconMBank";
import DeleteModal from "@/components/modals/DeleteModal";
import EditModal from "@/components/modals/EditModal";
// import { useHistory } from "react-router";
import ErrorCard from "@/components/main/ErrorCard";

const TransactionTemplate = () => {
  // let history = useHistory();
  const [templateList, setTemplateList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [selectedItem, setSelectedItem] = useState({});
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editItem, setEditItem] = useState({});
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [deleteItem, setDeleteItem] = useState({});
  const [banksList, setBanksList] = useState([]);

  useEffect(() => {
    getTransactionTemplates();
  }, []);

  const getTransactionTemplates = async () => {
    setLoading(true);
    await Axios.post("/getTransactionTemplates").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setTemplateList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setLoading(false);
      }
    });
    getBanksList();
  };

  const getBanksList = async () => {
    await Axios.post("/getBanksList").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setBanksList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const deleteHandler = async () => {
    setDeleteModalOpen(false);
    setLoading(true);
    await Axios.post("/deleteTransactionTemplate", {
      id: deleteItem.id,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        getTransactionTemplates();
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const editHandler = async (data) => {
    setLoading(true);
    setEditModalOpen(false);
    await Axios.post("/updateTransactionTemplate", {
      id: editItem.id,
      template_name: data,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        getTransactionTemplates();
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const doTransaction = () => {
    history.push({
      pathname: "/transaction-from-template",
      state: { template: selectedItem, banks: banksList },
    });
  };

  return !loading ? (
    <IconMBank />
  ) : (
    <div class="mx-auto mb-7 w-89 rounded-lg bg-white py-7 pr-7 md:w-113">
      <LargeTitle title="Загвар сонгох" />
      <div class="pl-7">
        {templateList.length > 0 ? (
          <>
            <div class="flex flex-col gap-2">
              {templateList.map((item, index) => (
                <button
                  onClick={() => {
                    setSelectedItem(item);
                    setSelectedIndex(index);
                  }}
                  class={`group h-16 px-4 hover:bg-primary ${
                    selectedIndex === index ? "bg-primary" : "bg-white"
                  } flex items-center justify-between rounded-lg border border-border-100`}
                >
                  <div class="flex items-center">
                    <div
                      class={`h-8 w-8 rounded-full group-hover:bg-white ${
                        selectedIndex === index ? "bg-white" : "bg-primary/20"
                      } mr-2 flex items-center justify-center`}
                    >
                      <p class={`w-8 font-bold text-xs text-primary`}>
                        {item.template_name.substring(0, 1).toUpperCase()}
                      </p>
                    </div>
                    <div class="flex flex-col text-left">
                      <p
                        class={`${
                          selectedIndex === index ? "text-white" : "text-black"
                        } break-all font-regular text-base group-hover:text-white`}
                      >
                        {item.template_name}
                      </p>
                      <p
                        class={`${
                          selectedIndex === index ? "text-white" : "text-gray"
                        } mt-[-1px] font-regular text-xs group-hover:text-white`}
                      >
                        {item.acct_num}
                      </p>
                    </div>
                  </div>
                  <div class="flex gap-2">
                    <button
                      onClick={() => {
                        setEditItem(item);
                        setEditModalOpen(true);
                      }}
                      class={`flex h-7 w-7 items-center justify-center rounded-lg group-hover:bg-white ${
                        selectedIndex === index ? "bg-white" : "bg-border-100"
                      }`}
                    >
                      <img
                        src={
                          selectedIndex === index
                            ? "/icons/ic_edit_green.png"
                            : "/icons/ic_edit_black.png"
                        }
                        alt=""
                        class="h-[18px] w-[14px]"
                      />
                    </button>
                    <button
                      onClick={() => {
                        setDeleteItem(item);
                        setDeleteModalOpen(true);
                      }}
                      class={`flex h-7 w-7 items-center justify-center rounded-lg group-hover:bg-white ${
                        selectedIndex === index ? "bg-white" : "bg-border-100"
                      }`}
                    >
                      <img
                        src={
                          selectedIndex === index
                            ? "/icons/ic_delete_green.png"
                            : "/icons/ic_delete_black.png"
                        }
                        alt=""
                        class="h-[18px] w-[14px] text-pink"
                      />
                    </button>
                  </div>
                </button>
              ))}
            </div>
            <div class="mb-16" />
            <button
              onClick={() => {
                doTransaction();
              }}
              disabled={selectedIndex === -1}
              class="h-12 w-full rounded-lg bg-black disabled:bg-border-100"
            >
              <p class="mt-[-1px] font-bold text-base text-white">
                Шилжүүлэг хийх
              </p>
            </button>
          </>
        ) : (
          <ErrorCard title="Танд шилжүүлгийн загвар байхгүй байна." />
        )}
        <DeleteModal
          title="Загвар устгах"
          description="Та шилжүүлгийн загвар устгах гэж байна, үргэлжлүүлэх үү?"
          isOpen={deleteModalOpen}
          onClick={() => {
            setDeleteModalOpen(false);
          }}
          onDelete={() => {
            deleteHandler();
          }}
        />
        <EditModal
          placeholder="Загварын нэр"
          title="Загварын нэр солих"
          description="Start building your credit score."
          onEdit={(data) => {
            editHandler(data);
          }}
          isOpen={editModalOpen}
          onClose={() => {
            setEditModalOpen(false);
          }}
        />
        <ErrorToast />
      </div>
    </div>
  );
};

export default TransactionTemplate;
