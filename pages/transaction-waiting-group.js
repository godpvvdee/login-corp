import Axios from "axios";
import React, { useState, useEffect } from "react";
import ErrorCard from "@/components/main/ErrorCard";
import LargeTitle from "@/components/main/LargeTitle";
import IconMBank from "/icons/IconMBank";
import { toast } from "react-toastify";
import ErrorToast from "@/components/main/ErrorToast";
import TransactionBatchWaitingTable from "@/components/transaction/TransactionBatchWaitingTable";

const TransactionWaitingGroup = () => {
  const [loading, setLoading] = useState(true);
  const [isReview, setIsReview] = useState(true);
  const [newBatchTransactions, setNewBatchTransactions] = useState([]);
  const [reviewedBatchTransactions, setReviewedBatchTransactions] = useState(
    []
  );
  const [showBatchTransactions, setShowBatchTransactions] = useState([]);

  useEffect(() => {
    getBatchTransactionRequests();
  }, []);

  const getBatchTransactionRequests = async () => {
    setLoading(true);
    await Axios.post("/getBatchTransactionRequests").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setNewBatchTransactions(
          res.data.data.filter((item) => item.status === "NEW")
        );
        setReviewedBatchTransactions(
          res.data.data.filter((item) => item.status === "REVIEWED")
        );
        setShowBatchTransactions(
          res.data.data.filter((item) => item.status === "NEW")
        );
      } else {
        toast.error(`😤 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  return !loading ? (
    <IconMBank />
  ) : (
    <div class="px-7 pb-7">
      <div class="w-full overflow-hidden rounded-lg bg-white pt-7">
        <LargeTitle title="Хүлээгдэж буй багц шилжүүлэг" />
        <div class="pl-4 md:pl-7">
          <div class="mb-7 flex items-center justify-between">
            <div class="rounded-lg border border-border-100 bg-border-100 md:mr-7">
              <button
                onClick={() => {
                  setIsReview(true);
                  setShowBatchTransactions(newBatchTransactions);
                }}
                class={`h-10 w-36 md:w-32 ${
                  isReview ? "bg-white" : "bg-border-100"
                } items-center rounded-lg`}
              >
                <p
                  class={`font-bold text-xs ${
                    isReview ? "text-primary" : "text-black"
                  }`}
                >
                  Хянах
                </p>
              </button>
              <button
                onClick={() => {
                  setIsReview(false);
                  setShowBatchTransactions(reviewedBatchTransactions);
                }}
                class={`h-10 w-36 rounded-lg md:w-32 ${
                  isReview ? "bg-border-100" : "bg-white"
                }  items-center`}
              >
                <p
                  class={`font-bold text-xs ${
                    isReview ? "text-black" : "text-primary"
                  }`}
                >
                  Батлах
                </p>
              </button>
            </div>
            {/* <div class="flex items-center">
              <button
                onClick={() => {
                  setIsAll(!isAll);
                }}
                class="flex mr-7"
              >
                {isAll ? (
                  <img
                    src="/icons/ic_check_circle.png"
                    class="w-5 h-5 mr-2"
                    alt=""
                  />
                ) : (
                  <div class="rounded-full w-5 h-5 bg-border-200 mr-2" />
                )}
                <p class="font-semibold text-base text-black mt-[-1px]">
                  Бүгдийг сонгох
                </p>
              </button>
              <button class="h-10 bg-primary items-center flex px-4 rounded-lg">
                <p class="font-bold text-xs text-white mr-5">Шилжүүлэг хянах</p>
                <div class="w-6 h-6 bg-white bg-opacity-25 rounded-full">
                  <p class="text-white text-xs font-bold mt-[2.5px]">0</p>
                </div>
              </button>
            </div> */}
          </div>
        </div>
        {showBatchTransactions.length > 0 ? (
          <TransactionBatchWaitingTable
            data={showBatchTransactions}
            isReview={isReview}
          />
        ) : (
          <div class="pl-7 pb-7">
            <ErrorCard title="Хүлээгдэж буй багц шилжүүлэг байхгүй байна." />
          </div>
        )}
      </div>
      <ErrorToast />
    </div>
  );
};

export default TransactionWaitingGroup;
