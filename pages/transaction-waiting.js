import Axios from "axios";
import React, { useState, useEffect } from "react";
import Link from "next/link";
import ErrorCard from "@/components/main/ErrorCard";
import LargeTitle from "@/components/main/LargeTitle";
import UserTypeGetter from "@/components/UserTypeGetter";
import IconMBank from "/icons/IconMBank";
import ReactTooltip from "react-tooltip";
// import { useHistory } from "react-router";
import { toast } from "react-toastify";
import ErrorToast from "@/components/main/ErrorToast";
import TransactionWaitingTable from "@/components/transaction/TransactionWaitingTable";

const TransactionWaiting = () => {
  // const history = useHistory();
  const [isReview, setIsReview] = useState(true);
  const [isAll, setIsAll] = useState(false);
  const [loading, setLoading] = useState(true);
  const [newTransactions, setNewTransactions] = useState([]);
  const [reviewedTransactions, setReviewedTransactions] = useState([]);
  const [showTransactions, setShowTransactions] = useState([]);
  const [selectedTransactions, setSelectedTransactions] = useState(0);
  const [banksList, setBanksList] = useState([]);
  const [transactionFees, setTransactionFees] = useState({});

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const setAllTransactions = () => {
    let temp = [...showTransactions];

    temp.forEach((item) =>
      isReview
        ? UserTypeGetter(item.permission).review
          ? (item.isSelected = !isAll)
          : (item.isSelected = false)
        : UserTypeGetter(item.permission).confirm
        ? (item.isSelected = !isAll)
        : (item.isSelected = false)
    );
    setShowTransactions(temp);
    setSelectedTransactions(
      !isAll
        ? isReview
          ? temp.filter((item) => UserTypeGetter(item.permission).review).length
          : temp.filter((item) => UserTypeGetter(item.permission).confirm)
              .length
        : 0
    );
  };

  const transactionSelectHandler = (index) => {
    let temp = [...showTransactions];
    temp[index].isSelected = !temp[index].isSelected;
    setShowTransactions(temp);
    setSelectedTransactions(
      showTransactions.filter((item) => item.isSelected === true).length
    );
  };

  const transactionSelectReset = (data) => {
    let temp = [...data];
    temp.forEach((item) => (item.isSelected = false));
    setShowTransactions(temp);
  };

  const getWrittenList = async () => {
    setLoading(true);
    await Axios.post("/getWrittenList").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setNewTransactions(
          res.data.data.filter((item) => item.status === "NEW")
        );
        setShowTransactions(
          res.data.data.filter((item) => item.status === "NEW")
        );
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setLoading(false);
      }
    });
    getReviewedList();
  };

  const getReviewedList = async () => {
    setLoading(true);
    await Axios.post("/getReviewedList").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setReviewedTransactions(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setLoading(false);
      }
    });
    getBanksList();
  };

  const getBanksList = async () => {
    await Axios.post("/getBanksList").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setBanksList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    getTransactionFees();
  };

  const getTransactionFees = async () => {
    setLoading(true);
    await Axios.post("/getTransactionFees").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setTransactionFees(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const getTypeName = (data) => {
    switch (data) {
      case "INTRABANK":
        return "Банк доторх";
      case "OWN_ACCOUNT":
        return "Өөрийн данс руу";
      case "LOAN_PAYMENT":
        return "Зээлийн төлөлт";
      case "INTERBANK":
        return "Банк хооронд";
      default:
        return data;
    }
  };

  useEffect(() => {
    getWrittenList();
  }, []);

  return !loading ? (
    <IconMBank />
  ) : (
    <div class="px-7 pb-7">
      <div class="w-full overflow-hidden rounded-lg bg-white pt-7">
        <LargeTitle title="Хүлээгдэж буй шилжүүлэг" />
        <div class="px-7">
          <div class="mb-7 items-center justify-between md:flex">
            <div class="flex items-center">
              <div class="mb-4 mr-7 flex rounded-lg border border-border-100 bg-border-100 md:mb-0">
                <button
                  onClick={() => {
                    setIsReview(true);
                    setIsAll(false);
                    setSelectedTransactions(0);
                    transactionSelectReset(newTransactions);
                  }}
                  class={`h-10 w-36 md:w-32 ${
                    isReview ? "bg-white" : "bg-border-100"
                  } items-center rounded-lg`}
                >
                  <p
                    class={`font-bold text-xs ${
                      isReview ? "text-primary" : "text-black"
                    }`}
                  >
                    Хянах
                  </p>
                </button>
                <button
                  onClick={() => {
                    setIsReview(false);
                    setIsAll(false);
                    setSelectedTransactions(0);
                    transactionSelectReset(reviewedTransactions);
                  }}
                  class={`h-10 w-32 rounded-lg ${
                    isReview ? "bg-border-100" : "bg-white"
                  }  items-center`}
                >
                  <p
                    class={`font-bold text-xs ${
                      isReview ? "text-black" : "text-primary"
                    }`}
                  >
                    Батлах
                  </p>
                </button>
              </div>
            </div>
            {
              <div class="flex items-center ">
                <button
                  disabled={selectedTransactions === 0}
                  onClick={() => {
                    history.push({
                      pathname: "/transaction-review-group",
                      state: {
                        banksList: banksList,
                        transactionFees: transactionFees,
                        isReview: isReview,
                        transactionList: showTransactions.filter(
                          (item) => item.isSelected
                        ),
                      },
                    });
                  }}
                  class="group flex h-10 w-75 items-center justify-center rounded-lg bg-primary px-4 disabled:bg-border-100 md:w-auto"
                >
                  <p class="font-bold text-xs text-white group-disabled:text-gray">
                    {isReview ? "Шилжүүлэг хянах" : "Шилжүүлэг батлах"}
                  </p>
                  <div class="ml-5 h-6 w-6 rounded-full bg-white bg-opacity-25 group-disabled:hidden">
                    <p class="mt-[2.5px] font-bold text-xs text-white">
                      {selectedTransactions}
                    </p>
                  </div>
                </button>
              </div>
            }
          </div>
        </div>
        {showTransactions.length > 0 ? (
          <TransactionWaitingTable
            onSelectAll={() => {
              setIsAll(!isAll);
              setAllTransactions();
            }}
            onTransactionSelect={(index) => {
              transactionSelectHandler(index);
            }}
            onTransactionClick={(item) => {
              history.push({
                pathname: isReview
                  ? "/transaction-review"
                  : "/transaction-confirm",
                state: {
                  transactionFees: transactionFees,
                  transactionInfo: item,
                  selectedBank:
                    item.type === "INTERBANK"
                      ? banksList.filter(
                          (bank) => bank.id === item.to_acct_bank_id
                        )[0]
                      : {},
                },
              });
            }}
            isAll={isAll}
            isReview={isReview}
            data={showTransactions}
            banksList={banksList}
          />
        ) : (
          <div class="px-7 pb-7">
            <ErrorCard title="Хүлээгдэж буй шилжүүлэг байхгүй байна." />
          </div>
        )}
        {/* <div class="flex flex-col ">
          {showTransactions.length > 0 ? (
            showTransactions.map((item, index) => (
              <div class="flex cursor-pointer ml-7 p-2 items-center rounded-lg bg-white hover:bg-border-200">
                {!isReview &&
                  (UserTypeGetter(item.permission).confirm ? (
                    <button
                      onClick={() => {}}
                      class={`w-5 h-5 bg-contain bg-no-repeat pr-7 mr-5 ${
                        item.isSelected
                          ? "bg-[url('../public/icons/ic_checkbox.png')]"
                          : "bg-[url('../public/icons/ic_checkbox_false.png')]"
                      }`}
                    />
                  ) : (
                    <button
                      data-tip="Танд уг шилжүүлгийг батлах эрх байхгүй байна."
                      class={`w-5 h-5 bg-contain bg-no-repeat pr-7 mr-5 ${"bg-[url('../public/icons/ic_checkbox_false.png')]"}`}
                    />
                  ))}
                {isReview &&
                  (UserTypeGetter(item.permission).review ? (
                    <button
                      onClick={() => {
                        transactionSelectHandler(index);
                      }}
                      class={`w-5 h-5 bg-contain bg-no-repeat pr-7 mr-5 ${
                        item.isSelected
                          ? "bg-[url('../public/icons/ic_checkbox.png')]"
                          : "bg-[url('../public/icons/ic_checkbox_false.png')]"
                      }`}
                    />
                  ) : (
                    <button
                      data-tip="Танд уг шилжүүлгийг хянах эрх байхгүй байна."
                      class={`w-5 h-5 bg-contain bg-no-repeat pr-7 mr-5 ${"bg-[url('../public/icons/ic_checkbox_false.png')]"}`}
                    />
                  ))}

                <Link
                  className="flex w-full"
                  to={{
                    pathname: isReview
                      ? "/transaction-review"
                      : "/transaction-confirm",
                    state: {
                      transactionFees: transactionFees,
                      transactionInfo: item,
                      selectedBank:
                        item.type === "INTERBANK"
                          ? banksList.filter(
                              (bank) => bank.id === item.to_acct_bank_id
                            )[0]
                          : {},
                    },
                  }}
                >
                  <div class="flex flex-col w-2/12">
                    <p class="text-base font-regular text-black">Зарлага</p>
                    <p class="text-xs font-regular text-gray">
                      {item.wrote_date.replaceAll("T", " ").replaceAll("Z", "")}
                    </p>
                  </div>
                  <div class="flex flex-col w-3/12">
                    <p class="text-base font-regular text-black">
                      {item.wrote_last_name + " " + item.wrote_first_name}
                    </p>
                    <p class="text-xs font-regular text-gray">
                      {isReview ? "Шивсэн ажилтан" : "Хянасан ажилтан"}
                    </p>
                  </div>
                  <div class="flex flex-col w-2/12">
                    <p class="text-base font-regular text-black">
                      {getTypeName(item.type)}
                    </p>
                    <p class="text-xs font-regular text-gray">
                      Шилжүүлгийн төрөл
                    </p>
                  </div>
                  <div class="flex flex-col w-2/12">
                    <p class="text-base font-regular text-black">
                      {item.to_account_num}
                    </p>
                    <p class="text-xs font-regular text-gray">
                      Хүлээн авагчийн данс
                    </p>
                  </div>
                  <div class="flex flex-col w-3/12">
                    <p class="text-base font-regular text-black">
                      {item.to_account_name}
                    </p>
                    <p class="text-xs font-regular text-gray">
                      Хүлээн авагчийн нэр
                    </p>
                  </div>
                  <div class="flex flex-col w-3/12">
                    <p class="text-base font-regular text-black">
                      {item.remarks}
                    </p>
                    <p class="text-xs font-regular text-gray">
                      Шилжүүлгийн утга
                    </p>
                  </div>
                  <div class="flex flex-col text-right w-3/12">
                    <p class="text-base font-semibold text-black">
                      {formatter.format(item.to_amount).replaceAll("$", "") +
                        " " +
                        item.from_currency}
                    </p>
                    <p class="text-xs font-regular text-gray">Мөнгөн дүн</p>
                  </div>
                </Link>
                <ReactTooltip
                  place="top"
                  type="light"
                  backgroundColor="white"
                  className="tooltip"
                />
              </div>
            ))
          ) : (
            <div class="pl-7">
              <ErrorCard title="Хүлээгдэж буй шилжүүлэг байхгүй байна." />
            </div>
          )}
        </div> */}
      </div>
      <ErrorToast />
    </div>
  );
};

export default TransactionWaiting;
