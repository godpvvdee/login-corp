import axios from "axios";
import { sessionOptions } from "config/session";
import { withIronSessionApiRoute } from "iron-session/next";
var https = require("https");
var fs = require("fs");
//// Iron session
export default withIronSessionApiRoute(userRoute, sessionOptions);
async function userRoute(req, res) {
  const httpsAgent = new https.Agent({
    cert: fs.readFileSync("cert/test-cert.pem"),
    key: fs.readFileSync("cert/test-key.pem"),
    passphrase: "testP@ss123",
    rejectUnauthorized: false,
  });
  // console.log("helsensahde ", req.session.token);
  try {
    const result = await axios({
      method: "get",
      url: `https://10.41.200.92:8082/m-customer-corporate-service/customer?fetch=all`,
      headers: {
        Authorization: `Bearer ${req.session.token}`,
      },
      httpsAgent,
    });
    console.log("sss ===", req?.session?.corporate);
    if (result.data.code.substring(0, 1) === "2") {
      res.status(200).json({
        success: true,
        data: { ...result.data.data },
        auth_time: req?.session?.authTime,
        exp_time: req?.session?.expTime,
        corporate: req?.session?.corporate,
        isAuth: result.data.data && req?.session?.corporate ? true : false,
      });
    } else {
      res.status(200).json({ success: false, data: {} });
    }
    // res.json();
  } catch (err) {
    console.log(err.message);
    res.status(200).json({ success: false, data: {} });
  }

  res.end();
}
