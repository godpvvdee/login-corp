import { apiServer } from "@/config/api";
import logger from "@/config/logger";
import { serviceList } from "@/config/serviceList";
import { withSessionRoute } from "@/config/withSession";

const handler = withSessionRoute(async (req, res) => {
  let { token } = req.session;

  if (token) {
    apiServer.defaults.headers.Authorization = `Bearer ${token}`;
    apiServer.defaults.timeout = 30000;

    const { name, value } = req.query || {};
    const data = req.body ? JSON.parse(req.body) : null;
    switch (req.method) {
      case "GET":
        try {
          // console.log('MICRO SERVICE API ', `${serviceList[name]}${value}`)
          const result = await apiServer.get(
            `${serviceList[name]}${value || ""}`
          );
          if (result.code.substring(0, 1) === "2") {
            logger.info(`\nGET ${serviceList[name]}${value}`, {
              message: JSON.stringify(result),
            });
            res.status(200).json({
              success: true,
              data: result.data,
              message: result.message,
            });
          } else {
            logger.warn(`\nGET ${serviceList[name]}${value}`, {
              message: JSON.stringify(result),
            });
            res.status(200).json({ success: false, message: result.message });
          }
        } catch (e) {
          logger.error(`\n GET ${serviceList[name]}${value}`, { message: e });
          res.status(200).json({ success: false, message: e });
        }
        break;
      case "POST":
        try {
          const result = await apiServer.post(
            `${serviceList[name]}${value || ""}`,
            data
          );

          console.log();
          console.log(
            "MICRO SERVICE API ",
            `${serviceList[name]}`,
            "\nBODY  ",
            data,
            "\nRES ",
            result
          );

          if (result.code.substring(0, 1) === "2") {
            logger.info(`\nPOST ${serviceList[name]}${value || ""}`, {
              message: JSON.stringify(result),
            });
            if (name === "checkPincodeLogin") {
              req.session.corporate = data.corporate;
              console.log("req.session.corprate = ", req.session.corporate);
              await req.session.save();
            }
            res.status(200).json({
              success: true,
              message: result.message,
              data: result.data,
            });
          } else {
            logger.warn(`\nPOST ${serviceList[name]} `, {
              message: JSON.stringify(result),
            });
            res.status(200).json({ success: false, message: result.message });
          }
        } catch (e) {
          logger.error(`\nPOST ${serviceList[name]}`, { message: e });
          res.status(200).json({ success: false, message: e });
        }
        break;
      case "DELETE":
        try {
          // console.log('DELETE REQ ', req)
          const result = await apiServer.delete(`${serviceList[name]}`, {
            data: data,
          });

          console.log("RES Delete ", serviceList[name], name, data);
          if (result.code.substring(0, 1) === "2") {
            logger.info(`\nDELETE ${serviceList[name]}`, {
              message: JSON.stringify(result),
            });
            res.status(200).json({
              success: true,
              message: result.message,
              data: result.data,
            });
          } else {
            logger.warn(`\nDELETE ${serviceList[name]} `, {
              message: JSON.stringify(result),
            });
            res.status(200).json({ success: false, message: result.message });
          }
        } catch (e) {
          logger.error(`\nDELETE ${serviceList[name]}`, { message: e });
          res.status(200).json({ success: false, message: e });
        }
        break;
      case "PUT":
        try {
          const result = await apiServer.put(`${serviceList[name]}`, data);

          console.log("RES PUT ", result, data, serviceList[name]);
          if (result.code.substring(0, 1) === "2") {
            logger.info(`\nPUT ${serviceList[name]}`, {
              message: JSON.stringify(result),
            });
            res.status(200).json({
              success: true,
              message: result.message,
              data: result.data,
            });
          } else {
            logger.warn(`\nPUT ${serviceList[name]} `, {
              message: JSON.stringify(result),
            });
            res.status(200).json({ success: false, message: result.message });
          }
        } catch (e) {
          logger.error(`\nPUT ${serviceList[name]}`, { message: e });
          res.status(200).json({ success: false, message: e });
        }
        break;
      default:
        res.status(200).json({
          success: false,
          message: `Боломжгүй үйлдэл ${req.method}`,
        });
    }
  } else {
    console.log("REDIRECT / ");
    res.status(200).json({
      success: false,
      message: "Таны холболтын хугацаа дууссан байна",
    });
  }
});
export default handler;
