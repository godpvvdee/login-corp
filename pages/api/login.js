import axios from "axios";
import { withIronSessionApiRoute } from "iron-session/next";
import moment from "moment";
import logger from "../../config/logger";
import Router from "next/router";
var CryptoJS = require("crypto-js");

import { sessionOptions } from "../../config/session";
var https = require("https");
var fs = require("fs");
//// Iron session

export default withIronSessionApiRoute(async (req, res) => {
  const httpsAgent = new https.Agent({
    cert: fs.readFileSync("cert/test-cert.pem"),
    key: fs.readFileSync("cert/test-key.pem"),
    passphrase: "testP@ss123",
    rejectUnauthorized: false,
  });
  const { username, password, deviceId } = req.body;

  console.log("ENV >> ", {
    message:
      process.env.BASIC_TOKEN +
      " OAUTH " +
      process.env.OAUTH_URL +
      " GATE " +
      process.env.GATEWAY_URL,
  });
  logger.info("ENV >> ", {
    message:
      process.env.BASIC_TOKEN +
      " OAUTH " +
      process.env.OAUTH_URL +
      " GATE " +
      process.env.GATEWAY_URL,
  });

  try {
    const result = await axios({
      method: "POST",
      url: `https://10.41.200.92:8082/m-customer-corporate-service/user/token`,
      headers: {
        Authorization:
          "Basic MTc4NjdlNTctZDJkOC00ZGI1LTg2N2UtNTdkMmQ4NGRiNTczOlNLVVBGU1NMUndKdlgtcGJYNjlfako3c0VNT3FLNkZxWng5ZVh5aTJWV0U=",
      },
      data: {
        device_id: deviceId,
        grant_type: "password",
        username: CryptoJS.AES.decrypt(
          username,
          "RF32KsMSvh5wFv5j2As9"
        ).toString(CryptoJS.enc.Utf8),
        password: CryptoJS.AES.decrypt(
          password,
          "RF32KsMSvh5wFv5j2As9"
        ).toString(CryptoJS.enc.Utf8),
      },
      httpsAgent,
    });

    req.session = {
      token: result.data.data.id_token,
      authTime: moment().format(),
      expTime: parseInt(moment().valueOf()) + 600000 * 15,
      corprate: null, // Millisecond
    };

    await req.session.save();

    if (result.data.code.substring(0, 1) === "2") {
      res.status(200).json({ success: true, message: result.data.message });
    } else {
      res.status(200).json({
        success: false,
        message: "Нэвтрэх нэр нууц үг чинь буруу байншдээ бро",
      });
    }
  } catch (err) {
    res.status(200).json({ success: false, message: "Алдаа гарлаа" });
  }

  res.end();
}, sessionOptions);
