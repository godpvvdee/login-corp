import { withSessionRoute } from "@/config/withSession";

const handler = withSessionRoute((req, res) => {
  try {
    req.session.destroy();
    res
      .status(200)
      .json({ success: true, message: "Системээс гарлаа", isAuth: false });
  } catch (e) {
    res.status(200).json({ success: false, message: e, isAuth: false });
  }
});
export default handler;
