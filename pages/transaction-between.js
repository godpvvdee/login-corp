import Axios from "axios";
import React, { useState, useEffect } from "react";
import LargeTitle from "@/components/main/LargeTitle";
import MInput from "@/components/main/MInput";
import TransactionAccountPicker from "@/components/transaction/TransactionAccountPicker";
import { toast } from "react-toastify";
import ErrorToast from "@/components/main/ErrorToast";
import IconMBank from "icons/IconMBank";
import UserTypeGetter from "@/components/UserTypeGetter";
import { sha256 } from "js-sha256";
import TransactionConfirmSuccessModal from "@/components/modals/TransactionConfirmSuccessModal";
import TransactionReviewSuccessModal from "@/components/modals/TransactionReviewSuccessModal";
import TransactionWriteSuccessModal from "@/components/modals/TransactionWriteSuccessModal";
import TransactionPincodeModal from "@/components/modals/TransactionPincodeModal";
import ErrorCard from "@/components/main/ErrorCard";
import MButton from "@/components/buttons/MButton";
import SelectedAccountCard from "@/components/accounts/SelectedAccountCard";
import DetailCard from "@/components/main/DetailCard";
import { apiClient } from "@/config/api";
// import { useHistory } from "react-router";
import NewOTPModal from "@/components/modals/NewOTPModal";
import { useAuth } from "contexts/auth";

const TransactionBetween = () => {
  const { user } = useAuth();

  // var history = useHistory();
  const [sendAmount, setSendAmount] = useState("");
  const [toAmount, setToAmount] = useState("");
  const [loading, setLoading] = useState(false);
  const [description, setDescription] = useState("");
  const [fromAccount, setFromAccount] = useState({});
  const [toAccount, setToAccount] = useState({});
  const [caList, setCaList] = useState([]);
  const [toList, setToList] = useState([]);
  const [fromList, setFromList] = useState([]);
  const [reviewModal, setReviewModal] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);
  const [writeModal, setWriteModal] = useState(false);
  const [pageState, setPageState] = useState(1);
  const [pinModal, setPinModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [accountsEmpty, setAccountsEmpty] = useState(false);
  const [currencyRate, setCurrencyRate] = useState(0.0);
  const [otpModal, setOtpModal] = useState(false);
  const [userInfo, setUserInfo] = useState({});
  const [transactionFees, setTransactionFees] = useState({});

  const fromAccountPickerHandler = (data) => {
    setFromAccount(data);
    setToList(
      caList.filter(
        (item) => data.currentAccount.acct_num !== item.currentAccount.acct_num
      )
    );
    if (toAccount.currentAccount.acct_num === data.currentAccount.acct_num) {
      setToAccount(
        caList.filter(
          (item) =>
            data.currentAccount.acct_num !== item.currentAccount.acct_num
        )[0]
      );
    }
    if (data.currentAccount.acct_ccy !== toAccount.currentAccount.acct_ccy) {
      setLoading(true);
      getTransactionRate(
        data.currentAccount.acct_ccy,
        toAccount.currentAccount.acct_ccy,
        data.currentAccount.acct_ccy === "MNT" ? "NCSR" : "NCBR"
      );
    }
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const pinHandler = (data) => {
    setPincode(data);
  };

  const toAccountPickerHandler = (data) => {
    setToAccount(data);
    if (data.currentAccount.acct_ccy !== fromAccount.currentAccount.acct_ccy) {
      setLoading(true);
      getTransactionRate(
        fromAccount.currentAccount.acct_ccy,
        data.currentAccount.acct_ccy,
        fromAccount.currentAccount.acct_ccy === "MNT" ? "NCSR" : "NCBR"
      );
    }
  };

  const writeTransaction = async (data) => {
    setLoading(true);
    await Axios.post("/writeTransaction", {
      type: "OWN_ACCOUNT",
      fromAccount: fromAccount.currentAccount.acct_num,
      fromAmount: parseFloat(sendAmount),
      fromCurrency: fromAccount.currentAccount.acct_ccy,
      toAccount: toAccount.currentAccount.acct_num,
      toAccountName: toAccount.currentAccount.acct_name,
      toAmount: parseFloat(sendAmount),
      toCurrency: fromAccount.currentAccount.acct_ccy,
      description: description,
      pincode: sha256(pincode),
      otp: data.otp,
      otpReceiver: data.receiver,
      otpType: data.type,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        if (res.data.message === "SUCCESSFUL") {
          setWriteModal(true);
        } else if (res.data.message === "REVIEWED") {
          setReviewModal(true);
        } else {
          setConfirmModal(true);
        }
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const clearData = () => {
    history.push("/");
  };

  const getUserDetails = async () => {
    setLoading(true);
    await Axios.post("/getUserDetails").then((res) => {
      if (res.status.toString().substring(0, 1) === "2") {
        setUserInfo(res.data);
        setOtpModal(true);
      } else {
        setUserInfo({});
      }
    });
    setLoading(false);
  };

  const doTransaction = () => {
    if (parseFloat(sendAmount) >= 20000000) {
      getUserDetails();
    } else {
      writeTransaction({
        otp: "",
        type: "",
        receiver: "",
      });
    }
  };

  const getCAList = async () => {
    setLoading(true);
    await apiClient
      .get("/api", {
        params: {
          name: "getCorpCAList",
          value: `corporate_cif=${user.corporate}`,
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          if (res.data.length > 1) {
            let temp = [...res.data];
            temp.map(
              (item, index) =>
                (temp[index].config = UserTypeGetter(item.permission))
            );
            if (temp.filter((item) => item.config.write === true).length > 0) {
              setFromList(temp.filter((item) => item.config.write === true));
              setFromAccount(
                temp.filter((item) => item.config.write === true)[0]
              );
              setToList(
                temp.filter(
                  (item) =>
                    temp.filter((item) => item.config.write === true)[0]
                      .currentAccount.acct_num !== item.currentAccount.acct_num
                )
              );
              setToAccount(
                temp.filter(
                  (item) =>
                    temp.filter((item) => item.config.write === true)[0]
                      .currentAccount.acct_num !== item.currentAccount.acct_num
                )[0]
              );
              setCaList(temp);
              if (
                temp.filter((item) => item.config.write === true)[0]
                  .currentAccount.acct_ccy !==
                temp.filter(
                  (item) =>
                    temp.filter((item) => item.config.write === true)[0]
                      .currentAccount.acct_num !== item.currentAccount.acct_num
                )[0].currentAccount.acct_ccy
              ) {
                getTransactionRate(
                  temp.filter((item) => item.config.write === true)[0]
                    .currentAccount.acct_ccy,
                  temp.filter(
                    (item) =>
                      temp.filter((item) => item.config.write === true)[0]
                        .currentAccount.acct_num !==
                      item.currentAccount.acct_num
                  )[0].currentAccount.acct_ccy,
                  temp.filter((item) => item.config.write === true)[0]
                    .currentAccount.acct_ccy === "MNT"
                    ? "NCSR"
                    : "NCBR"
                );
              } else {
                getTransactionFees();
              }
            } else {
              setAccountsEmpty(true);
            }
          } else {
            setAccountsEmpty(true);
            setLoading(false);
          }
        } else {
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
  };

  const getTransactionRate = async (data1, data2, data3) => {
    await Axios.post("/getTransactionRate", {
      ccy1: data1,
      ccy2: data2,
      rCode: data3,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setCurrencyRate(res.data.data.rate);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    getTransactionFees();
  };

  const getTransactionFees = async () => {
    setLoading(true);
    await Axios.post("/getTransactionFees").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setTransactionFees(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const sendAmountHandler = (data) => {
    if (data === "") {
      setToAmount("");
      setSendAmount("");
    } else {
      let temp = parseFloat(data) / currencyRate;
      setToAmount(temp);
      setSendAmount(data);
    }
  };

  const toAmountHandler = (data) => {
    if (data === "") {
      setToAmount("");
      setSendAmount("");
    } else {
      let temp = (parseFloat(data) * currencyRate).toFixed(2);
      setToAmount(data);
      setSendAmount(temp);
    }
  };

  useEffect(() => {
    setPageState(1);
    getCAList();
  }, []);

  return !loading ? (
    <IconMBank />
  ) : (
    <div class="mx-auto w-89 rounded-lg bg-white py-7 pr-4 md:w-113 md:pr-7">
      <LargeTitle title="Өөрийн данс хооронд" />
      <div class="pl-4 md:pl-7">
        {accountsEmpty ? (
          <ErrorCard title="Танд шилжүүлэг илгээх боломжтой данс байхгүй байна." />
        ) : pageState === 1 ? (
          <div>
            <TransactionAccountPicker
              title="Илгээх данс сонгох"
              accountList={fromList}
              selectedAccount={fromAccount}
              onChange={fromAccountPickerHandler}
            />
            <div class="my-4 h-[1px] w-full bg-border-100" />
            <TransactionAccountPicker
              title="Хүлээн авах данс сонгох"
              accountList={toList}
              selectedAccount={toAccount}
              onChange={toAccountPickerHandler}
            />
            <div class="mt-2 flex">
              <div class="w-full">
                <MInput
                  maxLength={11}
                  placeholder="Мөнгөн дүн"
                  numberFormat
                  value={sendAmount}
                  onChange={(e) => {
                    sendAmountHandler(e);
                  }}
                />
              </div>
              <div class="ml-2 flex h-12 w-30 items-center justify-between rounded-lg bg-border-100 px-4">
                <div class="flex flex-col text-left">
                  <p class="font-regular text-xs text-gray">Валют</p>
                  <p class="mt-[-1px] font-regular text-base text-black">
                    {fromAccount.currentAccount.acct_ccy}
                  </p>
                </div>
              </div>
            </div>
            {fromAccount.currentAccount.acct_ccy !==
            toAccount.currentAccount.acct_ccy ? (
              <>
                <div class="flex">
                  <div class="w-full">
                    <MInput
                      maxLength={11}
                      placeholder="Мөнгөн дүн"
                      numberFormat
                      value={toAmount}
                      onChange={(e) => {
                        toAmountHandler(e);
                      }}
                    />
                  </div>
                  <div class="ml-2 flex h-12 w-30 items-center justify-between rounded-lg bg-border-100 px-4">
                    <div class="flex flex-col text-left">
                      <p class="font-regular text-xs text-gray">Валют</p>
                      <p class="mt-[-1px] font-regular text-base text-black">
                        {toAccount.currentAccount.acct_ccy}
                      </p>
                    </div>
                  </div>
                </div>
                <p class="mb-2 ml-2 font-semibold text-xs text-gray">
                  1 {toAccount.currentAccount.acct_ccy} = {currencyRate + " "}
                  {fromAccount.currentAccount.acct_ccy}
                </p>
              </>
            ) : null}

            <MInput
              maxLength={60}
              placeholder="Шилжүүлгийн утга"
              id="description"
              type="text"
              onChange={(e) => setDescription(e)}
            />
            <div class="mb-18" />
            {/* {parseFloat(sendAmount) >
            fromAccount.currentAccount.positive_balance_amt ? (
              <ErrorCard title="Илгээх дансны үлдэгдэл хүрэлцэхгүй байна." />
            ) : (
              <MButton
                onClick={() => {
                  setPageState(2);
                }}
                disabled={
                  description === "" ||
                  sendAmount === "" ||
                  parseFloat(sendAmount) <= 0
                }
                title="Шилжүүлэг хийх"
              />
            )} */}
          </div>
        ) : (
          <div class="flex flex-col gap-2">
            <SelectedAccountCard
              title="Илгээх данс"
              acct_num={fromAccount.currentAccount.acct_num}
              acct_ccy={fromAccount.currentAccount.acct_ccy}
              balance_amt={fromAccount.currentAccount.positive_balance_amt}
            />
            <SelectedAccountCard
              title="Хүлээн авах данс"
              acct_num={toAccount.currentAccount.acct_num}
              acct_ccy={toAccount.currentAccount.acct_ccy}
              balance_amt={toAccount.currentAccount.positive_balance_amt}
            />
            <div class="flex gap-2">
              <DetailCard
                title="Мөнгөн дүн"
                description={formatter.format(sendAmount).replaceAll("$", "")}
              />
              <div class="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                <p class="font-regular text-xs text-gray">Валют</p>
                <p class="mt-[-1px] font-regular text-base text-black">
                  {fromAccount.currentAccount.acct_ccy}
                </p>
              </div>
            </div>
            <DetailCard
              title="Шилжүүлгийн шимтгэл"
              description={transactionFees.own_acct + "₮"}
            />
            <div class="flex w-full flex-col justify-center rounded-lg bg-border-100 px-4 py-2">
              <p class="font-regular text-xs text-gray">Шилжүүлгийн утга</p>
              <p class="mt-[-1px] break-all font-regular text-base leading-tight text-black">
                {description}
              </p>
            </div>
            <div class="mb-18" />
            <MButton
              disabled={false}
              onClick={() => {
                setPinModal(true);
              }}
              title="Зөвшөөрөх"
            />
          </div>
        )}
      </div>

      <ErrorToast />
      <TransactionConfirmSuccessModal
        type="BETWEEN"
        isOpen={confirmModal}
        onClick={() => {
          setConfirmModal(false);
          clearData();
        }}
      />
      <TransactionReviewSuccessModal
        type="BETWEEN"
        isOpen={reviewModal}
        onClick={() => {
          setReviewModal(false);
          clearData();
        }}
      />
      <TransactionWriteSuccessModal
        type="BETWEEN"
        isOpen={writeModal}
        onClick={() => {
          setWriteModal(false);
          clearData();
        }}
      />
      <TransactionPincodeModal
        onClose={() => {
          setPinModal(false);
        }}
        isOpen={pinModal}
        onChange={pinHandler}
        onClick={() => {
          setPinModal(false);
          doTransaction();
        }}
      />
      <NewOTPModal
        onSuccess={(data) => {
          setOtpModal(false);
          writeTransaction(data);
        }}
        userInfo={userInfo}
        isOpen={otpModal}
        onClose={() => setOtpModal(false)}
      />
    </div>
  );
};

export default TransactionBetween;
