import React, { useEffect, useState } from "react";
import ChooseCorporateCard from "../components/choose-corporate/ChooseCorporateCard";
import { toast } from "react-toastify";
import PincodeCheckModal from "../components/modals/PincodeCheckModal";
import axios from "axios";
import ErrorToast from "../components/main/ErrorToast";
import IconMBank from "../icons/IconMBank";
import { apiClient } from "@/config/api";
import { sha256 } from "js-sha256";
import Accounts from "./accounts";

const ChooseCorporate = (props) => {
  const [corporateList, setCorporateList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [pinModal, setPinModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [selectedCorp, setSelectedCorp] = useState({});
  const [pinmodalLoading, setPinmodalLoading] = useState(false);

  useEffect(() => {
    getCorporateList();
  }, []);

  const getCorporateList = async () => {
    await apiClient
      .get("/api", {
        params: {
          name: "corporateList",
        },
      })
      .then((res) => {
        if (res.success) {
          if (res.data.length === 1) {
            setCorporateList(res.data);
            setSelectedCorp(res.data[0]);
          } else {
            setCorporateList(res.data);
          }
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        setLoading(false);
      });
  };
  const checkResetPin = async (data) => {
    await apiClient
      .get("/api", {
        params: {
          name: "checkResetPinLogin",
          value: `corporateCif=${data.corporate_cif}`,
        },
      })
      .then((res) => {
        console.log("res.data.code=", res);
        if (res.success) {
          if (res.data === false) {
            setPinModal(true);
          } else {
            history.push({
              pathname: "/",
              state: {
                detail: "changePincode",
                corporate: data,
                corporateList: corporateList,
              },
            });
          }
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        setLoading(false);
      });
  };
  const pinHandler = (data) => {
    setPincode(data);
  };

  const checkPincode = async () => {
    setPinmodalLoading(true);
    await apiClient
      .post(
        "/api",
        {
          pincode: sha256(pincode),
          corporate: selectedCorp.corporate_cif,
        },
        {
          params: {
            name: "checkPincodeLogin",
            value: `corporateCif=${selectedCorp.corporate_cif}`,
          },
        }
      )
      .then((res) => {
        setPinModal(false);
        if (res.success) {
          if (res.data) {
            window.location.assign("/accounts");
            console.log("selectedCorp=", selectedCorp);
          } else {
            toast.error(`🥺 ${res.message}`, {
              position: "bottom-right",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            setLoading(false);
          }
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    setPinmodalLoading(false);
  };

  return loading ? (
    <IconMBank />
  ) : (
    <div className="flex h-screen w-full flex-col items-center justify-center">
      <img
        src="/icons/ic_mbank.png"
        className="absolute top-5 left-11 h-13 w-[130px]"
        alt=""
      />
      {corporateList.length < 1 ? (
        <>
          <p className="font-bold text-px24 text-black">
            Уучлаарай та байгууллагын эрхгүй байна!
          </p>
        </>
      ) : (
        <>
          <p className="font-bold text-px24 text-black">Компани сонгох</p>
          <p className="mb-11 font-regular text-base text-black">
            Start building your credit score.
          </p>
        </>
      )}
      <div className="flex gap-7">
        {corporateList.map((item, index) => (
          <ChooseCorporateCard
            onClick={() => {
              setSelectedCorp(item);
              checkResetPin(item);
            }}
            key={index}
            companyName={item.corporate_name}
            position={item.position}
          />
        ))}
      </div>
      <ErrorToast />
      <PincodeCheckModal
        onClose={() => {
          setPinModal(false);
        }}
        isLoading={pinmodalLoading}
        isOpen={pinModal}
        onChange={pinHandler}
        onClick={() => {
          checkPincode();
        }}
      />
    </div>
  );
};

export default ChooseCorporate;
