import Axios from "axios";
import React, { useState, useEffect } from "react";
import AccountBalanceCard from "../components/accounts/AccountBalanceCard";
import AccountsListCard from "../components/accounts/AccountsListCard";
import CurrencyRateCard from "../components/accounts/CurrencyRateCard";
import StockRateCard from "../components/accounts/StockRateCard";
import LoansListCard from "../components/loans/LoansListCard";
import { toast } from "react-toastify";
import ErrorToast from "../components/main/ErrorToast";
import IconMBank from "../icons/IconMBank";
import { apiClient } from "@/config/api";
import { useAuth } from "contexts/auth";
const Accounts = (props) => {
  const { user } = useAuth();
  const [caList, setCaList] = useState([]);
  const [loanList, setLoanList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [allBalance, setAllBalance] = useState(0);
  const [mseList, setMseList] = useState([]);
  const [currencyRateList, setCurrencyRateList] = useState([]);

  const getCAList = async () => {
    setLoading(true);
    await apiClient
      .get("/api", {
        params: {
          name: "getCorpCAList",
          value: `corporate_cif=${user.corporate}`,
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          setCaList(res.data);
          console.log("reduce", res.data);
          setAllBalance(
            res.data.reduce(
              (n, { currentAccount }) =>
                n + currentAccount?.positive_balance_amt,
              0
            )
          );
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    getMSEPrice();
  };

  const getLoanList = async () => {
    await Axios.post("/getLoanList").then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setLoanList(res.data.data);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  // const getCurrencyRateList = async () => {
  //   await Axios.post("/getCurrencyRateList").then((res) => {
  //     if (res.data.code.substring(0, 1) === "2") {
  //       var temp = [];
  //       var all = res.data.data;
  //       temp.push(res.data.data.filter((item) => item.ccy2 === "USD")[0]);
  //       all
  //         .filter((item) => item.ccy2 !== "USD")
  //         .map((item) => {
  //           temp.push(item);
  //         });

  //       setCurrencyRateList(temp);
  //     } else {
  //       toast.error(`🥺 ${res.data.message}`, {
  //         position: "bottom-right",
  //         autoClose: 3000,
  //         hideProgressBar: false,
  //         closeOnClick: true,
  //         pauseOnHover: true,
  //         draggable: true,
  //         progress: undefined,
  //       });
  //       setLoading(false);
  //     }
  //   });
  //   getLoanList();
  // };
  const getCurrencyRateList = async () => {
    setLoading(true);
    await apiClient
      .get("/api", {
        params: {
          name: "getCurrencyRateList",
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          var temp = [];
          var all = res.data;
          temp.push(res.data.filter((item) => item.ccy2 === "USD")[0]);
          all
            .filter((item) => item.ccy2 !== "USD")
            .map((item) => {
              temp.push(item);
            });

          setCurrencyRateList(temp);
        } else {
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setLoading(false);
        }
      });
  };
  const getMSEPrice = async () => {
    setLoading(true);

    await apiClient
      .get("/api", {
        params: {
          name: "getMSEPrice",
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          setMseList(res.data);
        } else {
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setLoading(false);
        }
      });
  };
  useEffect(() => {
    getCAList();
    getCurrencyRateList();
    getMSEPrice();
  }, []);

  return !loading ? (
    <IconMBank />
  ) : (
    <>
      <div className="flex  flex-col gap-7 p-7 xl:flex-row">
        <div className="flex w-full flex-col gap-7">
          <AccountBalanceCard total={allBalance} />
          <AccountsListCard caList={caList} />
          <LoansListCard loanList={loanList} />
        </div>
        <div className="flex w-full flex-col gap-7">
          <div className="flex flex-col gap-7 md:flex-row xl:flex-col 3xl:flex-row ">
            <div className="w-full md:w-1/2 xl:w-full">
              <CurrencyRateCard currencyRateList={currencyRateList} />
            </div>
            <div className="md:w-1/2 xl:w-full">
              <StockRateCard firstList={mseList} />
            </div>
          </div>
          {/* <AccountLatestTransactionsCard /> */}
          {/* <CurrencyCalculatorCard /> */}
          {/* <WaitingList /> */}
        </div>
      </div>
      <ErrorToast />
    </>
  );
};

export default Accounts;
