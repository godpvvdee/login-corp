import Axios from "axios";
import React, { useState, useEffect } from "react";
import LargeTitle from "@/components/main/LargeTitle";
import IconMBank from "icons/IconMBank";
import TransactionAccountPicker from "@/components/transaction/TransactionAccountPicker";
import { toast } from "react-toastify";
import ErrorToast from "@/components/main/ErrorToast";
import UserTypeGetter from "@/components/UserTypeGetter";
import { sha256 } from "js-sha256";
import TransactionConfirmSuccessModal from "@/components/modals/TransactionConfirmSuccessModal";
import TransactionReviewSuccessModal from "@/components/modals/TransactionReviewSuccessModal";
import TransactionWriteSuccessModal from "@/components/modals/TransactionWriteSuccessModal";
import TransactionPincodeModal from "@/components/modals/TransactionPincodeModal";
import MInput from "@/components/main/MInput";
import TransactionBankPicker from "@/components/transaction/TransactionBankPicker";
import ErrorCard from "@/components/main/ErrorCard";
import DetailCard from "@/components/main/DetailCard";
import MButton from "@/components/buttons/MButton";
import SelectedAccountCard from "@/components/accounts/SelectedAccountCard";
import TransactionWarning from "@/components/modals/TransactionWarning";
// import { useHistory } from "react-router";

import TransactionOTPModal from "@/components/modals/TransactionOTPModal";
import NewOTPModal from "@/components/modals/NewOTPModal";
import { apiClient } from "@/config/api";
import { useAuth } from "@/contexts/auth";

const TransactionIntra = () => {
  const { user } = useAuth();
  const [loading, setLoading] = useState(true);
  const [sendAmount, setSendAmount] = useState("");
  const [description, setDescription] = useState("");
  const [fromAccount, setFromAccount] = useState({});
  const [toAccount, setToAccount] = useState({});
  const [toAccountNumber, setToAccountNumber] = useState("");
  const [caList, setCaList] = useState([]);
  const [reviewModal, setReviewModal] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);
  const [writeModal, setWriteModal] = useState(false);
  const [pageState, setPageState] = useState(1);
  const [pinModal, setPinModal] = useState(false);
  const [pincode, setPincode] = useState("");
  const [banksList, setBanksList] = useState([]);
  const [selectedBank, setSelectedBank] = useState({});
  const [accountsEmpty, setAccountsEmpty] = useState(false);
  const [isInterbank, setIsInterbank] = useState(false);
  const [isInterbankNameNotFound, setIsInterbankNameNotFound] = useState(false);
  const [warningModal, setWarningModal] = useState(false);
  const [toAmount, setToAmount] = useState("");
  const [currencyRate, setCurrencyRate] = useState(0.0);
  const [otpModal, setOtpModal] = useState(false);
  const [userInfo, setUserInfo] = useState({});
  const [transactionFees, setTransactionFees] = useState({});
  console.log("toAccountNumber", toAccountNumber);
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  const pinHandler = (data) => {
    setPincode(data);
  };

  const fromAccountPickerHandler = (data) => {
    setFromAccount(data);
  };

  const reduceBanksList = (data) => {
    var temp = [];
    var all = data;
    temp.push(data.filter((item) => item.id === "39")[0]);
    temp.push(data.filter((item) => item.id === "05")[0]);
    temp.push(data.filter((item) => item.id === "15")[0]);
    temp.push(data.filter((item) => item.id === "04")[0]);
    temp.push(data.filter((item) => item.id === "32")[0]);

    all
      .filter(
        (item) =>
          item.id !== "39" &&
          item.id !== "05" &&
          item.id !== "15" &&
          item.id !== "04" &&
          item.id !== "32"
      )
      .map((item) => {
        temp.push(item);
      });
    setBanksList(temp);
    setSelectedBank(temp[0]);
    getTransactionFees();
  };

  const getTransactionFees = async () => {
    await apiClient
      .get("api", {
        params: {
          name: "getTransactionFees",
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          setTransactionFees(res.data);
        } else {
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    setLoading(false);
  };

  const getTransactionRate = async (data1, data2, data3) => {
    setLoading(true);
    await Axios.post("/getTransactionRate", {
      ccy1: data1,
      ccy2: data2,
      rCode: data3,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        setCurrencyRate(res.data.data.rate);
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const getBanksList = async () => {
    await apiClient
      .get("api", {
        params: {
          name: "getBanksList",
        },
      })
      .then((res) => {
        if (res.success == true) {
          reduceBanksList(res.data);
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    setLoading(false);
  };

  const getCAList = async () => {
    await apiClient
      .get("/api", {
        params: {
          name: "getCorpCAList",
          value: `corporate_cif=${user.corporate}`,
        },
      })
      .then((res) => {
        if (res.success == true) {
          setCaList(res.data);
          let temp = [...res.data];
          temp.map(
            (item, index) =>
              (temp[index].config = UserTypeGetter(item.permission))
          );
          if (temp.filter((item) => item.config.write === true).length > 0) {
            setCaList(temp.filter((item) => item.config.write === true));
            setFromAccount(
              temp.filter((item) => item.config.write === true)[0]
            );
          } else {
            setAccountsEmpty(true);
          }
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        getBanksList();
      });
  };

  const checkAccountNumber = async (data) => {
    await apiClient
      .get("api", {
        params: {
          name: "checkAccountNumber",
          value: `value=${data}`,
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          setToAccount(res.data);
          if (res.data.ccy !== fromAccount.currentAccount.acct_ccy) {
            getTransactionRate(
              fromAccount.currentAccount.acct_ccy,
              res.data.ccy,
              fromAccount.currentAccount.acct_ccy === "MNT" ? "NCSR" : "NCBR"
            );
          }
        } else {
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
  };
  const checkAccountNumberIntra = async (data) => {
    // await Axios.post("/checkAccountNumberIntra", {
    //   accountNo: data,
    //   bankId: selectedBank.id,
    // }).then((res) => {
    //   if (res.data.code.substring(0, 1) === "2") {
    await apiClient
      .get("api", {
        params: {
          name: "checkAccountNumberIntra",
          value: "accountNo=9310000069&bankId=51&cif=C00000088",
        },
      })
      .then((res) => {
        if ((res.success = true)) {
          if (res.data.data === "") {
            setIsInterbankNameNotFound(true);
          } else {
            var temp = {
              acct_num: data,
              name: res.data.data,
              ccy: "MNT",
            };
            setToAccount(temp);
          }
        } else {
          setIsInterbankNameNotFound(true);
          toast.error(`🥺 ${res.data.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
  };

  const interbankNameHandler = (data) => {
    var temp = {
      acct_num: toAccountNumber,
      name: data,
      ccy: "",
    };
    setToAccount(temp);
  };

  const toAccountHandler = (data) => {
    var temp = data.replaceAll(" ", "");
    setToAccountNumber(temp);
    if (selectedBank.id === "39") {
      if (temp.replaceAll(" ", "").length === 10) {
        checkAccountNumber(temp);
      } else {
        setToAccount({});
      }
    } else {
      setToAccount({});
    }
  };

  const toBankPickerHandler = (data) => {
    setToAccount({});
    setToAccountNumber("");
    setSelectedBank(data);
    if (data.id === "39") {
      setIsInterbank(false);
    } else {
      setIsInterbank(true);
    }
  };

  const clearData = () => {
    history.push("/");
  };

  useEffect(() => {
    setPageState(1);
    getCAList();
  }, []);

  const writeTransaction = async (data) => {
    setLoading(true);
    await apiClient
      .post(
        "/api",
        {
          corporate_cif: user.corporate,
          type: "INTRABANK",
          fromAccount: fromAccount.currentAccount.acct_num,
          fromAmount: parseFloat(sendAmount),
          fromCurrency: fromAccount.currentAccount.acct_ccy,
          toAccount: toAccount.acct_num,
          toAccountName: toAccount.name,
          toAmount: parseFloat(sendAmount),
          toCurrency: fromAccount.currentAccount.acct_ccy,
          description: description,
          pincode: sha256(pincode),
          otp: data.otp,
          otpReceiver: data.receiver,
          otpType: data.type,
        },
        {
          params: {
            name: "writeTransaction",
          },
        }
      )
      .then((res) => {
        if (res.success == true) {
          if (res.message === "SUCCESSFUL") {
            setWriteModal(true);
          } else if (res.message === "REVIEWED") {
            setReviewModal(true);
          } else {
            setConfirmModal(true);
          }
        } else {
          toast.error(`🥺 ${res.message}`, {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    setLoading(false);
  };

  const writeInterbankTransaction = async (data) => {
    setLoading(true);
    await Axios.post("/writeInterbankTransaction", {
      type: "INTERBANK",
      fromAccount: fromAccount.currentAccount.acct_num,
      fromAmount: parseFloat(sendAmount),
      fromCurrency: fromAccount.currentAccount.acct_ccy,
      toAccount: toAccount.acct_num,
      toAccountName: toAccount.name,
      toBankId: selectedBank.id,
      toBankBicId: selectedBank.biccode,
      toBankBranchCode:
        selectedBank.id === "19"
          ? selectedBank.id + "0200"
          : selectedBank.id + "0000",
      toAmount: parseFloat(sendAmount),
      toCurrency: fromAccount.currentAccount.acct_ccy,
      description: description,
      pincode: sha256(pincode),
      otp: data.otp,
      otpReceiver: data.receiver,
      otpType: data.type,
    }).then((res) => {
      if (res.data.code.substring(0, 1) === "2") {
        if (res.data.message === "SUCCESSFUL") {
          setWriteModal(true);
        } else if (res.data.message === "REVIEWED") {
          setReviewModal(true);
        } else {
          setConfirmModal(true);
        }
      } else {
        toast.error(`🥺 ${res.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
    setLoading(false);
  };

  const sendAmountHandler = (data) => {
    if (data === "") {
      setToAmount("");
      setSendAmount("");
    } else {
      let temp = parseFloat(data) / currencyRate;
      setToAmount(temp);
      setSendAmount(data);
    }
  };

  const toAmountHandler = (data) => {
    if (data === "") {
      setToAmount("");
      setSendAmount("");
    } else {
      let temp = (parseFloat(data) * currencyRate).toFixed(2);
      setToAmount(data);
      setSendAmount(temp);
    }
  };

  const getUserDetails = async () => {
    setLoading(true);
    await Axios.post("/getUserDetails").then((res) => {
      if (res.status.toString().substring(0, 1) === "2") {
        setUserInfo(res.data);
        setOtpModal(true);
      } else {
        setUserInfo({});
      }
    });
    setLoading(false);
  };

  const doTransaction = () => {
    if (parseFloat(sendAmount) >= 20000000) {
      getUserDetails();
    } else {
      isInterbank
        ? writeInterbankTransaction({
            otp: "",
            type: "",
            receiver: "",
          })
        : writeTransaction({
            otp: "",
            type: "",
            receiver: "",
          });
    }
  };
  console.log("fromAccount", fromAccount);
  return loading ? (
    <IconMBank />
  ) : (
    <div class="mx-auto w-89 rounded-lg bg-white py-7 pr-4 md:w-113 md:pr-7">
      <LargeTitle title="Бусдын данс руу" />
      <div class="pl-4 md:pl-7">
        {accountsEmpty ? (
          <ErrorCard title="Танд шилжүүлэг илгээх боломжтой данс байхгүй байна." />
        ) : pageState === 1 ? (
          <div>
            <TransactionAccountPicker
              title="Илгээх данс сонгох"
              accountList={caList}
              selectedAccount={fromAccount}
              onChange={fromAccountPickerHandler}
            />
            <div class="my-4 h-[1px] w-full bg-border-100" />
            <TransactionBankPicker
              selectedBank={selectedBank}
              banksList={banksList}
              onChange={toBankPickerHandler}
            />
            {selectedBank !== {} ? (
              <MInput
                maxLength={12}
                placeholder="Хүлээн авагчийн данс"
                id="accNo"
                value={toAccountNumber.replaceAll(" ", "")}
                type="text"
                onChange={(e) => {
                  toAccountHandler(e);
                }}
              />
            ) : null}
            {isInterbankNameNotFound ? (
              <MInput
                maxLength={30}
                placeholder="Дансны нэр"
                id="accNo"
                type="text"
                onChange={(e) => interbankNameHandler(e)}
              />
            ) : (
              <div
                onClick={() => {
                  if (isInterbank) {
                    if (
                      toAccountNumber.length > 8 &&
                      toAccountNumber.length < 14
                    ) {
                      checkAccountNumberIntra(toAccountNumber);
                    } else {
                      setToAccount({});
                    }
                  } else {
                  }
                }}
                class="flex h-12 flex-col justify-center rounded-lg bg-border-100 px-4"
              >
                <p
                  class={`text-gray ${
                    toAccount.name !== undefined
                      ? "text-xxs"
                      : "mt-[-2px] text-base"
                  } font-regular`}
                >
                  Дансны нэр
                </p>
                <p class="mt-[-3px] font-regular text-base text-black">
                  {toAccount.name !== null ? toAccount.name : ""}
                </p>
              </div>
            )}
            <div class="mt-2 flex">
              <div class="w-[calc(100%-120px)]">
                <MInput
                  maxLength={11}
                  placeholder="Мөнгөн дүн"
                  value={sendAmount}
                  numberFormat
                  onChange={(e) => {
                    sendAmountHandler(e);
                  }}
                />
              </div>
              <div class="ml-2 flex h-12 w-30 items-center justify-between rounded-lg bg-border-100 px-4">
                <div class="flex flex-col text-left">
                  <p class="font-regular text-xs text-gray">Валют</p>
                  <p class="mt-[-1px] font-regular text-base text-black">
                    {fromAccount.currentAccount?.acct_ccy !== undefined
                      ? fromAccount.currentAccount.acct_ccy
                      : ""}
                  </p>
                </div>
              </div>
            </div>
            {toAccount.ccy !== undefined &&
            toAccount.ccy !== fromAccount.currentAccount.acct_ccy &&
            !isInterbank ? (
              <>
                <div class="flex">
                  <div class="w-[calc(100%-120px)]">
                    <MInput
                      maxLength={11}
                      placeholder="Мөнгөн дүн"
                      numberFormat
                      value={toAmount}
                      onChange={(e) => {
                        toAmountHandler(e);
                      }}
                    />
                  </div>
                  <div class="ml-2 flex h-12 w-30 items-center justify-between rounded-lg bg-border-100 px-4">
                    <div class="flex flex-col text-left">
                      <p class="font-regular text-xs text-gray">Валют</p>
                      <p class="mt-[-1px] font-regular text-base text-black">
                        {toAccount.ccy}
                      </p>
                    </div>
                  </div>
                </div>
                <p class="mb-2 ml-2 font-semibold text-xs text-gray">
                  1 {toAccount.ccy} = {currencyRate + " "}
                  {fromAccount.currentAccount.acct_ccy}
                </p>
              </>
            ) : null}
            <MInput
              maxLength={60}
              placeholder="Шилжүүлгийн утга"
              id="description"
              type="text"
              onChange={(e) => setDescription(e)}
            />
            <div class="mb-18" />

            {parseFloat(sendAmount) >
            fromAccount.currentAccount?.positive_balance_amt ? (
              <ErrorCard title="Илгээх дансны үлдэгдэл хүрэлцэхгүй байна." />
            ) : (
              <MButton
                onClick={() => {
                  if (parseFloat(sendAmount) >= 5000000 && isInterbank) {
                    setWarningModal(true);
                  } else {
                    setPageState(2);
                  }
                }}
                disabled={
                  description === "" ||
                  sendAmount === "" ||
                  toAccount.name === undefined ||
                  toAccountNumber === "" ||
                  parseFloat(sendAmount) <= 0
                }
                title="Шилжүүлэг хийх"
              />
            )}
          </div>
        ) : (
          <div class="flex flex-col gap-2">
            <SelectedAccountCard
              title="Илгээх данс"
              acct_num={fromAccount.currentAccount.acct_num}
              acct_ccy={fromAccount.currentAccount.acct_ccy}
              balance_amt={fromAccount.currentAccount.positive_balance_amt}
            />

            <div class="flex h-13 items-center rounded-lg bg-border-100 px-4">
              <img
                src={`/icons/banks/bank_${selectedBank.id}.png`}
                alt=""
                class="mr-2 h-6 w-6"
              />
              <p class="mt-[-1px] font-regular text-base text-black">
                {selectedBank.bankname}
              </p>
            </div>
            <DetailCard
              title="Хүлээн авагчийн данс"
              description={toAccount.acct_num + " " + toAccount.ccy}
            />
            <DetailCard title="Дансны нэр" description={toAccount.name} />
            <div class="flex gap-2">
              <DetailCard
                title="Мөнгөн дүн"
                description={formatter.format(sendAmount).replaceAll("$", "")}
              />
              <div class="flex h-13 flex-col justify-center rounded-lg bg-border-100 px-4">
                <p class="font-regular text-xs text-gray">Валют</p>
                <p class="mt-[-1px] font-regular text-base text-black">
                  {fromAccount.currentAccount.acct_ccy}
                </p>
              </div>
            </div>
            <DetailCard
              title="Шилжүүлгийн шимтгэл"
              description={
                (isInterbank
                  ? sendAmount >= 5000000
                    ? transactionFees.rtgs
                    : transactionFees.sh
                  : transactionFees.intrabank) + "₮"
              }
            />
            <div class="flex w-full flex-col justify-center rounded-lg bg-border-100 px-4 py-2">
              <p class="font-regular text-xs text-gray">Шилжүүлгийн утга</p>
              <p class="mt-[-1px] break-all font-regular text-base leading-tight text-black">
                {description}
              </p>
            </div>
            <div class="mb-18" />
            <MButton
              disabled={false}
              onClick={() => {
                setPinModal(true);
              }}
              title="Зөвшөөрөх"
            />
          </div>
        )}
      </div>
      <ErrorToast />
      <TransactionWarning
        isOpen={warningModal}
        onClick={() => {
          setWarningModal(false);
          setPageState(2);
        }}
      />
      <TransactionConfirmSuccessModal
        toAccount={toAccount}
        selectedBank={selectedBank}
        type="INTRA"
        isOpen={confirmModal}
        onClick={() => {
          setConfirmModal(false);
          clearData();
        }}
      />
      <TransactionReviewSuccessModal
        toAccount={toAccount}
        selectedBank={selectedBank}
        type="INTRA"
        isOpen={reviewModal}
        onClick={() => {
          setReviewModal(false);
          clearData();
        }}
      />
      <TransactionWriteSuccessModal
        toAccount={toAccount}
        selectedBank={selectedBank}
        type="INTRA"
        isOpen={writeModal}
        onClick={() => {
          setWriteModal(false);
          clearData();
        }}
      />
      <NewOTPModal
        onSuccess={(data) => {
          setOtpModal(false);
          isInterbank
            ? writeInterbankTransaction(data)
            : writeTransaction(data);
        }}
        userInfo={userInfo}
        isOpen={otpModal}
        onClose={() => setOtpModal(false)}
      />
      <TransactionPincodeModal
        onClose={() => {
          setPinModal(false);
        }}
        isOpen={pinModal}
        onChange={pinHandler}
        onClick={() => {
          setPinModal(false);
          doTransaction();
        }}
      />
    </div>
  );
};

export default TransactionIntra;
