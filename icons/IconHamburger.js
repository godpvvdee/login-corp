import React from "react";

export default function IconHamburger() {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="m7 6.8 11 11M18 6.8l-11 11"
          stroke="#12131A"
          stroke-width="2"
          stroke-linecap="round"
        />
      </svg>
    </div>
  );
}
