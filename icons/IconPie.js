const IconPie = () => {
  return (
    <g data-name="Group 51458">
      <path
        data-name="Path 654649"
        d="M10 2a8 8 0 1 0 8 8 8 8 0 0 0-8-8M0 10a10 10 0 1 1 10 10A10 10 0 0 1 0 10"
      />
      <path
        data-name="Path 654650"
        d="M16 9.5A6.039 6.039 0 0 0 10.5 4a.469.469 0 0 0-.5.479V9.5a.5.5 0 0 0 .5.5h5.02a.47.47 0 0 0 .48-.5"
      />
    </g>
  );
};

export default IconPie;
