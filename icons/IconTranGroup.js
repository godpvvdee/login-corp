import React from "react";

const IconTranGroup = () => {
  return (
    <g data-name="Group 53386">
      <g data-name="Path 656873">
        <path
          d="M3 0h3a3 3 0 0 1 3 3v4a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3z"
          transform="translate(11)"
        />
        <path
          d="M3 2c-.551 0-1 .449-1 1v4c0 .551.449 1 1 1h3c.551 0 1-.449 1-1V3c0-.551-.449-1-1-1H3m0-2h3a3 3 0 0 1 3 3v4a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3z"
          transform="translate(11)"
        />
      </g>
      <g
        data-name="Rectangle 27294"
        transform="rotate(180 4.5 9)"
        style={{ strokeWidth: "2px" }}
      >
        <rect width="9" height="10" rx="3" />
        <rect x="1" y="1" width="7" height="8" rx="2" />
      </g>
      <g data-name="Rectangle 27292">
        <rect width="9" height="6" rx="3" />
        <rect x="1" y="1" width="7" height="4" rx="2" />
      </g>
      <g data-name="Rectangle 27293" transform="rotate(180 10 9)">
        <rect width="9" height="6" rx="3" style={{ stroke: "none" }} />
        <rect x="1" y="1" width="7" height="4" rx="2" />
      </g>
    </g>
  );
};

export default IconTranGroup;
