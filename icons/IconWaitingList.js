import React from "react";

const IconWaitingList = () => {
  return (
    <g data-name="Style=Line-38">
      <path
        data-name="Path 655333"
        d="M18.293 15.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1-1.414 1.414L20 18.414V22a1 1 0 0 1-2 0v-3.586l-.293.293a1 1 0 0 1-1.414-1.414z"
        style={{ fillRule: "evenodd" }}
      />
      <path
        data-name="Path 655334"
        d="M12 6a7 7 0 1 0 1.751 13.779 1 1 0 0 1 .5 1.937 9 9 0 1 1 6.681-9.84 1 1 0 0 1-1.985.247A7 7 0 0 0 12 6z"
        style={{ fillRule: "evenodd" }}
      />
      <path
        data-name="Path 655335"
        d="M4.6 5.3a1 1 0 1 1-1.2-1.6l2-1.5a1 1 0 0 1 1.2 1.6z"
      />
      <path
        data-name="Path 655336"
        d="M18.6 2.2a1 1 0 1 0-1.2 1.6l2 1.5a1 1 0 1 0 1.2-1.6z"
      />
    </g>
  );
};

export default IconWaitingList;
