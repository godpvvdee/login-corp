import React from "react";

const IconUnlock = () => {
  return (
    <>
      <defs>
        <clipPath id="44omd0rcia">
          <path data-name="Rectangle 26021" d="M0 0h16v20H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51656" style={{ clipPath: "url(#44omd0rcia)" }}>
        <path
          data-name="Path 654680"
          d="M5.759 7h4.482c.8 0 1.47 0 2.011.045a4.094 4.094 0 0 1 1.564.391 4.006 4.006 0 0 1 1.748 1.748 4.1 4.1 0 0 1 .392 1.565c.044.541.044 1.205.044 2.01v1.483c0 .805 0 1.469-.044 2.01a4.1 4.1 0 0 1-.392 1.564 4 4 0 0 1-1.748 1.748 4.077 4.077 0 0 1-1.564.392c-.541.044-1.206.044-2.011.044H5.759c-.8 0-1.47 0-2.011-.044a4.077 4.077 0 0 1-1.564-.392 3.991 3.991 0 0 1-1.748-1.748 4.1 4.1 0 0 1-.392-1.564C0 15.711 0 15.047 0 14.242v-1.483c0-.8 0-1.469.044-2.01a4.1 4.1 0 0 1 .392-1.565 4 4 0 0 1 1.748-1.748 4.094 4.094 0 0 1 1.564-.391C4.289 7 4.954 7 5.759 7M3.911 9.038a2.178 2.178 0 0 0-.819.18 2.006 2.006 0 0 0-.874.874 2.144 2.144 0 0 0-.18.819C2 11.362 2 11.944 2 12.8v1.4c0 .857 0 1.439.038 1.889a2.133 2.133 0 0 0 .18.819 2 2 0 0 0 .874.874 2.153 2.153 0 0 0 .819.181C4.361 18 4.943 18 5.8 18h4.4c.857 0 1.439 0 1.889-.037a2.153 2.153 0 0 0 .819-.181 2 2 0 0 0 .874-.874 2.133 2.133 0 0 0 .18-.819C14 15.639 14 15.057 14 14.2v-1.4c0-.856 0-1.438-.038-1.889a2.144 2.144 0 0 0-.18-.819 2.006 2.006 0 0 0-.874-.874 2.178 2.178 0 0 0-.819-.18C11.639 9 11.057 9 10.2 9H5.8c-.857 0-1.439 0-1.889.038"
          style={{ fillRule: "evenodd" }}
        />
        <path
          data-name="Path 654681"
          d="M7 14a1 1 0 0 1 2 0v1a1 1 0 0 1-2 0z"
        />
        <path
          data-name="Path 654682"
          d="M8 0a4 4 0 0 0-4 4 1 1 0 0 0 2 0 2 2 0 0 1 4 0v4a1 1 0 0 0 2 0V4a4 4 0 0 0-4-4"
        />
      </g>
    </>
  );
};

export default IconUnlock;
