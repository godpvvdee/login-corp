import React from "react";

const IconTransactionBank = () => {
  return (
    <>
      <path
        data-name="Arrow - Swap (1)"
        d="M13.707 7.707a1 1 0 0 1-1.414-1.414l4-4a1 1 0 0 1 1.414 0l4 4a1 1 0 0 1-1.414 1.414L18 5.414V17a1 1 0 0 1-2 0V5.414z"
        transform="rotate(45 12.086 4.964)"
        style={{ fillRule: "evenodd" }}
      />
    </>
  );
};

export default IconTransactionBank;
