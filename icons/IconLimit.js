import React from "react";

const IconLimit = () => {
  return (
    <>
      <defs>
        <clipPath id="zfgte7veka">
          <path data-name="Rectangle 26023" d="M0 0h20v20H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51661">
        <g data-name="Group 51660" style={{ clipPath: "url(#zfgte7veka)" }}>
          <path
            data-name="Path 654684"
            d="M4.156 4.156a9 9 0 1 0 12.689 12.689l-1.42-1.42a7 7 0 1 1-9.849-9.849z"
            style={{ fillRule: "evenodd" }}
          />
          <path
            data-name="Path 654685"
            d="M10 4a7.02 7.02 0 0 0-.876.054 1 1 0 0 1-.248-1.984A9 9 0 0 1 18.93 12.124a1 1 0 0 1-1.984-.248A7.008 7.008 0 0 0 10 4"
            style={{ fillRule: "evenodd" }}
          />
          <path
            data-name="Path 654686"
            d="M15.2.4a1 1 0 0 1 1.4-.2l2 1.5a1 1 0 0 1-1.2 1.6l-2-1.5a1 1 0 0 1-.2-1.4"
            style={{ fillRule: "evenodd" }}
          />
          <path
            data-name="Path 654687"
            d="M.293.293a1 1 0 0 1 1.414 0l18 18a1 1 0 0 1-1.414 1.414l-18-18a1 1 0 0 1 0-1.414"
            style={{ fillRule: "evenodd" }}
          />
        </g>
      </g>
    </>
  );
};

export default IconLimit;
