import React from "react";

const IconWaitingGroup = () => {
  return (
    <g data-name="Style=Line-51">
      <path
        data-name="Path 655393"
        d="M12 6a7 7 0 1 0 7 7 7 7 0 0 0-7-7zm-9 7a9 9 0 1 1 9 9 9 9 0 0 1-9-9z"
        style={{ fillRule: "evenodd" }}
      />
      <path
        data-name="Path 655394"
        d="M12.707 9.293a1 1 0 0 0-1.414 0l-2 2a1 1 0 0 0 1.414 1.414l.293-.293V16a1 1 0 0 0 2 0v-3.586l.293.293a1 1 0 0 0 1.414-1.414z"
        style={{ fillRule: "evenodd" }}
      />
      <path
        data-name="Path 655395"
        d="M4.6 5.3a1 1 0 1 1-1.2-1.6l2-1.5a1 1 0 0 1 1.2 1.6z"
      />
      <path
        data-name="Path 655396"
        d="M18.6 2.2a1 1 0 1 0-1.2 1.6l2 1.5a1 1 0 1 0 1.2-1.6z"
      />
    </g>
  );
};

export default IconWaitingGroup;
