import React from "react";

const IconTemplate = () => {
  return (
    <>
      <defs>
        <clipPath id="mtsg490pja">
          <path data-name="Rectangle 26057" d="M0 0h19v20.997H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51797">
        <g
          data-name="Group 51786"
          style={{ clipPath: "url(#mtsg490pja)" }}
          transform="translate(4 2)"
        >
          <path
            data-name="Path 655061"
            d="M8 2a3 3 0 1 0 3 3 3 3 0 0 0-3-3M3 5a5 5 0 1 1 5 5 5 5 0 0 1-5-5"
            style={{ fillRule: "evenodd" }}
          />
          <path
            data-name="Path 655062"
            d="M19 17a1 1 0 0 1-1 1h-2v2a1 1 0 0 1-2 0v-2h-2a1 1 0 0 1 0-2h2v-2a1 1 0 0 1 2 0v2h2a1 1 0 0 1 1 1"
          />
          <path
            data-name="Path 655063"
            d="M12 13a1.007 1.007 0 0 1-.29.71.99.99 0 0 1-.71.29H5a3 3 0 0 0-3 3 1 1 0 0 0 1 1h6a.99.99 0 0 1 1 1 1.007 1.007 0 0 1-.29.71A.99.99 0 0 1 9 20H3a3 3 0 0 1-3-3 5 5 0 0 1 5-5h6a.99.99 0 0 1 1 1"
          />
        </g>
      </g>
    </>
  );
};

export default IconTemplate;
