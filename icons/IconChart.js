const IconChart = () => {
  return (
    <g data-name="Style=Line - 2022-02-11T133553.011">
      <path
        data-name="Path 655077"
        d="M4 3a1 1 0 0 1 1 1v13a2 2 0 0 0 2 2h13a1 1 0 0 1 0 2H7a4 4 0 0 1-4-4V4a1 1 0 0 1 1-1z"
        style={{ fillRule: "evenodd" }}
      />
      <path
        data-name="Path 655078"
        d="M20.316 3.051a1 1 0 0 1 .632 1.265l-1.977 5.93A3 3 0 0 1 14 11.419L12.581 10a1 1 0 0 0-1.656.391l-1.977 5.93a1 1 0 0 1-1.9-.632l1.977-5.93A3 3 0 0 1 14 8.581L15.419 10a1 1 0 0 0 1.656-.391l1.977-5.93a1 1 0 0 1 1.264-.628z"
        style={{ fillRule: "evenodd" }}
      />
    </g>
  );
};

export default IconChart;
