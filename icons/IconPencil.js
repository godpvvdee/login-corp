import React from "react";

const IconPencil = () => {
  return (
    <>
      <defs>
        <clipPath id="fvqrn1qvea">
          <path data-name="Rectangle 26022" d="M0 0h18v18H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51658" style={{ clipPath: "url(#fvqrn1qvea)" }}>
        <path
          data-name="Path 654683"
          d="M10.413 1.172a4 4 0 0 1 5.657 0l.758.758a4 4 0 0 1 0 5.658l-8.551 8.55a4 4 0 0 1-1.8 1.036l-2.7.722A3 3 0 0 1 .1 14.223l.721-2.7a4 4 0 0 1 1.037-1.8zm4.243 1.415a2 2 0 0 0-2.829 0l-.912.912L14.5 7.085l.912-.912a2 2 0 0 0 0-2.829zM4.915 9.5 9.5 4.912 13.088 8.5 8.5 13.085zM3.5 10.913l-.223.223a2 2 0 0 0-.518.9l-.722 2.7a1 1 0 0 0 1.225 1.225l2.7-.722a1.992 1.992 0 0 0 .9-.518l.224-.223z"
          style={{ fillRule: "evenodd" }}
        />
      </g>
    </>
  );
};

export default IconPencil;
