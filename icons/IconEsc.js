import React from "react";

export default function IconEsc() {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M5 7h14M5 12h14M5 17h14"
          stroke="#000"
          stroke-width="2"
          stroke-linecap="round"
        />
      </svg>
    </div>
  );
}
