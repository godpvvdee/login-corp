const IconArrowRight = () => {
  return (
    <g data-name="Group 51279">
      <path
        data-name="Path 654527"
        d="M.41.366a1.52 1.52 0 0 1 1.98 0l4.2 3.75a1.156 1.156 0 0 1 0 1.767l-4.2 3.75a1.52 1.52 0 0 1-1.98 0 1.156 1.156 0 0 1 0-1.768L3.621 5 .41 2.134a1.156 1.156 0 0 1 0-1.768"
      />
    </g>
  );
};

export default IconArrowRight;
