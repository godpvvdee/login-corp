import React from "react";

const IconLogout = () => {
  return (
    <>
      <defs>
        <clipPath id="tgokzvq5va">
          <path data-name="Rectangle 26100" d="M0 0h22v22H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51758" style={{ clipPath: "url(#tgokzvq5va)" }}>
        <path
          data-name="Path 654723"
          d="M9.778 20.778A1.222 1.222 0 0 1 11 19.556h3.911c1.047 0 1.759 0 2.309-.046a2.626 2.626 0 0 0 1-.221 2.439 2.439 0 0 0 1.068-1.068 2.626 2.626 0 0 0 .221-1c.045-.55.046-1.262.046-2.309V7.089c0-1.047 0-1.758-.046-2.309a2.637 2.637 0 0 0-.221-1 2.439 2.439 0 0 0-1.068-1.068 2.631 2.631 0 0 0-1-.22c-.55-.045-1.262-.046-2.309-.046H11A1.223 1.223 0 0 1 11 0h3.962c.984 0 1.8 0 2.457.054a5 5 0 0 1 1.912.479 4.886 4.886 0 0 1 2.136 2.137 4.978 4.978 0 0 1 .479 1.911C22 5.243 22 6.055 22 7.039v7.923c0 .984 0 1.8-.054 2.457a4.983 4.983 0 0 1-.479 1.912 4.878 4.878 0 0 1-2.136 2.136 4.983 4.983 0 0 1-1.912.479c-.661.054-1.473.054-2.457.054H11a1.222 1.222 0 0 1-1.222-1.222"
          style={{ fillRule: "evenodd" }}
        />
        <path
          data-name="Path 654724"
          d="M5.753 13.8a1.222 1.222 0 0 1-1.728 1.73L.358 11.864a1.223 1.223 0 0 1 0-1.729l3.667-3.666A1.222 1.222 0 0 1 5.753 8.2l-1.58 1.58h10.494a1.222 1.222 0 0 1 0 2.445H4.173z"
        />
      </g>
      Î
    </>
  );
};

export default IconLogout;
