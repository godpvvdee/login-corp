import React from "react";

const IconHelp = () => {
  return (
    <>
      <defs>
        <clipPath id="posqhlvsma">
          <path data-name="Rectangle 26099" d="M0 0h24v24H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51756" style={{ clipPath: "url(#posqhlvsma)" }}>
        <path
          data-name="Path 654721"
          d="M12 2.4a9.6 9.6 0 1 0 9.6 9.6A9.6 9.6 0 0 0 12 2.4M0 12a12 12 0 1 1 12 12A12 12 0 0 1 0 12"
          style={{ fillRule: "evenodd" }}
        />
        <path
          data-name="Path 654722"
          d="M10.96 9a1.2 1.2 0 1 1-2.077-1.2A3.6 3.6 0 0 1 15.6 9.6a3.582 3.582 0 0 1-1.8 3c-.8.488-.3 1.8-1.8 1.8a1.2 1.2 0 0 1-1.2-1.2c0-.042 0-.084.006-.125a2.634 2.634 0 0 1 .625-1.526 4.286 4.286 0 0 1 1.08-.915c1.82-1.082-.4-3.3-1.548-1.633M12 15.6a1.2 1.2 0 1 0 1.2 1.2 1.2 1.2 0 0 0-1.2-1.2"
          style={{ fillRule: "evenodd" }}
        />
      </g>
    </>
  );
};

export default IconHelp;
