const IconCandles = () => {
  return (
    <>
      <defs>
        <clipPath id="75hwxvztza">
          <path
            data-name="Rectangle 25867"
            style={{ fill: "none" }}
            d="M0 0h16v20H0z"
          />
        </clipPath>
      </defs>
      <g data-name="Group 51454" style={{ clipPath: "url(#75hwxvztza)" }}>
        <path
          data-name="Path 654645"
          d="M14 1a1 1 0 0 0-2 0v2a1 1 0 0 0 2 0zM4 4a1 1 0 0 0-2 0v6a1 1 0 0 0 2 0zM3 14a1 1 0 0 1 1 1v4a1 1 0 0 1-2 0v-4a1 1 0 0 1 1-1m11-4a1 1 0 0 0-2 0v6a1 1 0 0 0 2 0z"
          style={{ fillRule: "evenodd" }}
        />
        <path
          data-name="Path 654646"
          d="M14 5v3a1 1 0 0 1-2 0V5a1 1 0 0 1 2 0m-4 0a3 3 0 0 1 6 0v3a3 3 0 0 1-6 0zm-6 7v1a1 1 0 0 1-2 0v-1a1 1 0 0 1 2 0m-4 0a3 3 0 0 1 6 0v1a3 3 0 0 1-6 0z"
          style={{ fillRule: "evenodd" }}
        />
      </g>
    </>
  );
};

export default IconCandles;
