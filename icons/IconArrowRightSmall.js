import React from "react";

const IconArrowRightSmall = () => {
  return (
    <svg
      data-name="Group 53645"
      xmlns="http://www.w3.org/2000/svg"
      width="7"
      height="10"
      viewBox="0 0 7 10"
    >
      <defs>
        <clipPath id="gloxcpvima">
          <path data-name="Rectangle 25711" d="M0 0h7v10H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51279" style={{ clipPath: "url(#gloxcpvima)" }}>
        <path
          data-name="Path 654527"
          d="M.41.366a1.52 1.52 0 0 1 1.98 0l4.2 3.75a1.156 1.156 0 0 1 0 1.767l-4.2 3.75a1.52 1.52 0 0 1-1.98 0 1.156 1.156 0 0 1 0-1.768L3.621 5 .41 2.134a1.156 1.156 0 0 1 0-1.768"
        />
      </g>
    </svg>
  );
};

export default IconArrowRightSmall;
