const IconTransaction = () => {
  return (
    <path
      data-name="Arrow - Swap (1)"
      d="M3.707 16.293a1 1 0 0 0-1.414 1.414l4 4a1 1 0 0 0 1.414 0l4-4a1 1 0 0 0-1.414-1.414L8 18.586V7a1 1 0 0 0-2 0v11.586zm10-8.586a1 1 0 0 1-1.414-1.414l4-4a1 1 0 0 1 1.414 0l4 4a1 1 0 0 1-1.414 1.414L18 5.414V17a1 1 0 0 1-2 0V5.414z"
      style={{ fillRule: "evenodd" }}
    />
  );
};

export default IconTransaction;
