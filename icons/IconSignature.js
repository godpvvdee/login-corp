const IconSignature = () => {
  return (
    <>
      <defs>
        <clipPath id="jhildsjo2a">
          <path data-name="Rectangle 25629" d="M0 0h20v20H0z" />
        </clipPath>
      </defs>
      <g data-name="Group 51225" style={{ clipPath: "url(#jhildsjo2a)" }}>
        <path
          data-name="Path 654507"
          d="m12.681 19.349-.069-.069a.556.556 0 0 1 0-.786l5.882-5.881a.556.556 0 0 1 .786 0l.069.069a2.222 2.222 0 0 1 0 3.143l-3.525 3.524a2.222 2.222 0 0 1-3.143 0"
        />
        <path
          data-name="Path 654508"
          d="m.011 3.22 1.64 8.343A5.557 5.557 0 0 0 4.915 15.6l4.736 2.03a.554.554 0 0 0 .611-.118l7.247-7.246a.555.555 0 0 0 .118-.612L15.6 4.917a5.557 5.557 0 0 0-4.036-3.263L3.206.011a.556.556 0 0 0-.5.936l5.686 5.772a2.215 2.215 0 1 1-1.684 1.717L.948 2.719a.556.556 0 0 0-.937.5"
        />
      </g>
    </>
  );
};

export default IconSignature;
