import axios from "axios";

export const apiClient = axios.create({
  headers: {
    Accept: "application/json",
    "Content-Type": "application/txt;charset=UTF-8",
  },
  maxContentLength: `Infinity`,
});

apiClient.interceptors.response.use(
  function (response) {
    return response?.data || {};
  },
  function (error) {
    message.error(error);
    return Promise.reject(error);
  }
);

export const apiServer = axios.create({
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json;charset=UTF-8",
    // Authorization: `Bearer ${process.env.ACCESS_TOKEN}`,
  },
  rejectUnauthorized: false,
  timeout: 3000,
  method: "GET",
  baseURL:
    process.env.NODE_ENV === "production"
      ? process.env.GATEWAY_URL
      : "http://10.41.200.92:701/gateway/",
});

apiServer.interceptors.response.use(
  function (response) {
    return response?.data || {};
  },
  function (error) {
    return error.code;
  }
);
