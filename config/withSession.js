import { withIronSessionApiRoute, withIronSessionSsr } from "iron-session/next";
import { sessionOptions } from "./session";

export function withSessionSsr(handler) {
  return withIronSessionSsr(handler, sessionOptions);
}

export function withSessionRoute(handler) {
  return withIronSessionApiRoute(handler, sessionOptions);
}
