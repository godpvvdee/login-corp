// this file is a wrapper with defaults to be used in both API routes and `getServerSideProps` functions
export const sessionOptions = {
  cookieName: "__RequestVerificationToken",
  password: "c1-VrBP48TL^fR}m^n]o]w!fzd@@aBhL",
  // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
  cookieOptions: {
    maxAge: process.env.NODE_ENV === "production" ? 60 * 60 : 60 * 60, // 30 min
    secure: process.env.NODE_ENV === "production",
  },
};
