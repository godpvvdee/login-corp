export const serviceList = {
  corporateList: "m-customer-corporate-service/corporate", // байгууллагын нэр app.js
  getCorpCAList: "m-customer-corporate-service/request-for-web/account-list?", // харилцах дансны жагсаалт account.js

  checkResetPinLogin:
    "m-customer-corporate-service/customer/corp-web/check-reset-pincode?", // хэрэглэгчийн байгуулга руу нэвтрэх пин
  checkPincodeLogin:
    "m-customer-corporate-service/customer/user-info/check-corporate-pincode?",

  getCurrencyRateList: "m-customer-corporate-service/info/currencyrate/list", // Валютын ханш account.js
  getMSEPrice: "m-customer-corporate-service/mse/price",
  getBanksList: "m-customer-corporate-service/info/banks", // бусдын дансруу гүйлгээ хийх transaction-intra.js
  getTransactionFees: "m-customer-corporate-service/info/fee",
  writeTransaction: "m-customer-corporate-service/request-for-web/write", // шилжүүлэг хийх transaction-intra end between
  getApprovedTransactions: "m-customer-corporate-service/transaction/approved?",
  checkAccountNumber: "m-customer-corporate-service/account/autofill?",
  checkAccountNumberIntra: "m-customer-corporate-service/cam/account?",
};
