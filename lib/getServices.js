import client from "./client";
import imageUrlBuilder from "@sanity/image-url";

const builder = imageUrlBuilder(client);

export const urlFor = (source) => {
  return builder.image(source);
};
export const getAllProduct = async () => {
  return await client.fetch(
    `*[_type == 'product' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
  );
};
export const getEngiinElectron = async () => {
  return await client.fetch(
    `*[_type == 'engiinElectronTooluur' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`
  );
};
export const getEngiinBySlug = async (slug) => {
  return await client.fetch(
    `*[_type == 'engiinElectronTooluur' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': images[].asset->url }`,
    { slug }
  );
};
export const getOlonTaript = async () => {
  return await client.fetch(
    `*[_type == 'olontaript' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`
  );
};
export const getOlonTariptBySlug = async (slug) => {
  return await client.fetch(
    `*[_type == 'olontaript' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`,
    { slug }
  );
};
