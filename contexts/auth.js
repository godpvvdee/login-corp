import axios from "axios";
import React, { createContext, useContext, useEffect, useState } from "react";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  var CryptoJS = require("crypto-js");
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  useEffect(() => {
    async function loadUserFromCookies() {
      await axios.get("/api/userInfo").then((res) => {
        if (res.data.success) {
          setUser(res.data);
          console.log("mama", res.data);
          setLoading(false);
        }
      });
    }
    loadUserFromCookies().catch(console.error);
  }, []);

  const login = async (username, password, deviceId) => {
    setLoading(true);
    try {
      const result = await axios.post("/api/login", {
        username: CryptoJS.AES.encrypt(
          username,
          "RF32KsMSvh5wFv5j2As9"
        ).toString(),
        password: CryptoJS.AES.encrypt(
          password,
          "RF32KsMSvh5wFv5j2As9"
        ).toString(),
        deviceId,
      });
      if (result.data.success) {
        // window.location.reload();
        window.location.assign("/choose");
        setLoading(false);
      } else {
        setLoading(false);
        setUser(false);
        console.log("error1", result.data.message);
        toast.error(`🥺 ${result.data.message}`, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        if (
          "The credentials you entered are invalid" ===
          result.data.message.error_description
        )
          setMessage("Таны оруулсан мэдээлэл буруу байна");
        else setMessage(result.data.message.error_description);
      }
    } catch (e) {
      setLoading(false);
      console.log("Auth ", e);
    }
  };

  const logout = async () => {
    try {
      const result = await axios.get("/api/logout");
      if (result.data.success) setUser(null), result.sent({ isAuth: true });
      else
        console.error("Logout !!! ", result.data),
          result.sent({ isAuth: false });
    } catch (e) {}
  };

  return (
    <AuthContext.Provider
      value={{ isAuthenticated: !!user, user, login, loading, logout, message }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const ProtectRoute = ({ children }) => {
  // const router = useRouter();
  // console.log("Router ", router.pathname);
  const { isAuthenticated, loading } = useAuth();
  // console.log("Protect Router ", isAuthenticated, loading);

  // if (isLoading || (!isAuthenticated && router.pathname !== "/login")) {
  //   // return <div>Loading...</div>;
  // }
  // return children;

  // useEffect(() => {

  // }, [isAuthenticated, isLoading]);
  if (
    loading ||
    !isAuthenticated
    //  && router.pathname !== "/login"
  ) {
    // router.push("/login");
    return children;
  }
  return children;
};

export const useAuth = () => useContext(AuthContext);
